(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    __indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  define(["Deferred", "DeferredCounter", "Storage", "EventDispatcher", "Ajax", "SpriteSheet", "SpriteFactory"], function(Deferred, DeferredCounter, Storage, EventDispatcher, Ajax, SpriteSheet, SpriteFactory) {
    var AssetManager, parseTextPackJSON, paths;
    paths = {
      plugins: "resources/plugins/",
      spritesheets: "resources/spritesheets/",
      resources_file: "resources/resources.json",
      sprites: "resources/sprites/"
    };
    AssetManager = (function(_super) {
      __extends(AssetManager, _super);

      function AssetManager(jsonURL) {
        this.jsonURL = jsonURL;
        AssetManager.__super__.constructor.call(this);
        this.spritesheets_ = {};
        this.sounds_ = {};
        this.animations_ = {};
        this.sprites_ = {};
        if (!this.spritesheets_["unordered"]) {
          this.spritesheets_["unordered"] = new SpriteSheet("unordered");
        }
      }

      return AssetManager;

    })(EventDispatcher);
    AssetManager.prototype.loadImage = function(imgURL) {
      var defer, img,
        _this = this;
      defer = new Deferred();
      img = new Image();
      img.src = imgURL;
      img.onload = function() {
        return defer.resolve(img, img);
      };
      img.onerror = function() {
        return defer.reject(img, imgURL);
      };
      return defer.promise();
    };
    AssetManager.prototype.loadImages = function(imgs) {
      var defer, img, _i, _len,
        _this = this;
      defer = new DeferredCounter(imgs.length);
      defer.promise().then(function(x) {
        return _this.triggerListener("IMAGES_LOADED", x);
      });
      for (_i = 0, _len = imgs.length; _i < _len; _i++) {
        img = imgs[_i];
        this.loadImage(img).then(function(x) {
          return defer.release(this, x);
        }).fail(function(x) {
          return defer.acquire(this, x);
        });
      }
      return defer.promise();
    };
    AssetManager.prototype.loadSpritesheet = function(sheet, path) {
      var defer,
        _this = this;
      defer = new Deferred();
      parseTextPackJSON.call(this, sheet, path).then(function(x) {
        defer.resolve(_this, x);
        return _this.triggerListener("SPRITESHEET_LOADED", x);
      }).fail(function(x) {
        defer.reject(_this, x);
        return _this.triggerListener("SPRITESHEET_LOADING_FAILED", x);
      });
      this.triggerListener("SPRITESHEET_LOADING", sheet);
      return defer.promise();
    };
    AssetManager.prototype.loadSpritesheets = function(sheets) {
      var copy, defer, file, length, path, sheet, _i, _j, _k, _len, _len1, _len2, _ref,
        _this = this;
      if ((typeof sheets === "string") || (typeof sheets[0] === "string")) {
        copy = sheets;
        sheets = [];
        sheets[0] = {};
        sheets[0].path = paths.spritesheets;
        if (typeof copy === "string") {
          sheets[0].files = copy.split(/\s*,\s*/);
        } else {
          sheets[0].files = copy;
        }
      }
      length = 0;
      for (_i = 0, _len = sheets.length; _i < _len; _i++) {
        sheet = sheets[_i];
        length += sheet.files.length;
      }
      defer = new DeferredCounter(length);
      for (_j = 0, _len1 = sheets.length; _j < _len1; _j++) {
        sheet = sheets[_j];
        path = sheet.path || paths.spritesheets;
        _ref = sheet.files;
        for (_k = 0, _len2 = _ref.length; _k < _len2; _k++) {
          file = _ref[_k];
          this.loadSpritesheet(file.path || file, path).then(function(x) {
            return defer.release(_this, x);
          }).fail(function(x) {
            return defer.acquire(_this, x);
          });
        }
      }
      defer.promise().then(function(x) {
        return this.triggerListener("SPRITESHEETS_LOADED", x);
      });
      return defer.promise();
    };
    AssetManager.prototype.getSpriteSheet = function(id) {
      if (__indexOf.call(this.spritesheets_, id) >= 0) {
        return this.spritesheets_[id];
      }
    };
    AssetManager.prototype.getSprite = function(id) {
      if (__indexOf.call(Object.keys(this.sprites_), id) >= 0) {
        return this.sprites_[id];
      } else {
        return this.sprites_["unordered_placeholder"];
      }
    };
    AssetManager.prototype.loadSprite = function(url) {
      var defer,
        _this = this;
      defer = new Deferred();
      this.loadImage(paths.sprites + url).then(function(img) {
        var sprite;
        sprite = SpriteFactory.fromSingleImage(img);
        sprite.name = "unordered_" + sprite.name;
        _this.sprites_[sprite.name] = sprite;
        _this.spritesheets_["unordered"].addSprite(sprite);
        _this.triggerListener("SPRITESHEET_CHANGED", {
          name: "unordered",
          spr: sprite
        });
        return defer.resolve(_this, sprite);
      }).fail(function(x) {
        return defer.reject(_this, x);
      });
      return defer.promise();
    };
    AssetManager.prototype.loadSprites = function(sprites) {
      var defer, spr, sprite, _i, _j, _len, _len1;
      console.log(sprites);
      for (_i = 0, _len = sprites.length; _i < _len; _i++) {
        spr = sprites[_i];
        if (Hal.asm.getSprite("unordered_" + spr) != null) {
          console.log("postoji vec tu");
          sprites.splice(sprites.indexOf(spr));
        }
      }
      console.log(sprites);
      defer = new DeferredCounter(sprites.length);
      for (_j = 0, _len1 = sprites.length; _j < _len1; _j++) {
        sprite = sprites[_j];
        this.loadSprite(sprite).then(function(x) {
          return defer.release(this, x);
        }).fail(function(x) {
          return defer.acquire(this, x);
        });
      }
      return defer.promise();
    };
    AssetManager.prototype.loadSpritesFromList = function() {
      var defer, request,
        _this = this;
      defer = new Deferred();
      request = Ajax.get(paths.sprites + "sprites.list");
      request.success(function(data) {
        data = data.split("\n");
        return _this.loadSprites(data).then(function(x) {
          defer.resolve(_this, x);
          return _this.triggerListener("SPRITESHEET_LOADED", _this.spritesheets_["unordered"]);
        });
      });
      request.fail(function(x) {
        return defer.reject(this, x);
      });
      return defer.promise();
    };
    parseTextPackJSON = function(file, path) {
      var defer, request, url,
        _this = this;
      defer = new Deferred();
      url = path + file;
      request = Ajax.get(url);
      request.success(function(data) {
        var img_url;
        img_url = path + data.meta.image;
        return _this.loadImage(img_url).then(function(img) {
          var frame, key, sh, sname, sprite, _i, _len, _ref;
          sh = new SpriteSheet(url, img, data.meta);
          _ref = Object.keys(data.frames);
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            sname = _ref[_i];
            key = sh.name + "_" + sname;
            frame = data.frames[sname];
            sprite = SpriteFactory.clipFromSpriteSheet(img, key, frame.frame);
            _this.sprites_[key] = sprite;
            _this.triggerListener("SPRITE_LOADED", sprite);
            sh.addSprite(sprite);
          }
          _this.spritesheets_[sh.name] = sh;
          return defer.resolve(_this, sh);
        }).fail(function(x) {
          return defer.reject(_this, x);
        });
      });
      request.fail(function(x) {
        return defer.reject(x);
      });
      return defer.promise();
    };
    return AssetManager;
  });

}).call(this);
