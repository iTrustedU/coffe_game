(function() {
  "use strict";
  define(["Halal"], function(Halal) {
    var Draggable;
    Draggable = (function() {
      function Draggable() {}

      Draggable.prototype.init = function() {
        var _this = this;
        Halal.addListener("DRAG_START", function(x) {
          _this.mpos[0] += _this.x;
          _this.mpos[1] += _this.y;
          if (_this.in_focus[0] && _this.inBounds(_this.mpos)) {
            return _this.triggerListener("DRAG_START", _this.mpos);
          }
        });
        Halal.addListener("DRAG_END", function(x) {
          _this.mpos[0] += _this.x;
          _this.mpos[1] += _this.y;
          if (_this.in_focus[0] && _this.inBounds(_this.mpos)) {
            return _this.triggerListener("DRAG_END", _this.mpos);
          }
        });
        return Halal.addListener("DRAG", function(x) {
          _this.mpos[0] += _this.x;
          _this.mpos[1] += _this.y;
          if (_this.in_focus[0] && _this.inBounds(_this.mpos)) {
            return _this.triggerListener("DRAG", _this.mpos);
          }
        });
      };

      return Draggable;

    })();
    return new Draggable();
  });

}).call(this);
