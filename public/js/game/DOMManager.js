(function() {
  "use strict";
  define([], function() {
    var DOMManager;
    DOMManager = (function() {
      function DOMManager() {
        var _this = this;
        this.renderspace = document.getElementById('renderspace');
        this.dom_layer = document.getElementById('domlayer');
        this.viewport = document.getElementById('viewport');
        this.area = renderspace.getBoundingClientRect();
        this.current_zindex = 1000;
        this.canvases = [];
        this.in_fullscreen = false;
        this.screen_w = document.body.clientWidth;
        this.screen_h = window.screen.availHeight;
        this.fullscreen_scale = [1.5, 1.5];
        Hal.addListener("FULLSCREEN_CHANGE", function(in_fullscreen) {
          var c, _i, _j, _len, _len1, _ref, _ref1;
          _this.area = _this.renderspace.getBoundingClientRect();
          if (in_fullscreen) {
            Hal.r.resize(_this.screen_w / _this.fullscreen_scale[0], _this.screen_h / _this.fullscreen_scale[1]);
            _ref = _this.canvases;
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
              c = _ref[_i];
              c.setAttribute('style', (c.getAttribute('style') || '') + ' ' + '-webkit-transform: scale3d(' + _this.fullscreen_scale[0] + ',' + _this.fullscreen_scale[1] + ', 1.0); -webkit-transform-origin: 0 0 0;');
            }
            return Hal.scm.enterFullScreen(_this.fullscreen_scale);
          } else {
            _this.renderspace.style["width"] = Hal.r.prev_bounds[2] + "px";
            _this.renderspace.style["height"] = Hal.r.prev_bounds[3] + "px";
            Hal.r.resize(Hal.r.prev_bounds[2], Hal.r.prev_bounds[3]);
            _ref1 = _this.canvases;
            for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
              c = _ref1[_j];
              c.setAttribute('style', (c.getAttribute('style') || '') + ' ' + '-webkit-transform: scale3d(1.0, 1.0, 1.0); -webkit-transform-origin: 0 0 0;');
            }
            return Hal.scm.exitFullScreen([1, 1]);
          }
        });
        Hal.addListener("DOM_ADD", function(callb) {
          if (callb != null) {
            return callb.call(null, _this.dom_layer);
          }
        });
        window.addEventListener("resize", function() {
          return _this.area = _this.renderspace.getBoundingClientRect();
        });
        document.addEventListener("fullscreenchange", function() {
          this.in_fullscreen = !this.in_fullscreen;
          return Hal.triggerListener("FULLSCREEN_CHANGE", this.in_fullscreen);
        }, false);
        document.addEventListener("webkitfullscreenchange", function() {
          this.in_fullscreen = !this.in_fullscreen;
          return Hal.triggerListener("FULLSCREEN_CHANGE", this.in_fullscreen);
        }, false);
        document.addEventListener("mozfullscreenchange", function() {
          this.in_fullscreen = !this.in_fullscreen;
          return Hal.triggerListener("FULLSCREEN_CHANGE", this.in_fullscreen);
        }, false);
        Hal.addListener("REQUEST_FULLSCREEN", function(scene) {
          if (!Hal.supports("FULLSCREEN")) {
            console.log("fullscreen not available");
            return;
          }
          if (!_this.in_fullscreen) {
            _this.renderspace.style["width"] = _this.screen_w + "px";
            _this.renderspace.style["height"] = _this.screen_h + "px";
            return _this.renderspace.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
          }
        });
      }

      return DOMManager;

    })();
    DOMManager.prototype.createCanvas = function(width, height) {
      var canvas;
      if (width == null) {
        width = this.area.width;
      }
      if (height == null) {
        height = this.area.height;
      }
      canvas = document.createElement("canvas");
      canvas.width = width;
      canvas.height = height;
      return canvas;
    };
    DOMManager.prototype.addCanvas = function(canvas, x, y, isTransp, z_index) {
      if (x == null) {
        x = 0;
      }
      if (y == null) {
        y = 0;
      }
      if (z_index == null) {
        z_index = 1;
      }
      canvas.style.left = x + "px";
      canvas.style.top = y + "px";
      canvas.style["z-index"] = this.current_zindex + z_index;
      if (!isTransp) {
        canvas.style['background-color'] = "white";
      }
      this.viewport.appendChild(canvas);
      this.canvases.push(canvas);
      return this.current_zindex += 1;
    };
    return DOMManager;
  });

}).call(this);
