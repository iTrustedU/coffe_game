(function() {
  "use strict";
  define([], function() {
    var Renderer;
    Renderer = (function() {
      function Renderer(bounds, canvas, camera, transp) {
        this.bounds = bounds;
        this.camera = camera;
        this.transp = transp;
        if (canvas != null) {
          this.canvas = canvas;
        } else {
          this.canvas = Hal.dom.createCanvas(this.bounds[2], this.bounds[3]);
          this.bounds[0] = 0;
          this.bounds[1] = 0;
          Hal.dom.addCanvas(this.canvas, this.bounds[0], this.bounds[1], true, 10);
        }
        this.ctx = this.canvas.getContext("2d");
      }

      return Renderer;

    })();
    Renderer.prototype.drawImage = function(img, x, y, dx, dy) {
      if (dx == null) {
        dx = 0;
      }
      if (dy == null) {
        dy = 0;
      }
      return this.ctx.drawImage(img, 0, 0, img.width, img.height, ~~(x + this.bounds[0] + this.camera[0] - dx || 0), ~~(y + this.bounds[1] - dy + this.camera[1] || 0), img.width + dx, img.height + dy);
    };
    Renderer.prototype.drawSprite = function(sprite, x, y, dx, dy) {
      if (dx == null) {
        dx = 0;
      }
      if (dy == null) {
        dy = 0;
      }
      return this.ctx.drawImage(sprite.img, sprite.x, sprite.y, sprite.w - 2, sprite.h - 2, ~~(x + this.bounds[0] + this.camera[0] - dx || 0), ~~(y + this.bounds[1] - dy + this.camera[1] || 0), sprite.w + dx, sprite.h + dy);
    };
    Renderer.prototype.drawPolygon = function(points) {
      var p, _i, _len, _ref;
      this.ctx.beginPath();
      this.ctx.moveTo(points[0][0] + this.bounds[0] + this.camera[0], points[0][1] + this.bounds[1] + this.camera[1]);
      _ref = points.slice(1);
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        p = _ref[_i];
        this.ctx.lineTo(p[0] + this.bounds[0] + this.camera[0], p[1] + this.bounds[1] + this.camera[1]);
      }
      this.ctx.closePath();
      return this.ctx.stroke();
    };
    Renderer.prototype.fillPolygon = function(points, color) {
      var p, _i, _len, _ref;
      this.ctx.save();
      this.ctx.beginPath();
      this.ctx.fillStyle = color;
      this.ctx.moveTo(points[0][0] + this.bounds[0] + this.camera[0], points[0][1] + this.bounds[1] + this.camera[1]);
      _ref = points.slice(1);
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        p = _ref[_i];
        this.ctx.lineTo(p[0] + this.bounds[0] + this.camera[0], p[1] + this.bounds[1] + this.camera[1]);
      }
      this.ctx.closePath();
      this.ctx.fill();
      return this.ctx.restore();
    };
    Renderer.prototype.resize = function(w, h) {
      this.canvas.width = w;
      this.canvas.height = h;
      this.prev_bounds = this.bounds.slice();
      this.bounds[2] = w;
      return this.bounds[3] = h;
    };
    Renderer.prototype.drawPoint = function(p) {
      this.ctx.fillRect(p[0] + this.bounds[0], p[1] + this.bounds[1], 2, 2);
      return this.ctx.fill();
    };
    Renderer.prototype.drawPoints = function(pts) {
      var p, _i, _len, _results;
      _results = [];
      for (_i = 0, _len = pts.length; _i < _len; _i++) {
        p = pts[_i];
        _results.push(this.drawPoint(p));
      }
      return _results;
    };
    Renderer.prototype.drawLine = function(a, b) {
      this.ctx.beginPath();
      this.ctx.moveTo(a[0], a[1]);
      this.ctx.lineTo(b[0], b[1]);
      this.ctx.closePath();
      return this.ctx.stroke();
    };
    Renderer.prototype.fillRect = function(rect) {
      this.ctx.fillRect(rect[0] + this.bounds[0] + this.camera[0], rect[1] + this.bounds[1] + this.camera[1], rect[2], rect[3]);
      return this.ctx.fill();
    };
    Renderer.prototype.strokeRect = function(rect) {
      this.ctx.strokeRect(rect[0] + this.bounds[0] + this.camera[0], rect[1] + this.bounds[1] + this.camera[1], rect[2], rect[3]);
      return this.ctx.stroke();
    };
    Renderer.prototype.clearRect = function(rect) {
      return this.ctx.clearRect(this.camera[0] + this.bounds[0] + rect[0], this.camera[1] + this.bounds[1] + rect[1], rect[2], rect[3]);
    };
    Renderer.prototype.clear = function() {
      return this.ctx.clearRect(this.bounds[0], this.bounds[1], this.bounds[2], this.bounds[3]);
    };
    return Renderer;
  });

}).call(this);
