"use strict";

define(

    ["Halal", "jquery-ui"],

    /* Lets you search and fallback for on supported technologies */
    function(Hal, $) {
        var socket              = io.connect('http://localhost:8080'),
            $props_container    = $("#entity-properties-container"),
            $ent_container      = $("#entity-container"),
            $anim_container     = $("#animation-container"),
            $sprites_container  = $("#sprites-container"),
            $sheet_container    = $("#spritesheets-container"),
            $all_containers     = $("ol[id$='-container']"),
            $ents_container     = $('#list-container'),
            $ent_save_btn       = $('#entity-save'),
            $ent_new_btn        = $('#entity-new'),
            $tile_list          = $('#tiles-list'),
            $tile_group         = $('.tile-group'),
            $map_list           = $('#map-list'),
            $map_group          = $('.map-group'),
            $map_list_box       = $('<select/>', {
                "size": 3,
                "class": '.map-list'
            }),
            $map_save           = $('#map-save'),
            $map_load           = $('#map-load'),
            $new_prop_btn       = $('#new-prop-btn'),
            $new_list_btn       = $('#new-list-btn'),
            Maps                = {},
            amjad               = null,
            Tiles = {
                terrain: [],
                decor: [],
                buildings: [],
                poi: [],
                character: []
            };

        Hal.addListener("SCENE_ADDED_AMJAD", function(sc) {
            amjad = sc;
        });

        Hal.addListener("MAP_SAVED", function(map) {
            saveMap(map.name, map.map);
        });

        $map_save.on('click', function() {
            amjad.pause();
            $("#save-map-dialog").dialog({
                      autoOpen: true,
                      height: 300,
                      width: 350,
                      modal: true,
                      closeOnEscape: true,
                      buttons: {
                        "OK": function() {
                            var name = $(this).find("input").val();
                            $(this).dialog("close");
                            Hal.triggerListener("REQEST_MAP_SAVE", name);
                            amjad.resume();
                        }
                      },
                      close: function() {
                        amjad.resume();
                      }
            });
        });

        $map_load.on('click', function() {
            var nm = $map_list_box.find("option:selected").text();
            loadMap(nm);
        });

        function saveMap(name, map) {
            socket.emit("NEW_MAP", {name: name, map: JSON.stringify(map)});
        }

        function loadMap(name) {
            console.log("resources/maps/" + name);
            $.getJSON("resources/maps/" + name, function(data) {
                Hal.triggerListener("REQUEST_MAP_LOAD", {map: data, name: name});
            });
        }

        $tile_list.on("click", function() {
            $ents_container.empty();
            $map_group.hide();
            $tile_group.show();
            $ents_container.slideDown();
        });

        $map_list.on("click", function() {
            $ents_container.empty();
            $tile_group.hide();
            $map_group.show();
            $ents_container.slideDown();
            $map_list_box.appendTo($ents_container);
        });

        socket.on('hello', function (data) {
            if (data.type == "maps") {
                $map_list_box.empty();
                for(var i = 0; i < data.data.length; ++i) {
                    addMap(data.data[i]);
                }
            }
        });
 
        socket.on('RESOURCES_CHANGED', function(data) {
            if(data.type == "sprites")
                Hal.asm.loadSprite(data.file);
            else if (data.type == "maps")
                var opt = $map_list_box.find("option[value='" + data.file + "']");
                if(opt[0]) {
                    opt.remove();
                }
                addMap(data.file);
        });

        socket.on("NEW_TILE_LOADED", function(tile) {
            Tiles[tile.level][tile.name] = tile;
            //createTileBox(tile.name, tile.level);om
            $('#entities-'+tile.level).click();
        });

        $.getJSON("resources/TilesList.json", function(data) {
            for (var i in data) {
                var tile = data[i];
                Tiles[tile.level][tile.name] = tile;
            }
        });

        $ent_save_btn.on("click", function() {
            try {
                var tile = saveTile();
                var jsonstr = JSON.stringify(tile);
                socket.emit("NEW_TILE", {tile: jsonstr});
            }
            catch (e) {
                console.log("Couldn't parse JSON representation of an Entity");
            }
        });

        $ent_new_btn.on("click", function() {
            var placeholder_li = $('#unordered_placeholder');
            var tile = createEmptyTileEntity(placeholder_li);
            //prikazuje placeholder sprite
            loadTile({tile: tile, spr: placeholder_li});
        });

        function addMap(name) {
            var $map_n = $('<option/>');
            $map_n.text(name);
            Maps[name] = name;
            $map_list_box.append($map_n);
        }

        function create_li(img, name) {
            if (!img) {
                img = new Image();
            }

            var $li = $('<li/>',
            {
                "id": name,
                "class": "sprite-box",
                "css": {
                    "overflow": "hidden",
                    "display": "none"
                },
                "title": name + "\t[" + img.width + "x" + img.height + "]"
            });

            $li.contextPopup({
              title: name,
              items: [
                { 
                    label: 'Bla bla',
                    action: function() { 
                        alert('clicked 1');
                    } 
                },
                { 
                    label:'Create Sample Tile',
                    action:function() { 
                        var tile = createEmptyTileEntity($li);
                        loadTile({tile: tile, spr: $li});
                    },
                },
                null,
                { 
                    label: 'Hurr durr',
                    action: function() { 
                        alert('clicked hurr durr');
                    } 
                }
              ]
            });

            img = $(img).clone();
            var title = $("<div/>");
            title.addClass("selectable-title");
            title.text(name);
            $li.append(img);
            $li.append(title);
            return $li;
        }

        Hal.asm.addListener("SPRITESHEET_CHANGED", function(sheet) {
            //$("#"+sheet.name).click();
            var spr = createSprite(sheet.spr.img, sheet.spr.name)
            $sprites_container.append(spr)
            spr.fadeIn()
        });

        function createSprite(img, spr) {
            var li = create_li(img, spr);
            li.addClass("sprite-box");
            li.fadeIn();
            li.draggable({
                helper: "clone"
            });
            li.on('click', function() {
                Hal.triggerListener("SPRITE_SELECTED", $(this).attr('id'));
            });
            return li
        }

        Hal.asm.addListener("SPRITESHEET_LOADED", function(sheet) {
            $sprites_container.slideUp();
            var li = create_li(sheet.img, sheet.name);

            li.click((function(sheet_) {
                return (function() {
                    $sprites_container.empty();
                    $sprites_container.slideDown();
                    for(var spr in sheet_.sprites) {
                        var spr = createSprite(sheet_.sprites[spr].img, spr);
                        $sprites_container.append(spr);
                        spr.fadeIn()
                    }
                });
            }(sheet)));

            $sheet_container.append(li);
            $all_containers.parent().find("legend").click(function(evt) {
                $(this).parent().find("ol[id!='list-container']").slideToggle("fast", "swing");
                evt.stopPropagation();
                evt.preventDefault();
                evt.stopImmediatePropagation();
            });
            li.fadeIn();
        });

        $anim_container.droppable({
            activeClass: "ui-state-default",
            hoverClass: "ui-state-hover",
            accept: ".sprite",
            drop: function(event, ui) {
                var spr = ui.draggable;
                var sprId = spr.attr("id");
                spr = spr.clone();
                spr.find(".selectable-title").remove();
                spr.addClass("animation-frame");
                $anim_container.append(spr);
            }
        });

        $('#entities-terrain,#entities-decor,#entities-buildings,#entities-poi,#entities-character').click(function() {
            $(this).parent().parent().find("a").removeClass("active");
            $(this).addClass("active");
            $ents_container.empty();
            var id = $(this).attr('id');
            id = id.substr(id.indexOf('-') + 1);
            for (var tile in Tiles[id]) {
                $ents_container.append(createTileBox(tile, id));
            }
        });
        
        function createTileBox(tile, id) {
            var li = create_li(Hal.asm.getSprite(Tiles[id][tile].sprite).img, Tiles[id][tile].name);
            li.attr('id', Tiles[id][tile].sprite);
            li.on('click', function(id_, name) {
                return function() { 
                    Hal.triggerListener("TILE_SELECTED", {
                        tile: Tiles[id_][name],
                        spr: $(this)
                    });
                    loadTile({tile: Tiles[id_][name], spr: $(this)});
                };
            }(id, tile));
            li.fadeIn();
            return li;
        }

        function createKeyValWrapper(label, val) {
            var $wrapper = $('<div/>', {
                'class': 'keyval-wrapper',
                'id': label.attr('for')
            });
            return $wrapper.append(label).append(val);
        }

        function createNewProperty(key, val, box, type, regexValidator) {
            var $input = $('<input/>', {
                "type": "text",
                "name": key,
                "id": key,
                "class": 'ui-widget-content ui-corner-all',
                "value": val
            });
            box.append(createKeyValWrapper(createKeyLabel(key), $input));
        }

        (function setupEntitySandbox() {
            $new_prop_btn.click(function() {
                amjad.pause()
                $("#keyval-form").dialog({
                      autoOpen: true,
                      height: 300,
                      width: 350,
                      modal: true,
                      closeOnEscape: true,
                      buttons: {
                        "OK": function() {
                            var key = $(this).find("#key").val();
                            var value = $(this).find("#value").val();
                            createNewProperty(key, value, $props_container);
                            $(this).dialog("close");
                            amjad.resume();
                        }
                      },
                      close: function() {
                        amjad.resume()
                      },
                      create: function() {
                      }
                });
            });

            $new_list_btn.click(function() {
                amjad.pause()
                 $("#list-form").dialog({
                      autoOpen: true,
                      height: 500,
                      width: 500,
                      modal: true,
                      closeOnEscape: true,
                      buttons: {
                        "OK": function() {
                            var key = $(this).find("#key").val();
                            var value = $(this).find("#value").val();
                            createNewProperty(key, value, $props_container);
                            $(this).dialog("close");
                            amjad.resume();
                        }
                      },
                      close: function() {
                        amjad.resume()
                      },
                      create: function() {
                      }
                });               
            })
        }());

        function createBox(name) {
            var $box = $("<fieldset/>");
            var $legend = $("<legend/>");
            $legend.text(name);
            $legend.appendTo($box);
            return $box;
        }

        function createKeyLabel(key) {
            var $label = $('<label/>', {
                "for": key
            });
            $label.text(key + ":");
            return $label;
        }
          
        function createNewComboBox(key, values, container, layer) {
            var $cbox = $("<select/>");
            var len = values.length;
            for (var i = 0; i < len; ++i) {
                var opt = $("<option/>");
                opt.text(values[i]).appendTo($cbox);
                if(values[i] == layer) {
                    opt.attr("selected", "selected")
                }
            }
            container.append(createKeyValWrapper(createKeyLabel(key), $cbox));
        }

        function createSpriteDroppable(key, container, spr) {
            var $sprholder = $("<div/>", {
                "id": 'ent-sprite-droppable'
            });
            $sprholder.addClass('sprite-placeholder');
            $sprholder.addClass('ui-state-default');
            $sprholder.droppable({
                activeClass: "ui-state-default",
                hoverClass: "ui-state-hover",
                accept: ".sprite-box",
                drop: function(event, ui) {
                    var sprname = ui.draggable.attr('id');
                    container.data("sprite_name", sprname);
                    $(this).empty();
                    $(this).append(ui.draggable);
                    createGrid(sprname, "size", $ent_container);
                    $(this).parent().parent().find('.keyval-wrapper#size:first').remove()
                }
            });
            if (spr != null) {
                spr = $(spr).clone();
                spr.css({'margin':'0 0 0 0', 'border': '0'});
                container.data("sprite_name", spr.attr('id'));
                $sprholder.empty();
                spr.appendTo($sprholder);
                spr.fadeIn();
            }
            container.append(createKeyValWrapper(createKeyLabel(key), $sprholder));
        }

        function createGrid(sprname, key, container, num) {
            var toggleActive = function() {
                $(this).toggleClass('minigrid-active-cell');
            };

            var spr = Hal.asm.getSprite(sprname),
                h = Math.pow(2, ~~(Math.log(spr.h-1)/Math.LN2) + 1),
                factor = 32,
                size = 128,
                w = Math.max(h, spr.h) * 2,
                numrows = w / size,
                numcols = w / size,
                diagonal = (Math.sqrt(2*size*size) * numrows) / (size/factor),
                diff = diagonal - (numcols*factor),
                wrapper = $('<div/>', {
                    "width": (diagonal) + 'px',
                    "height": ((diagonal / 2)) + 'px',
                    "class": "minigrid-wrapper"
                }),
                parent = $('<div />', {
                    "class": 'minigrid',
                    "width": numcols  * factor,
                    "height": numrows  * factor,
                });

            parent.css("left", (diff/2-1) + "px");
            parent.css("top", -(diff/2-(numrows*5)/2+(numrows/2+1)) + "px");
            var k = 0;
            var bin = decodeMiniGridSize(num);
            for (var i = 0; i < numrows; i++) {
                for(var p = 0; p < numcols; p++){
                    var $cell = $('<div />', {
                        css: {
                            'float': 'left'
                        },
                        width: factor - 1,
                        height: factor - 1
                    });
                    if(+bin[k]) {
                        $cell.addClass("minigrid-active-cell");
                    }
                    k++;
                    $cell.appendTo(parent);
                    $cell.click(toggleActive);
                }
            }

            parent.appendTo(wrapper);
            container.append(createKeyValWrapper(createKeyLabel(key), wrapper));
        }

        function loadTile(tilewrap) {
            $props_container.empty();
            var tile = tilewrap.tile;
            for(var at in tile.attr) {
                createNewProperty(at, tile.attr[at], $props_container);
            }
            $ent_container.empty();
            for(at in tile) {
                var type = typeof tile[at];
                if(type != "object" && type != "function") {
                    if (at == 'level') {
                        createNewComboBox(at, Object.keys(Tiles), $ent_container, tile[at]);
                    }
                    else if (at == 'sprite') {
                        createSpriteDroppable(at, $ent_container, tilewrap.spr);
                    }
                    else if (at == 'size') {
                        createGrid(tile.sprite, at, $ent_container, tile[at]);
                    }
                    else {
                        createNewProperty(at, tile[at], $ent_container);
                    }
                }
            }
        }

        function decodeMiniGridSize(num) {
            return num.toString(2).slice(1).split('')
        }

        function parseMiniGridSize() {
            var arr = [];
            $.each($ent_container.find(".minigrid").children(), function(k, v) {
                arr[k] = 0;
                if ($(v).hasClass('minigrid-active-cell')) {
                    arr[k] = 1;
                }
            });
            var binaryString = '1' + arr.toString().replace(/,/g,'');
            var num = parseInt(binaryString, 2);
            return num;
        }

        function parseAttributes() {
            var attr = {};
            var key = '';
            $.each($props_container.children(), function(k, v) {
                k = $(v).attr('id');
                attr[k] = $(v).find('input').val();
            });
            return attr;
        }

        function saveTile() {
            var tile = {
                level: $ent_container.find('#level').find('select > option:selected').text(),
                sprite: $ent_container.data('sprite_name'),
                name: $ent_container.find('#name').find('input').val(),
                size: parseMiniGridSize(),
                attr: parseAttributes()
            };
            return tile;
        }

        function createEmptyTileEntity(spr) {
            var tile = {
                name: "",
                level: "terrain",
                sprite: spr.attr("id"),
                size: "2",
                attr: {
                    description: "empty tile",
                    type: "none"
                }
            }
            return tile;
        }

        function createNewEntity(sprite) {

        }

        function numToBinary(num, justify) {
            var base = ~~((Math.log(num) / Math.LN2 + 1)),
                out = '';
            justify = Math.min(justify, base);
            while(~~num > 0) {
                out += (~~num % 2);
                num /= 2;
            }
            return new Array(justify + 1).join("0") + out.split("").reverse().join('');
        }
    }
);