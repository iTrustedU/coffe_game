(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["EventDispatcher", "Timer", "AssetManager", "SceneManager", "DOMManager", "DOMEventManager", "Renderer", "MathUtil", "HalalSupport", "ImgUtils"], function(EventDispatcher, Timer, AssetManager, SceneManager, DOMManager, DOMEventManager, Renderer, MathUtil, HalalSupport, ImgUtils) {
    var Halal, engineLoop;
    window.requestAnimFrame = (function() {
      return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || function(callback) {
        return window.setTimeout(callback, 1);
      };
    })();
    String.prototype.capitalize = function() {
      return this.replace(/(?:^|\s)\S/g, function(a) {
        return a.toUpperCase();
      });
    };
    engineLoop = function() {
      var scene, _i, _len, _ref;
      Hal.r.clear();
      _ref = Hal.scm.scenes;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        scene = _ref[_i];
        if (!scene.paused) {
          scene.update();
        }
      }
      Hal.triggerListener("ENTER_FRAME", Hal.t.delta * 0.001);
      Hal.frame_id = requestAnimFrame(engineLoop);
      Hal.fps_counter++;
      return Hal.t.tick();
    };
    Halal = (function(_super) {
      __extends(Halal, _super);

      function Halal() {
        var _this = this;
        Halal.__super__.constructor.call(this);
        this.paused = false;
        this.t = new Timer();
        this.fps_counter = 0;
        this.frame_id = 0;
        this.uid = -1;
        this.fps = 0;
        this.t.each(1000, function() {
          _this.triggerListener("FPS_UPDATED", _this.fps_counter);
          _this.fps = _this.fps_counter;
          return _this.fps_counter = 0;
        });
        this.addListener("SUPPORTS_FULLSCREEN", function() {
          return document.body.mozRequestFullScreen || document.body.webkitRequestFullScreen || document.body.requestFullScreen;
        });
        this.addListener("SUPPORTS_MEMORY_INFO", function() {
          return window.performance.memory != null;
        });
        this.addListener("SUPPORTS_ADVANCED_SELECTORS", function() {
          return document.querySelectorAll != null;
        });
      }

      return Halal;

    })(EventDispatcher);
    Halal.prototype.pause = function() {
      cancelAnimationFrame(this.frame_id);
      this.paused = true;
      return this.triggerListener("ENGINE_PAUSED");
    };
    Halal.prototype.resume = function() {
      this.paused = false;
      engineLoop();
      return this.triggerListener("ENGINE_RESUMED");
    };
    Halal.prototype.start = function() {
      this.triggerListener("ENGINE_STARTED");
      return engineLoop();
    };
    Halal.prototype.isPaused = function() {
      return this.paused;
    };
    Halal.prototype.UID = function() {
      return ++this.uid;
    };
    Halal.prototype.supports = function(feature) {
      return this.triggerListener("SUPPORTS_" + feature);
    };
    Halal.prototype.Keys = {
      SHIFT: 16,
      G: 71,
      D: 68,
      W: 87,
      C: 67,
      ONE: 49,
      TWO: 50,
      THREE: 51,
      FOUR: 52,
      DELETE: 46,
      LEFT: 37,
      RIGHT: 39,
      UP: 38,
      DOWN: 40
    };
    Halal.prototype.extend = HalalSupport.extend;
    window.Hal = window.Hal || new Halal();
    window.Hal.asm = new AssetManager();
    window.Hal.scm = new SceneManager();
    window.Hal.dom = new DOMManager();
    window.Hal.evm = new DOMEventManager();
    window.Hal.imgutils = new ImgUtils();
    window.Hal.r = new Renderer([0, 0, Hal.dom.area.width, Hal.dom.area.height]);
    window.Hal.m = MathUtil;
    return window.Hal;
  });

}).call(this);
