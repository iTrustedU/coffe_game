"use strict"

define () ->

    class Sprite
        constructor: (@img, @name, @x, @y, @w, @h) ->
            @name = @name || @img.src.match(/.*\/(.*)\.png/)[1]
    return Sprite
