(function() {
  "use strict";
  define(function() {
    var EventDispatcher;
    EventDispatcher = (function() {
      function EventDispatcher() {
        this.listeners = [];
        this.list_arr = [];
      }

      return EventDispatcher;

    })();
    EventDispatcher.prototype.addListener = function(type, clb) {
      var ind;
      if (this.listeners == null) {
        return;
      }
      if (this.listeners[type] == null) {
        this.listeners[type] = [];
      }
      this.listeners[type].push(clb);
      ind = this.listeners[type].indexOf(clb);
      if (ind !== -1) {
        return clb;
      }
    };
    EventDispatcher.prototype.removeListener = function(type, clb) {
      var ind;
      if (this.listeners == null) {
        return;
      }
      if (this.listeners[type] != null) {
        ind = this.listeners[type].indexOf(clb);
        if (ind !== -1) {
          return this.listeners[type].splice(ind, 1);
        }
      }
    };
    EventDispatcher.prototype.removeListeners = function(type) {
      if (this.listeners == null) {
        return;
      }
      if (this.listeners[type] != null) {
        return this.listeners[type] = [];
      }
    };
    EventDispatcher.prototype.triggerListener = function(type, msg, target) {
      var clb, _i, _len, _ref, _results;
      if (target == null) {
        target = this;
      }
      if (this.listeners == null) {
        return;
      }
      this.list_arr = this.listeners[type];
      if (this.list_arr != null) {
        _ref = this.list_arr;
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          clb = _ref[_i];
          if (clb != null) {
            _results.push(clb.call(target, msg));
          } else {
            _results.push(void 0);
          }
        }
        return _results;
      }
    };
    return EventDispatcher;
  });

}).call(this);
