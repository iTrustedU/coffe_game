(function() {
  "use strict";
  define(["Halal"], function(Halal) {
    var Keyboard;
    Keyboard = (function() {
      function Keyboard() {}

      Keyboard.prototype.init = function() {
        var _this = this;
        Halal.addListener("KEY_DOWN", function(x) {
          _this.mpos[0] += _this.x;
          _this.mpos[1] += _this.y;
          if (_this.in_focus[0] && _this.inBounds(_this.mpos)) {
            return _this.triggerListener("KEY_DOWN", x);
          }
        });
        return Halal.addListener("KEY_UP", function(x) {
          _this.mpos[0] += _this.x;
          _this.mpos[1] += _this.y;
          if (_this.in_focus[0] && _this.inBounds(_this.mpos)) {
            return _this.triggerListener("KEY_UP", x);
          }
        });
      };

      return Keyboard;

    })();
    return new Keyboard();
  });

}).call(this);
