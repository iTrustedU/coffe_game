(function() {
  "use strict";
  define(function() {
    var Timer, trig;
    trig = {};
    if (window.performance == null) {
      window.performance = Date;
    }
    return Timer = (function() {
      function Timer() {
        this.cur_time = performance.now();
        this.delta = 0;
        this.triggers = [];
        this.trig_len_ = 0;
      }

      Timer.prototype.tick = function() {
        var _i, _len, _ref, _results;
        this.delta = performance.now() - this.cur_time;
        this.cur_time += this.delta;
        _ref = this.triggers;
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          trig = _ref[_i];
          trig.tpassed += this.delta;
          if (trig.tpassed >= trig.t) {
            trig.clb();
            _results.push(trig.tpassed = 0);
          } else {
            _results.push(void 0);
          }
        }
        return _results;
      };

      Timer.prototype.each = function(time, trigClb) {
        return this.trig_len_ = this.triggers.push({
          t: time,
          clb: trigClb,
          tpassed: 0
        }) - 1;
      };

      return Timer;

    })();
  });

}).call(this);
