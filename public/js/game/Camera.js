(function() {
  "use strict";
  define([], function() {
    var Camera;
    Camera = (function() {
      function Camera(x, y, width, height, scale) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.scale = scale;
        this.x = 0;
        this.y = 0;
      }

      return Camera;

    })();
    return Camera;
  });

}).call(this);
