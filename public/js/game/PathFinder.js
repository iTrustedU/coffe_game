(function() {
  "use strict";
  define(["../amjad/tiles/Tile"], function(Tile) {
    var PathFinder;
    return PathFinder = (function() {
      function PathFinder(map) {
        this.map = map;
        this.diagonal_cost = 14;
        this.straight_cost = 10;
        this.open = [];
        this.closed = [];
      }

      PathFinder.prototype.isInClosed = function(t) {
        return this.closed.some(function(x) {
          return x.row === t.row && x.col === t.col;
        });
      };

      PathFinder.prototype.isInOpen = function(t) {
        var l;
        l = this.open.filter(function(x) {
          return x.row === t.row && x.col === t.col;
        });
        return l[0];
      };

      PathFinder.prototype.tracePath = function(dest) {
        var out;
        out = [];
        while (dest.parent != null) {
          out.push(dest);
          dest = dest.parent;
        }
        return out;
      };

      PathFinder.prototype.find = function(from, to) {
        var cur, g, h, neighs, t, tt, xDist, yDist, _i, _len;
        this.open = [];
        this.closed = [];
        if ((from == null) || (to == null)) {
          console.log("no start or end node given");
          return;
        }
        this.open.push({
          row: from.row,
          col: from.col,
          f: 0,
          g: 0,
          h: 0,
          parent: null
        });
        while (this.open.length > 0) {
          cur = this.open.shift();
          if (cur.row === to.row && cur.col === to.col) {
            return this.tracePath(cur);
          }
          this.closed.push(cur);
          neighs = this.map.getReachableNeighbours(this.map.getTile(cur.row, cur.col));
          for (_i = 0, _len = neighs.length; _i < _len; _i++) {
            t = neighs[_i];
            if ((t.row === cur.row && (Math.abs(t.col - cur.col) === 2) && (t.col % 2) !== 0) || (t.col === cur.col)) {
              g = this.straight_cost;
            } else {
              g = this.diagonal_cost;
            }
            if (this.isInClosed(t)) {
              continue;
            }
            tt = this.isInOpen(t);
            if (!tt) {
              /*
                  ovo kao dobro balansira putanju
              */

              xDist = Math.abs(to.row - t.row);
              yDist = Math.abs(to.col - t.col);
              if (xDist > yDist) {
                h = this.diagonal_cost * yDist + this.straight_cost * (xDist - yDist);
              } else {
                h = this.diagonal_cost * xDist + this.straight_cost * (yDist - xDist);
              }
              /*
                  ovo ok radi
              */

              /*
                  ovo definitivno uvek trazi najkracu al' je skupo dabogsacuvaj
              */

              this.open.push({
                h: h,
                row: t.row,
                col: t.col,
                g: cur.g + g,
                f: h + g,
                parent: cur
              });
            } else if (cur.g + g < tt.g) {
              tt.g = cur.g + g;
              tt.f = tt.h + tt.g;
              tt.parent = cur;
            }
          }
          this.open.sort(function(a, b) {
            return a.f < b.f;
          });
        }
      };

      return PathFinder;

    })();
  });

}).call(this);
