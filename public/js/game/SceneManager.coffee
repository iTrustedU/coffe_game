"use strict"

define ["Scene"], 

(Scene) ->

    ###
        update scena se radi u Halalu direktno iz RAF callbacka
        ovo sam uradio kako bih izbegao overhead pri kreiranju funkcije
        i neprestano kopiranje delta vrednosti, koja je ionako
        uvek dostupna u pravom trenutku (u toku jednog frejma).
        mislim da je opravdano, videcemo.
    ###

    class SceneManager
        constructor: () ->
            @scenes = []
            @max_depth = 0
            @top_scene = null
            @scale     = [1, 1]
            @num_scenes = @scenes.length

            # Hal.addListener "ENTER_FRAME", (delta) =>
                # Hal.r.clear()
                # for i in [0..@num_scenes]
                #     @scenes[i].update(delta

            Hal.addListener "ENGINE_PAUSED", () =>
                @pauseAll()
            
            Hal.addListener "ENGINE_RESUMED", () =>
                @unpauseAll()

            Hal.addListener "MOUSE_MOVE", (pos) =>
                pos[0] = (pos[0] / @scale[0]) # - @top_scene.x) 
                pos[1] = (pos[1] / @scale[1]) # - @top_scene.y) 
                
                for scene in @scenes
                    scene.in_focus[0] = false
                    if not scene.paused and scene.inBounds(pos)
                        high = scene

                if not high?
                    return

                @top_scene = high
                @top_scene.in_focus[0] = true
                @top_scene.mpos[0] = (pos[0] - @top_scene.x)
                @top_scene.mpos[1] = (pos[1] - @top_scene.y)
                @top_scene.triggerListener("MOUSE_MOVE", @top_scene.mpos)

            Hal.addListener "RESIZE", (scale) =>
                for scene in @scenes
                    scene.triggerListener "RESIZE", scale

            Hal.addListener "SHOW_NEXT_SCENE", () =>
                @nextScene()

            Hal.addListener "SHOW_PREV_SCENE", () =>
                return
        #update: () ->
        #    Hal.r.clear()
        #ovi mozda da budu iventovi?

        nextScene: () ->
            ind = @scenes.indexOf(@top_scene)
            @top_scene.pause()
            @top_scene = @scenes[(ind+1) % @scenes.length]
            @top_scene.resume()

        enterFullScreen: (@scale) ->
            for sc in @scenes
                sc.triggerListener("ENTER_FULLSCREEN", @scale)
        
        exitFullScreen: (@scale) ->
            for sc in @scenes
                sc.triggerListener("EXIT_FULLSCREEN", @scale)

        addSceneOnTop: (sceneA, sceneB) ->
            i = @scenes.indexOf(sceneA) 
            return if i == -1
            depth = @scenes[i].depth
            zFrom = @scenes[i].g.canvas.style["z-index"]

            for scene in @findTopScenes(depth)
                z = scene.g.canvas.style["z-index"]
                scene.g.canvas.style["z-index"] = +z + 1
                scene.depth++

            sceneB.depth = depth + 1
            sceneB.g.canvas.style["z-index"] = +zFrom + 1
            @addScene(sceneB)

        getSceneByName: (name) ->
            for scene in @scenes
                if scene.name == name
                    return scene
            return null

        putOnTop: (scene) ->
            @sortScenes()
            if not @top_scene? || scene.depth == @max_depth
                return
            scene.depth = @max_depth + 1
            @pauseAll()
            scene.resume()
            @sortScenes()

        findTopScenes: (fromDepth) ->
            out = []
            for scene in @scenes
                if scene.depth > fromDepth
                    out.push(scene)
            return out

        findBelowScenes: (fromDepth) ->
            out = []
            for scene in @scenes
                if scene.depth < fromDepth
                    out.push(scene)
            return out

        addScene: (scene) ->
            if not (scene instanceof Scene)
                return null

            if not scene.name
                scene.name = "#scene" + "_" + scene.id

            if not scene.bounds
                scene.bounds = [
                    0,
                    0,
                    Hal.dom.renderspaceRect.width,
                    Hal.dom.renderspaceRect.height
                ]

            if not scene.depth
                scene.depth = @max_depth + 1

            scene.addListener("SCENE_PAUSED", 
                (x) =>
            )

            scene.addListener("SCENE_RESUMED", 
                (x) =>
            )

            scene.init()
            Hal.triggerListener("SCENE_ADDED_"+scene.name.toUpperCase(), scene)
            @pauseAll()

            @scenes.unshift(scene)

            @sortScenes()

            return scene
        
        sortScenes: () ->
            @scenes.sort (el1,el2) ->
                return el1.depth > el2.depth
            @top_scene = @scenes[@scenes.length - 1]
            @max_depth = @top_scene.depth

        pauseAll: () ->
            scene.pause() for scene in @scenes

        unpauseAll: () ->
            scene.resume() for scene in @scenes

    return SceneManager