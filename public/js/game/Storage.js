(function() {
  "use strict";
  define(function() {
    var Storage;
    Storage = (function() {
      function Storage(store) {
        this.store = store != null ? store : [];
        return;
      }

      return Storage;

    })();
    Storage.prototype.serialize = function(storeKey, key, val) {
      if (this.store[storeKey] == null) {
        this.store[storeKey] = [];
      }
      return this.store[storeKey][key] = val;
    };
    Storage.prototype.deserialize = function(storeKey, key) {
      if (this.store[storeKey] != null) {
        return this.store[storeKey][key];
      }
    };
    return Storage;
  });

}).call(this);
