"use strict"

define ()->

    class Storage
        constructor: (@store = []) ->
            return
        
    Storage::serialize = (storeKey, key, val) ->
        if not @store[storeKey]?
            @store[storeKey] = []
        @store[storeKey][key] = val

    Storage::deserialize = (storeKey, key) -> 
        if @store[storeKey]?
            return @store[storeKey][key]

    return Storage