(function() {
  "use strict";
  define(["Halal"], function(Halal) {
    var Scrollable;
    Scrollable = (function() {
      function Scrollable() {}

      Scrollable.prototype.init = function() {
        var _this = this;
        if ((this.parent_scene != null)) {
          Halal.addListener("SCROLL_UP", function() {
            var p;
            p = [_this.parent_scene.mpos[0] - _this.camera[0] - _this.pos[0], _this.parent_scene.mpos[1] - _this.camera[1] - _this.pos[1]];
            if (_this.in_focus[0] && _this.inBounds(p)) {
              return _this.triggerListener("SCROLL_UP", p);
            }
          });
          return Halal.addListener("SCROLL_DOWN", function() {
            var p;
            p = [_this.parent_scene.mpos[0] - _this.camera[0] - _this.pos[0], _this.parent_scene.mpos[1] - _this.camera[1] - _this.pos[1]];
            if (_this.in_focus[0] && _this.inBounds(p)) {
              return _this.triggerListener("SCROLL_DOWN", p);
            }
          });
        } else {
          Halal.addListener("SCROLL_DOWN", function() {
            _this.mpos[0] += _this.x;
            _this.mpos[1] += _this.y;
            if (_this.in_focus[0] && _this.inBounds(_this.mpos)) {
              return _this.triggerListener("SCROLL_DOWN", _this.mpos);
            }
          });
          return Halal.addListener("SCROLL_UP", function() {
            _this.mpos[0] += _this.x;
            _this.mpos[1] += _this.y;
            if (_this.in_focus[0] && _this.inBounds(_this.mpos)) {
              return _this.triggerListener("SCROLL_UP", _this.mpos);
            }
          });
        }
      };

      return Scrollable;

    })();
    return new Scrollable();
  });

}).call(this);
