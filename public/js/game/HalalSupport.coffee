"use strict"

define [],

() ->
    return {
        addComponent: (comp) ->
            comp.init.call(@) if comp.init?

        extend: (obj) ->
            if not obj
                return @

            for key in Object.keys(obj)
                continue if @ == obj[key]
                @[key] = obj[key]

            return @
    }

