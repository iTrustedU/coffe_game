"use strict"

define [
    "EventDispatcher", 
    "Timer", 
    "AssetManager", 
    "SceneManager", 
    "DOMManager", 
    "DOMEventManager", 
    "Renderer", 
    "MathUtil",
    "HalalSupport",
    "ImgUtils"
],

(
    EventDispatcher, 
    Timer, 
    AssetManager, 
    SceneManager, 
    DOMManager, 
    DOMEventManager, 
    Renderer, 
    MathUtil, 
    HalalSupport,
    ImgUtils
)  ->

    #treba isto neki shim za cancelFrame, otom potom
    
    window.requestAnimFrame = do () ->
        return  window.requestAnimationFrame       or
                window.webkitRequestAnimationFrame or
                window.mozRequestAnimationFrame    or
                (callback) ->
                    window.setTimeout(callback, 1)
                    
    String.prototype.capitalize = () ->
        return this.replace(/(?:^|\s)\S/g, (a) -> return a.toUpperCase() )

    engineLoop = () ->
        Hal.r.clear()
        for scene in Hal.scm.scenes
            scene.update() if not scene.paused
        Hal.triggerListener("ENTER_FRAME", Hal.t.delta * 0.001)

        Hal.frame_id = requestAnimFrame(engineLoop)
        Hal.fps_counter++
        Hal.t.tick()

    class Halal extends EventDispatcher
        constructor: () ->
            super()
            @paused         = false
            @t              = new Timer()
            @fps_counter    = 0
            @frame_id       = 0
            @uid            = -1
            @fps            = 0

            #don't do this very often
            @t.each 1000, () =>
                @triggerListener("FPS_UPDATED", @fps_counter)
                @fps         = @fps_counter
                @fps_counter = 0

            @addListener "SUPPORTS_FULLSCREEN", () ->
                return document.body.mozRequestFullScreen or 
                        document.body.webkitRequestFullScreen or 
                            document.body.requestFullScreen

            @addListener "SUPPORTS_MEMORY_INFO", () ->
                return window.performance.memory?
            @addListener "SUPPORTS_ADVANCED_SELECTORS", () ->
                return document.querySelectorAll?

    Halal::pause = () ->
        cancelAnimationFrame(@frame_id)
        @paused = true
        @triggerListener("ENGINE_PAUSED")

    Halal::resume = () ->
        @paused = false
        engineLoop()
        @triggerListener("ENGINE_RESUMED")

    Halal::start = () ->
        @triggerListener("ENGINE_STARTED")
        engineLoop()

    Halal::isPaused = () ->
        return @paused

    Halal::UID = () ->
        return ++@uid

    Halal::supports = (feature) ->
        @triggerListener "SUPPORTS_"+feature

    Halal::Keys = 
        SHIFT: 16
        G: 71
        D: 68
        W: 87
        C: 67
        ONE: 49
        TWO: 50
        THREE: 51
        FOUR: 52
        DELETE: 46
        LEFT: 37
        RIGHT: 39
        UP: 38
        DOWN: 40

    Halal::extend = HalalSupport.extend
    
    window.Hal          = window.Hal || new Halal()
    window.Hal.asm      = new AssetManager()
    window.Hal.scm      = new SceneManager()
    window.Hal.dom      = new DOMManager()
    window.Hal.evm      = new DOMEventManager()
    window.Hal.imgutils = new ImgUtils()

    window.Hal.r        = new Renderer([0, 0, Hal.dom.area.width, Hal.dom.area.height])
    window.Hal.m        = MathUtil

    return window.Hal