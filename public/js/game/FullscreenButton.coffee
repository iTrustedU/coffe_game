"use strict"

define [],

() ->

    Hal.triggerListener("DOM_ADD", (domlayer) ->
        if not Hal.supports("FULLSCREEN")
            console.log("fullscreen not supported on this device")

        width = 50
        right = 0
        div = $("<div/>", {
            css: 
                "background-color": "rgb(94, 190, 255)",
                "opacity": "0.8",
                "text-align": "center",
                "font-size": "0.7em",
                "position": "absolute",
                "width": width + "px",
                "display": "block",
                "right": right + "px"
                "cursor": "pointer"
        })

        div.text("Go fullscreen")
        
        div.on "mousemove", (ev) ->
            ev.stopPropagation()

        div.on "mouseup", (ev) ->
            ev.stopPropagation()

        div.on "click", (ev) ->
            Hal.triggerListener("REQUEST_FULLSCREEN")
            ev.stopPropagation()

        Hal.addListener "ENTER_FULLSCREEN", () -> 
            div.hide()
            $("#hud").toggle()

        Hal.addListener "EXIT_FULLSCREEN", () -> 
            div.show()
            $("#hud").toggle()

        $(domlayer).append(div)
    )