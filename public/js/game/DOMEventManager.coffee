"use strict"

define [],

() ->

    class DOMEventManager
        constructor: () ->
            @viewport                = null
            @prev_area               = null
            @mouse_leftbtn_down      = false
            @mouse_rightbtn_down     = false
            @can_drag                = true
            @pos                     = [0, 0]
            @viewport                = Hal.dom.renderspace
            @prev_area               = Hal.dom.area
            @domlayer                = Hal.dom.dom_layer
            @dragging                = false
            
            #ako ne podrzava queryselectorall
            #napravi shim koji prolazi kroz childove
            #node-a koji je ispod trenutne pozicije misha

            @viewport.addEventListener("mousedown", @mouseDown)
            @viewport.addEventListener("mouseup", @mouseUp)
            @viewport.addEventListener("mousemove", @mouseMove)
            @viewport.addEventListener("onmousewheel", @wheelMoved)
            @viewport.addEventListener("onContextMenu", () -> return false)
            @viewport.addEventListener("mousewheel", @wheelMoved)

            window.addEventListener("keydown", @keyDown)
            window.addEventListener("keyup", @keyUp)
            @viewport.addEventListener("click", @mouseClick)
            @viewport.addEventListener("dblclick", @mouseDblClick)

        wheelMoved: (evt) =>
            @getMousePos(evt)
            if (evt.wheelDelta < 0) 
                Hal.triggerListener("SCROLL_DOWN", @pos)
            else 
                Hal.triggerListener("SCROLL_UP", @pos)

        keyDown: (evt) =>
            if not (@domlayer.querySelectorAll(':hover').length > 0)
                @getMousePos(evt)
                Hal.triggerListener("KEY_DOWN", evt)
                #evt.preventDefault()
                #evt.stopPropagation()

        keyUp: (evt) =>
            @getMousePos(evt)
            Hal.triggerListener("KEY_UP", evt)
            #if(@domlayer.querySelectorAll(':hover').length > 0)
            # evt.preventDefault()
            # evt.stopPropagation()

        mouseDblClick: (evt) =>
            @getMousePos(evt)
            Hal.triggerListener("MOUSE_DBL_CLICK", @pos)

        mouseClick: (evt) =>
            @getMousePos(evt)
            Hal.triggerListener("MOUSE_CLICKED", @pos)
 
        mouseMove: (evt) =>
            if not (@domlayer.querySelectorAll(':hover').length > 0)
                @getMousePos(evt)
                Hal.triggerListener("MOUSE_MOVE", @pos)

                if (@mouse_leftbtn_down and (not @dragging and @can_drag))
                    Hal.triggerListener("DRAG_START", @pos)
                    @dragging = true
                    @can_drag = false
                #evt.stopPropagation()

        mouseUp: (evt) =>
            @getMousePos(evt)

            if @mouse_rightbtn_down and not @dragging
                Hal.triggerListener("MOUSE_CLICK", 2)
                @mouse_rightbtn_down = false
            else if @mouse_leftbtn_down and not @dragging
                Hal.triggerListener("MOUSE_CLICK", 0)
                @mouse_leftbtn_down = false
            if @dragging
                @dragging = false
                Hal.triggerListener("DRAG_END", @pos)
                @can_drag = true
                @mouse_leftbtn_down = false
            #evt.preventDefault()
            #evt.stopPropagation()

        mouseDown: (evt) =>
            @getMousePos(evt)
            if evt.button == 0
                @mouse_leftbtn_down = true
            else if evt.button == 2
                @mouse_rightbtn_down = true
            #evt.preventDefault()
            #evt.stopPropagation()

        getMousePos: (evt) =>
            @pos[0] = evt.clientX  - Hal.dom.area.left
            @pos[1] = evt.clientY - Hal.dom.area.top

    return DOMEventManager