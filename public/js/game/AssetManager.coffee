"use strict"

define ["Deferred", "DeferredCounter", "Storage", "EventDispatcher", "Ajax", "SpriteSheet", "SpriteFactory"],

(Deferred, DeferredCounter, Storage, EventDispatcher, Ajax, SpriteSheet, SpriteFactory) ->

    paths = 
        plugins: "resources/plugins/"
        spritesheets: "resources/spritesheets/"
        resources_file: "resources/resources.json",
        sprites: "resources/sprites/"

    class AssetManager extends EventDispatcher
        constructor: (@jsonURL) ->
            super()
            @spritesheets_  = {}
            @sounds_        = {}
            @animations_    = {}
            @sprites_       = {}
            if not @spritesheets_["unordered"]
                @spritesheets_["unordered"] = new SpriteSheet("unordered")

    AssetManager::loadImage = (imgURL) ->        
        #parse url to get the version no. of image
        #use that to check if it is in cache (a.k.a storage)
        defer   = new Deferred()
        img     = new Image()
        img.src = imgURL

        img.onload = =>
            defer.resolve(img, img)
        img.onerror = => 
            defer.reject(img, imgURL)

        return defer.promise()


    AssetManager::loadImages = (imgs) ->
        defer = new DeferredCounter(imgs.length)
        defer.promise().then (x) =>
            @triggerListener("IMAGES_LOADED", x)

        for img in imgs
            @loadImage(img)
            .then (x) ->
                    defer.release(@, x)
            .fail (x) ->
                    defer.acquire(@, x)

        return defer.promise()

    AssetManager::loadSpritesheet = (sheet, path) ->
        defer = new Deferred()
        parseTextPackJSON.call(@, sheet, path).then (x) =>
            defer.resolve(@, x)
            @triggerListener("SPRITESHEET_LOADED", x)
        .fail (x) =>
            defer.reject(@, x)
            @triggerListener("SPRITESHEET_LOADING_FAILED", x)
       
        @triggerListener("SPRITESHEET_LOADING", sheet)

        return defer.promise()

    AssetManager::loadSpritesheets = (sheets) ->
        if (typeof sheets == "string") or (typeof sheets[0] == "string")
            copy        = sheets
            sheets      = []
            sheets[0]   = {}
            sheets[0].path = paths.spritesheets
            if (typeof copy == "string") 
                sheets[0].files = copy.split(/\s*,\s*/) 
            else
                sheets[0].files = copy

        length = 0
        for sheet in sheets
            length += sheet.files.length

        defer = new DeferredCounter(length)

        for sheet in sheets
            path = sheet.path || paths.spritesheets
            for file in sheet.files
                @loadSpritesheet(file.path || file, path).then (x) =>
                    defer.release(@, x)
                .fail (x) =>
                    defer.acquire(@, x)

        defer.promise().then (x) ->
            @triggerListener("SPRITESHEETS_LOADED", x)

        return defer.promise()

    AssetManager::getSpriteSheet = (id) ->
        if id in @spritesheets_
            return @spritesheets_[id]

    AssetManager::getSprite = (id) ->
        if id in Object.keys(@sprites_)
            return @sprites_[id]
        else
            return @sprites_["unordered_placeholder"]

    AssetManager::loadSprite = (url) ->
        defer = new Deferred()
        @loadImage(paths.sprites + url).then (img) =>          
            sprite = SpriteFactory.fromSingleImage(img)
            sprite.name = "unordered_" + sprite.name
            @sprites_[sprite.name] = sprite
            @spritesheets_["unordered"].addSprite(sprite)
            @triggerListener("SPRITESHEET_CHANGED", {name: "unordered", spr: sprite})
            defer.resolve(@, sprite)
        .fail (x) =>
            defer.reject(@, x)

        return defer.promise()

    AssetManager::loadSprites = (sprites) ->
        console.log(sprites)
        for spr in sprites 
            if Hal.asm.getSprite("unordered_"+spr)?
                console.log("postoji vec tu")
                sprites.splice(sprites.indexOf(spr))

        console.log(sprites)
        defer = new DeferredCounter(sprites.length)

        for sprite in sprites
            @loadSprite(sprite).then (x) -> 
                defer.release(@, x)
            .fail (x) ->
                defer.acquire(@, x)

        return defer.promise()

    AssetManager::loadSpritesFromList = () ->
        defer   = new Deferred()
        request = Ajax.get(paths.sprites + "sprites.list")
        request.success (data) =>
            data = data.split("\n")
            @loadSprites(data).then (x) =>
                defer.resolve(@, x)
                @triggerListener("SPRITESHEET_LOADED", @spritesheets_["unordered"])

        request.fail (x) ->
            defer.reject(@, x)

        return defer.promise()


    parseTextPackJSON = (file, path) ->
        defer = new Deferred()
        url = path + file
        request = Ajax.get(url)
        
        request.success (data) =>
            img_url = path + data.meta.image
            @loadImage(img_url).then (img) =>
                sh = new SpriteSheet(url, img, data.meta)
                for sname in Object.keys(data.frames)
                    key     = sh.name + "_" + sname
                    frame   = data.frames[sname]
                    sprite  = SpriteFactory.clipFromSpriteSheet(
                        img,
                        key,
                        frame.frame
                    )
                    @sprites_[key] = sprite
                    @triggerListener("SPRITE_LOADED", sprite)
                    sh.addSprite(sprite)
                @spritesheets_[sh.name] = sh
                defer.resolve(@, sh)
            .fail (x) =>
                    defer.reject(@, x)

        request.fail (x) =>
            defer.reject(x)

        return defer.promise()

    return AssetManager