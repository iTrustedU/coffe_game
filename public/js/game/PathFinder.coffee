"use strict"

define ["../amjad/tiles/Tile"], 

(Tile) ->

    class PathFinder
        constructor: (@map) ->
            @diagonal_cost = 14
            @straight_cost = 10

            @open = []
            @closed = []

        isInClosed: (t) ->
            return @closed.some (x) -> return x.row == t.row and x.col == t.col

        isInOpen: (t) ->
            l = @open.filter (x) -> return x.row == t.row and x.col == t.col
            return l[0]

        tracePath: (dest) ->
            out = []
            while dest.parent?
                out.push(dest)
                dest = dest.parent

            return out

        find: (from, to) ->
            @open = []
            @closed = []

            if not from? or not to?
                console.log("no start or end node given")
                return

            @open.push({
                row: from.row
                col: from.col
                f: 0
                g: 0
                h: 0
                parent: null
            })

            while @open.length > 0
                cur = @open.shift()

                if cur.row == to.row and cur.col == to.col
                    return @tracePath(cur)
                @closed.push(cur)

                neighs = @map.getReachableNeighbours(@map.getTile(cur.row, cur.col))

                for t in neighs
                    if (t.row == cur.row and (Math.abs(t.col-cur.col) == 2) and (t.col % 2) != 0) or (t.col == cur.col)
                        g = @straight_cost
                    else 
                        g = @diagonal_cost

                    if @isInClosed(t)
                        continue

                    tt = @isInOpen(t)

                    if not tt
                        ###
                            ovo kao dobro balansira putanju
                        ###
                        xDist = Math.abs(to.row - t.row)
                        yDist = Math.abs((to.col - t.col))
                        
                        if xDist > yDist
                            h = @diagonal_cost*yDist + @straight_cost*(xDist - yDist)
                        else 
                            h = @diagonal_cost*xDist + @straight_cost*(yDist - xDist)
                        ###
                            ovo ok radi
                        ###
                        #h = (Math.abs(to.row - t.row) + Math.abs((to.col - t.col)/2-(t.col%2))) * @straight_cost
                        
                        #h = (Math.abs(to.row - t.row) + Math.abs(t.col - to.col)) * (@straight_cost + (t.col%2))
                        ###
                            ovo definitivno uvek trazi najkracu al' je skupo dabogsacuvaj
                        ###

                        # xDiff = Math.abs(to.pos[0] - t.pos[0])
                        # yDiff = Math.abs(to.pos[1] - t.pos[1])
                        # h = Math.sqrt(xDiff*xDiff - yDiff*yDiff)
                        # if t.col % 2
                        #     cost = @straight_cost
                        # else 
                        #     cost = @diagonal_cost

                        #h = (Math.abs(t.row - to.row) + Math.abs(t.col - to.col)) * @straight_cost
                        @open.push({
                            h: h
                            row: t.row
                            col: t.col
                            g: cur.g + g
                            f: h + g
                            parent: cur
                        })

                    else if cur.g + g < tt.g
                            tt.g = cur.g + g
                            tt.f = tt.h + tt.g
                            tt.parent = cur

                @open.sort (a,b) ->
                    return a.f < b.f