(function() {
  "use strict";
  define(["Halal"], function(Hal) {
    var Animation;
    return Animation = (function() {
      function Animation() {}

      Animation.extend({
        playAnimation: function(msecs) {
          return Hal.addListener("ENTER_FRAME", function(delta) {
            return console.log("do something with delta: " + delta);
          });
        }
      });

      Animation.prototype.init = function() {
        Hal.e;
        return this.addListener;
      };

      return Animation;

    })();
  });

}).call(this);
