"use strict"

define ["EventDispatcher", "HalalSupport"], 

(EventDispatcher, HalalSupport) ->

    class Entity extends EventDispatcher
        constructor: (x, y, @bounds) ->
            @parent_scene = null
            @g          = null
            @in_focus   = false
            @pos        = [x, y]
            @id         = Hal.UID()
            @camera     = []
            super()

        init: () ->
            return
        
        addCamera: (camera) ->
            @camera = camera

        extend: HalalSupport.extend
        addComponent: HalalSupport.addComponent
        
    Entity::inBounds = (point) ->
        return Hal.m.isPointInRect(point, @bounds)

    return Entity
    