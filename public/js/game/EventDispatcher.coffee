"use strict"

define  ()->

    class EventDispatcher
        constructor: () ->
            @listeners = []      
            @list_arr = []

    EventDispatcher::addListener = (type, clb) ->
        return if not @listeners?
        if not @listeners[type]?
            @listeners[type] = []
        @listeners[type].push(clb)
        ind = @listeners[type].indexOf(clb)
        return clb if ind != -1

    EventDispatcher::removeListener = (type, clb) ->
        return if not @listeners?
        if @listeners[type]? 
            ind = @listeners[type].indexOf(clb)
            @listeners[type].splice(ind, 1) if ind isnt -1
    EventDispatcher::removeListeners = (type) ->
        return if not @listeners?
        if @listeners[type]?
            @listeners[type] = []
    EventDispatcher::triggerListener = (type, msg, target = @) ->
        return if not @listeners?
        @list_arr = @listeners[type]
        if @list_arr?
            for clb in @list_arr
                clb.call(target, msg) if clb?
                    #break;

    return EventDispatcher
