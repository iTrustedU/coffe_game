"use strict"

define ["Halal"], 

(Halal) ->
    class Scrollable
        init: () ->
            if (@parent_scene?)
                Halal.addListener("SCROLL_UP", 
                    () =>
                        p = [@parent_scene.mpos[0] - @camera[0] - @pos[0], @parent_scene.mpos[1] - @camera[1] - @pos[1]]
                        if(@in_focus[0] && @inBounds(p))
                            @triggerListener("SCROLL_UP", p)
                )
                Halal.addListener("SCROLL_DOWN", 
                    () =>
                        p = [@parent_scene.mpos[0] - @camera[0] - @pos[0], @parent_scene.mpos[1] - @camera[1] - @pos[1]]
                        if(@in_focus[0] && @inBounds(p))
                            @triggerListener("SCROLL_DOWN", p)
                )
            else 
                Halal.addListener("SCROLL_DOWN",
                    () =>
                        @mpos[0] += @x
                        @mpos[1] += @y
                        if(@in_focus[0] && @inBounds(@mpos))
                            @triggerListener("SCROLL_DOWN", @mpos)
                )
                
                Halal.addListener("SCROLL_UP",
                    () =>
                        @mpos[0] += @x
                        @mpos[1] += @y
                        if(@in_focus[0] && @inBounds(@mpos))
                            @triggerListener("SCROLL_UP", @mpos)
                )                
    return new Scrollable()