"use strict"

define ["Halal"], 

(Halal) ->
    class Draggable
        init: () ->
            Halal.addListener("DRAG_START", 
                (x) => 
                    @mpos[0] += @x
                    @mpos[1] += @y
                    if(@in_focus[0] && @inBounds(@mpos))
                        @triggerListener("DRAG_START", @mpos)
            )
            Halal.addListener("DRAG_END",
                (x) =>
                    @mpos[0] += @x
                    @mpos[1] += @y
                    if(@in_focus[0] && @inBounds(@mpos))
                        @triggerListener("DRAG_END", @mpos)
            )
            Halal.addListener("DRAG", 
                (x) =>
                    @mpos[0] += @x
                    @mpos[1] += @y
                    if(@in_focus[0] && @inBounds(@mpos))
                        @triggerListener("DRAG", @mpos)
            )

    return new Draggable()