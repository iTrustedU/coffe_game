(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["EventDispatcher", "HalalSupport"], function(EventDispatcher, HalalSupport) {
    var Entity;
    Entity = (function(_super) {
      __extends(Entity, _super);

      function Entity(x, y, bounds) {
        this.bounds = bounds;
        this.parent_scene = null;
        this.g = null;
        this.in_focus = false;
        this.pos = [x, y];
        this.id = Hal.UID();
        this.camera = [];
        Entity.__super__.constructor.call(this);
      }

      Entity.prototype.init = function() {};

      Entity.prototype.addCamera = function(camera) {
        return this.camera = camera;
      };

      Entity.prototype.extend = HalalSupport.extend;

      Entity.prototype.addComponent = HalalSupport.addComponent;

      return Entity;

    })(EventDispatcher);
    Entity.prototype.inBounds = function(point) {
      return Hal.m.isPointInRect(point, this.bounds);
    };
    return Entity;
  });

}).call(this);
