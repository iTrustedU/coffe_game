"use strict"

define ["Halal"], 

(Halal) ->
    class Keyboard
        init: () ->
            Halal.addListener("KEY_DOWN", 
                (x) =>
                    @mpos[0] += @x
                    @mpos[1] += @y
                    if(@in_focus[0] && @inBounds(@mpos))
                        @triggerListener("KEY_DOWN", x)
            )
            Halal.addListener("KEY_UP", 
                (x) =>
                    @mpos[0] += @x
                    @mpos[1] += @y
                    if(@in_focus[0] && @inBounds(@mpos))
                        @triggerListener("KEY_UP", x)
            )
    return new Keyboard()