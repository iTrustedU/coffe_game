"use strict"

define ["Sprite"], 

(Sprite) ->

    SpriteFactory = {}

    SpriteFactory.clipFromSpriteSheet = (img, name, cliprect) ->
        return new Sprite(Hal.imgutils.clipImage(img, cliprect), name, 0, 0, cliprect.w, cliprect.h)

    SpriteFactory.fromSingleImage = (img, name) ->
        return new Sprite(img, name, 0, 0, img.width, img.height)

    return SpriteFactory