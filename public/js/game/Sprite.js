(function() {
  "use strict";
  define(function() {
    var Sprite;
    Sprite = (function() {
      function Sprite(img, name, x, y, w, h) {
        this.img = img;
        this.name = name;
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        this.name = this.name || this.img.src.match(/.*\/(.*)\.png/)[1];
      }

      return Sprite;

    })();
    return Sprite;
  });

}).call(this);
