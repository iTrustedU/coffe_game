(function() {
  "use strict";
  define(["Halal"], function(Halal) {
    var Clickable;
    Clickable = (function() {
      var btn;

      function Clickable() {}

      btn = {
        0: "LEFT",
        2: "RIGHT"
      };

      Clickable.prototype.init = function() {
        var _this = this;
        if ((this.parent_scene != null)) {
          Halal.addListener("MOUSE_CLICK", function(mbtn) {
            var p;
            p = [_this.parent_scene.mpos[0] - _this.camera[0] - _this.pos[0], _this.parent_scene.mpos[1] - _this.camera[1] - _this.pos[1]];
            if (_this.in_focus[0] && _this.inBounds(p)) {
              return _this.triggerListener(btn[mbtn] + "_CLICK", p);
            }
          });
          return Halal.addListener("MOUSE_CLICKED", function() {
            var p;
            p = [_this.parent_scene.mpos[0] - _this.camera[0] - _this.pos[0], _this.parent_scene.mpos[1] - _this.camera[1] - _this.pos[1]];
            if (_this.in_focus[0] && _this.inBounds(p)) {
              return _this.triggerListener("MOUSE_CLICKED", p);
            }
          });
        } else {
          Halal.addListener("MOUSE_CLICK", function(mbtn) {
            _this.mpos[0] += _this.x;
            _this.mpos[1] += _this.y;
            if (_this.in_focus[0] && _this.inBounds(_this.mpos)) {
              return _this.triggerListener(btn[mbtn] + "_CLICK", _this.mpos);
            }
          });
          Halal.addListener("MOUSE_CLICKED", function() {
            _this.mpos[0] += _this.x;
            _this.mpos[1] += _this.y;
            if (_this.in_focus[0] && _this.inBounds(_this.mpos)) {
              return _this.triggerListener("MOUSE_CLICKED", _this.mpos);
            }
          });
          return Halal.addListener("MOUSE_DBL_CLICK", function(mbtn) {
            _this.mpos[0] += _this.x;
            _this.mpos[1] += _this.y;
            if (_this.in_focus[0] && _this.inBounds(_this.mpos)) {
              return _this.triggerListener("MOUSE_DBL_CLICK", _this.mpos);
            }
          });
        }
      };

      return Clickable;

    })();
    return new Clickable();
  });

}).call(this);
