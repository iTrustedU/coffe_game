(function() {
  "use strict";
  define([], function() {
    return {
      addComponent: function(comp) {
        if (comp.init != null) {
          return comp.init.call(this);
        }
      },
      extend: function(obj) {
        var key, _i, _len, _ref;
        if (!obj) {
          return this;
        }
        _ref = Object.keys(obj);
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          key = _ref[_i];
          if (this === obj[key]) {
            continue;
          }
          this[key] = obj[key];
        }
        return this;
      }
    };
  });

}).call(this);
