"use strict"

define [],

() ->
    class DOMManager
        constructor: () ->
            @renderspace        = document.getElementById('renderspace')
            @dom_layer          = document.getElementById('domlayer')
            @viewport           = document.getElementById('viewport')
            @area               = renderspace.getBoundingClientRect()
            @current_zindex     = 1000
            @canvases           = []
            @in_fullscreen      = false
           
            @screen_w           = document.body.clientWidth
            @screen_h           = window.screen.availHeight
            @fullscreen_scale   = [1.5, 1.5]
            #slusaj resolution change
            Hal.addListener "FULLSCREEN_CHANGE", (in_fullscreen) =>
                @area = @renderspace.getBoundingClientRect()
                if in_fullscreen      
                    Hal.r.resize(@screen_w / @fullscreen_scale[0], @screen_h / @fullscreen_scale[1])
                    for c in @canvases
                        c.setAttribute('style', (c.getAttribute('style') || '') + ' ' + '-webkit-transform: scale3d('+@fullscreen_scale[0]+','+@fullscreen_scale[1]+', 1.0); -webkit-transform-origin: 0 0 0;')
                    #Hal.r.canvas.style['width'] = (@area.width / 0.5) + 'px'
                    #Hal.r.canvas.style['height'] = (@area.height / 0.5) + 'px'
                    Hal.scm.enterFullScreen(@fullscreen_scale)
                else
                    @renderspace.style["width"] = Hal.r.prev_bounds[2] + "px"
                    @renderspace.style["height"] = Hal.r.prev_bounds[3] + "px"
                    Hal.r.resize(Hal.r.prev_bounds[2], Hal.r.prev_bounds[3])
                    for c in @canvases
                        c.setAttribute('style', (c.getAttribute('style') || '') + ' ' + '-webkit-transform: scale3d(1.0, 1.0, 1.0); -webkit-transform-origin: 0 0 0;')
                    Hal.scm.exitFullScreen([1, 1])

            Hal.addListener "DOM_ADD", (callb) =>
                if callb?
                    callb.call(null, @dom_layer)
                    
            window.addEventListener("resize", () =>
                @area = @renderspace.getBoundingClientRect()
            )

            document.addEventListener("fullscreenchange", () ->
                @in_fullscreen = not @in_fullscreen
                Hal.triggerListener("FULLSCREEN_CHANGE", @in_fullscreen)
            , false);
            
            document.addEventListener("webkitfullscreenchange", () ->
                @in_fullscreen = not @in_fullscreen
                Hal.triggerListener("FULLSCREEN_CHANGE", @in_fullscreen)
            , false);

            document.addEventListener("mozfullscreenchange", () ->
                @in_fullscreen = not @in_fullscreen
                Hal.triggerListener("FULLSCREEN_CHANGE", @in_fullscreen)
            , false);

            Hal.addListener "REQUEST_FULLSCREEN", (scene) =>
                if not Hal.supports("FULLSCREEN")
                    console.log("fullscreen not available")
                    return
                if not @in_fullscreen
                    @renderspace.style["width"] = @screen_w + "px"
                    @renderspace.style["height"] = @screen_h + "px"
                    @renderspace.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT)

    DOMManager::createCanvas = (width = @area.width, height = @area.height) ->
        canvas         = document.createElement("canvas")
        canvas.width   = width
        canvas.height  = height
        return canvas

    DOMManager::addCanvas = (canvas, x = 0, y = 0, isTransp,  z_index = 1) ->
        canvas.style.left          = x + "px"
        canvas.style.top           = y + "px"
        canvas.style["z-index"]    = @current_zindex + z_index
        if not isTransp
            canvas.style['background-color'] = "white"
        @viewport.appendChild(canvas)
        @canvases.push(canvas)
        @current_zindex            += 1

    return DOMManager