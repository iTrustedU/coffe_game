"use strict"

define () ->
    trig = {}

    if not window.performance?
        window.performance = Date

    class Timer
        constructor: () ->
            @cur_time   = performance.now()
            @delta      = 0
            @triggers   = []
            @trig_len_  = 0

        tick: -> 
            @delta = performance.now() - @cur_time
            @cur_time += @delta
            for trig in @triggers
                trig.tpassed += @delta
                if trig.tpassed >= trig.t
                    trig.clb()
                    trig.tpassed = 0
                    
        each: (time, trigClb) ->
            @trig_len_ = @triggers.push({t: time, clb: trigClb, tpassed: 0}) - 1