(function() {
  "use strict";
  define([], function() {
    return Hal.triggerListener("DOM_ADD", function(domlayer) {
      var div, right, width;
      if (!Hal.supports("FULLSCREEN")) {
        console.log("fullscreen not supported on this device");
      }
      width = 50;
      right = 0;
      div = $("<div/>", {
        css: {
          "background-color": "rgb(94, 190, 255)",
          "opacity": "0.8",
          "text-align": "center",
          "font-size": "0.7em",
          "position": "absolute",
          "width": width + "px",
          "display": "block",
          "right": right + "px",
          "cursor": "pointer"
        }
      });
      div.text("Go fullscreen");
      div.on("mousemove", function(ev) {
        return ev.stopPropagation();
      });
      div.on("mouseup", function(ev) {
        return ev.stopPropagation();
      });
      div.on("click", function(ev) {
        Hal.triggerListener("REQUEST_FULLSCREEN");
        return ev.stopPropagation();
      });
      Hal.addListener("ENTER_FULLSCREEN", function() {
        div.hide();
        return $("#hud").toggle();
      });
      Hal.addListener("EXIT_FULLSCREEN", function() {
        div.show();
        return $("#hud").toggle();
      });
      return $(domlayer).append(div);
    });
  });

}).call(this);
