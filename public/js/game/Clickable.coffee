"use strict"

define ["Halal"], 

(Halal) ->
    class Clickable
        btn = {
            0: "LEFT"
            2: "RIGHT"
        }

        init: () ->
            if (@parent_scene?)
                Halal.addListener("MOUSE_CLICK", 
                    (mbtn) =>
                        p = [@parent_scene.mpos[0] - @camera[0] - @pos[0], @parent_scene.mpos[1] - @camera[1] - @pos[1]]
                        if(@in_focus[0] && @inBounds(p))
                            @triggerListener(btn[mbtn]+"_CLICK", p)
                )
                Halal.addListener("MOUSE_CLICKED", 
                    () =>
                        p = [@parent_scene.mpos[0] - @camera[0] - @pos[0], @parent_scene.mpos[1] - @camera[1] - @pos[1]]
                        if(@in_focus[0] && @inBounds(p))
                            @triggerListener("MOUSE_CLICKED", p)
                )
            else 
                Halal.addListener("MOUSE_CLICK",
                    (mbtn) =>
                        @mpos[0] += @x
                        @mpos[1] += @y
                        if(@in_focus[0] && @inBounds(@mpos))
                            @triggerListener(btn[mbtn]+"_CLICK", @mpos)
                )
                Halal.addListener("MOUSE_CLICKED",
                    () =>
                        @mpos[0] += @x
                        @mpos[1] += @y
                        if(@in_focus[0] && @inBounds(@mpos))
                            @triggerListener("MOUSE_CLICKED", @mpos)
                )
                Halal.addListener("MOUSE_DBL_CLICK", 
                    (mbtn) =>
                        @mpos[0] += @x
                        @mpos[1] += @y
                        if(@in_focus[0] && @inBounds(@mpos))
                            @triggerListener("MOUSE_DBL_CLICK", @mpos)    
                )
    return new Clickable()