(function() {
  "use strict";
  define(["Sprite"], function(Sprite) {
    var SpriteFactory;
    SpriteFactory = {};
    SpriteFactory.clipFromSpriteSheet = function(img, name, cliprect) {
      return new Sprite(Hal.imgutils.clipImage(img, cliprect), name, 0, 0, cliprect.w, cliprect.h);
    };
    SpriteFactory.fromSingleImage = function(img, name) {
      return new Sprite(img, name, 0, 0, img.width, img.height);
    };
    return SpriteFactory;
  });

}).call(this);
