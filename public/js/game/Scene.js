(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["EventDispatcher", "HalalSupport", "Renderer"], function(EventDispatcher, HalalSupport, Renderer, Camera) {
    var Scene;
    Scene = (function(_super) {
      __extends(Scene, _super);

      function Scene(name, bounds, canvas) {
        this.name = name;
        this.bounds = bounds;
        this.canvas = canvas;
        Scene.__super__.constructor.call(this);
        this.paused = false;
        this.id = Hal.UID();
        this.in_focus = [false];
        this.mpos = [0, 0];
        this.depth = 0;
        this.x = this.bounds[0];
        this.y = this.bounds[1];
        this.ents = [];
        this.scale = [];
        this.camera = [0, 0];
        this.free_index = -1;
        this.ent_cache = {};
        if (this.canvas) {
          this.canvas = null;
        } else {
          this.canvas = Hal.r.canvas;
        }
        this.g = new Renderer(this.bounds, this.canvas, this.camera);
        this.initHandlers();
      }

      Scene.prototype.resume = function() {
        this.paused = false;
        this.triggerListener("SCENE_RESUMED", this);
        return console.log("scene resumed");
      };

      Scene.prototype.pause = function() {
        this.paused = true;
        this.triggerListener("SCENE_PAUSED", this);
        return console.log("scene paused");
      };

      Scene.prototype.initHandlers = function() {
        var _this = this;
        this.addListener("RESIZE", function(scale) {
          return console.log(scale);
        });
        this.addListener("ENTER_FULLSCREEN", function(scale) {
          _this.scale = scale;
          console.log(_this.name + " in fullscreen");
          console.log("scale: " + scale[0] + "," + scale[1]);
          return Hal.triggerListener("ENTER_FULLSCREEN");
        });
        this.addListener("EXIT_FULLSCREEN", function(scale) {
          _this.scale = scale;
          console.log(_this.name + " exited fullscreen");
          console.log("scale: " + scale[0] + "," + scale[1]);
          Hal.triggerListener("EXIT_FULLSCREEN");
        });
        return this.addListener("KEY_UP", function(key) {
          if (key.keyCode === Hal.Keys.LEFT) {
            return Hal.triggerListener("SHOW_PREV_SCENE");
          } else if (key.keyCode === Hal.Keys.RIGHT) {
            return Hal.triggerListener("SHOW_NEXT_SCENE");
          }
        });
      };

      Scene.prototype.moveCamera = function(x, y) {
        this.camera[0] = x;
        return this.camera[1] = y;
      };

      Scene.prototype.update = function() {};

      Scene.prototype.inBounds = function(point) {
        return Hal.m.isPointInRect(point, this.bounds);
      };

      Scene.prototype.drawBounds = function() {
        return this.g.strokeRect(this.bounds);
      };

      Scene.prototype.zoom = function(scaleX, scaleY) {};

      Scene.prototype.addEntity = function(ent) {
        if (this.exists(ent.id)) {
          return;
        }
        ent.parent_scene = this;
        ent.g = this.g;
        ent.addCamera(this.camera);
        ent.in_focus = this.in_focus;
        ent.init();
        this.ent_cache[ent.id] = true;
        this.ents.push(ent);
        return ent;
      };

      Scene.prototype.removeEntity = function(ent) {
        var ind;
        ind = this.ents.indexOf(ent);
        if (ind !== -1) {
          this.ent_cache[ent.id] = false;
          this.ents[ind] = null;
          return this.ents.splice(ind);
        }
      };

      Scene.prototype.exists = function(id) {
        return this.ent_cache[id];
      };

      Scene.prototype.move = function(x, y) {
        this.bounds[0] = x;
        this.bounds[1] = y;
        this.x = x;
        this.y = y;
        this.g.offsetX = x;
        return this.g.offsetY = y;
      };

      Scene.prototype.addComponent = HalalSupport.addComponent;

      Scene.prototype.extend = HalalSupport.extend;

      return Scene;

    })(EventDispatcher);
    return Scene;
  });

}).call(this);
