"use strict"

define ["Vec2"], 

(Vec2) ->
    
    MathUtil = {
        MAT_ARRAY: if (typeof Float32Array != 'undefined') then Float32Array else Array;
        epsilon: 0.000001
    }

    MathUtil.createRegularon = (numsides, sidelen) ->
        out = []
        ang_step = (Math.PI * 2) / numsides
        ang = 0

        for t in [0..numsides-1]
            x = sidelen * Math.cos(ang)
            y = sidelen * Math.sin(ang)
            out.push([x,y])
            ang += ang_step
        return out

    MathUtil.toDegrees = (radians) ->
        return radians * 180 / Math.PI

    MathUtil.isPointInRect = (p, rect) ->
        return p[0] > rect[0] && p[0] < (rect[0] + rect[2]) && p[1] > rect[1] && p[1] < (rect[1] + rect[3])
    
    MathUtil.doLinesIntersect = (x1,y1,x2,y2) ->
        ###
            Due to numerical instability, epsilon hack is necessarry 
        ###
        rtop = (x1[1] - x2[1]) * (y2[0] - x2[0]) - (x1[0] - x2[0]) * (y2[1] - x2[1])
        stop = (x1[1] - x2[1]) * (y1[0] - x1[0]) - (x1[0] - x2[0]) * (y1[1] - x1[1])
        bott = (y1[0] - x1[0]) * (y2[1] - x2[1]) - (y1[1] - x1[1]) * (y2[0] - x2[0])

        if bott == 0
            return false

        invbott = 1 / bott
        r = rtop * invbott
        s = stop * invbott

        if ((r > 0) and (r < 1) and (s > 0) and (s < 1))
            return true

        return false

    MathUtil.isPointInPoly = (p, points) ->
        e1 = [-10000, p[1]]
        e2 = p
        hits = 0
        len = points.length
        for i in [0..len - 1]
            if ((@doLinesIntersect(e1, e2, points[i], points[(i+1) % len])))
                hits++
        return ((hits % 2) != 0)

    MathUtil.projectPointOnLine = (pt, a, b) ->
        vecAB = Vec2.sub([], b, a)
        vecAC = Vec2.sub([], pt, a)
        Vec2.normalize(vecAB, vecAB)
        Vec2.normalize(vecAC, vecAC)
        dotProd = Vec2.dot(vecAC, vecAB)
        lenAC = Vec2.distance(a, pt)
        vecCProj = Vec2.scale([], vecAB, dotProd * lenAC)
        vecCProj = Vec2.fromValues(a[0] + vecCProj[0], a[1] + vecCProj[1])
        return vecCProj

    MathUtil.perpDistance = (pt, a, b) ->
        c = @projectPointOnLine(pt, a, b)
        return Vec2.distance(pt, c)

    pointComparison = (a, b, center) ->
        if (a[0] >= 0 and b[0] < 0)
            return true
        if (a[0] == 0 and b[0] == 0)
            return a[1] > b[1]

        det = (a[0] - center[0]) * (b[1] - center[1]) - (b[0] - center[0]) * (a[1] - center[1])

        if (det < 0)
            return true
        if (det > 0)
            return false

        #they are on the same line 
        d1 = (a[0] - center[0]) * (a[0] - center[0]) + (a[1] - center[1]) * (a[1] - center[1])
        d2 = (b[0] - center[0]) * (b[0] - center[0]) + (b[1] - center[1]) * (b[1] - center[1])

        return d1 > d2

    return MathUtil