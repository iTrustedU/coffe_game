(function() {
  "use strict";
  var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  define([], function() {
    var DOMEventManager;
    DOMEventManager = (function() {
      function DOMEventManager() {
        this.getMousePos = __bind(this.getMousePos, this);
        this.mouseDown = __bind(this.mouseDown, this);
        this.mouseUp = __bind(this.mouseUp, this);
        this.mouseMove = __bind(this.mouseMove, this);
        this.mouseClick = __bind(this.mouseClick, this);
        this.mouseDblClick = __bind(this.mouseDblClick, this);
        this.keyUp = __bind(this.keyUp, this);
        this.keyDown = __bind(this.keyDown, this);
        this.wheelMoved = __bind(this.wheelMoved, this);
        this.viewport = null;
        this.prev_area = null;
        this.mouse_leftbtn_down = false;
        this.mouse_rightbtn_down = false;
        this.can_drag = true;
        this.pos = [0, 0];
        this.viewport = Hal.dom.renderspace;
        this.prev_area = Hal.dom.area;
        this.domlayer = Hal.dom.dom_layer;
        this.dragging = false;
        this.viewport.addEventListener("mousedown", this.mouseDown);
        this.viewport.addEventListener("mouseup", this.mouseUp);
        this.viewport.addEventListener("mousemove", this.mouseMove);
        this.viewport.addEventListener("onmousewheel", this.wheelMoved);
        this.viewport.addEventListener("onContextMenu", function() {
          return false;
        });
        this.viewport.addEventListener("mousewheel", this.wheelMoved);
        window.addEventListener("keydown", this.keyDown);
        window.addEventListener("keyup", this.keyUp);
        this.viewport.addEventListener("click", this.mouseClick);
        this.viewport.addEventListener("dblclick", this.mouseDblClick);
      }

      DOMEventManager.prototype.wheelMoved = function(evt) {
        this.getMousePos(evt);
        if (evt.wheelDelta < 0) {
          return Hal.triggerListener("SCROLL_DOWN", this.pos);
        } else {
          return Hal.triggerListener("SCROLL_UP", this.pos);
        }
      };

      DOMEventManager.prototype.keyDown = function(evt) {
        if (!(this.domlayer.querySelectorAll(':hover').length > 0)) {
          this.getMousePos(evt);
          return Hal.triggerListener("KEY_DOWN", evt);
        }
      };

      DOMEventManager.prototype.keyUp = function(evt) {
        this.getMousePos(evt);
        return Hal.triggerListener("KEY_UP", evt);
      };

      DOMEventManager.prototype.mouseDblClick = function(evt) {
        this.getMousePos(evt);
        return Hal.triggerListener("MOUSE_DBL_CLICK", this.pos);
      };

      DOMEventManager.prototype.mouseClick = function(evt) {
        this.getMousePos(evt);
        return Hal.triggerListener("MOUSE_CLICKED", this.pos);
      };

      DOMEventManager.prototype.mouseMove = function(evt) {
        if (!(this.domlayer.querySelectorAll(':hover').length > 0)) {
          this.getMousePos(evt);
          Hal.triggerListener("MOUSE_MOVE", this.pos);
          if (this.mouse_leftbtn_down && (!this.dragging && this.can_drag)) {
            Hal.triggerListener("DRAG_START", this.pos);
            this.dragging = true;
            return this.can_drag = false;
          }
        }
      };

      DOMEventManager.prototype.mouseUp = function(evt) {
        this.getMousePos(evt);
        if (this.mouse_rightbtn_down && !this.dragging) {
          Hal.triggerListener("MOUSE_CLICK", 2);
          this.mouse_rightbtn_down = false;
        } else if (this.mouse_leftbtn_down && !this.dragging) {
          Hal.triggerListener("MOUSE_CLICK", 0);
          this.mouse_leftbtn_down = false;
        }
        if (this.dragging) {
          this.dragging = false;
          Hal.triggerListener("DRAG_END", this.pos);
          this.can_drag = true;
          return this.mouse_leftbtn_down = false;
        }
      };

      DOMEventManager.prototype.mouseDown = function(evt) {
        this.getMousePos(evt);
        if (evt.button === 0) {
          return this.mouse_leftbtn_down = true;
        } else if (evt.button === 2) {
          return this.mouse_rightbtn_down = true;
        }
      };

      DOMEventManager.prototype.getMousePos = function(evt) {
        this.pos[0] = evt.clientX - Hal.dom.area.left;
        return this.pos[1] = evt.clientY - Hal.dom.area.top;
      };

      return DOMEventManager;

    })();
    return DOMEventManager;
  });

}).call(this);
