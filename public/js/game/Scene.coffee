"use strict"

define ["EventDispatcher", "HalalSupport", "Renderer"], 

(EventDispatcher, HalalSupport, Renderer, Camera) ->
    class Scene extends EventDispatcher
        constructor: (@name, @bounds, @canvas) ->
            super()

            @paused     = false
            @id         = Hal.UID()
            @in_focus   = [false]
            @mpos       = [0, 0]
            @depth      = 0
            @x          = @bounds[0]
            @y          = @bounds[1]
            @ents       = []
            @scale      = []
            @camera     = [0, 0]
            @free_index = -1
            @ent_cache  = {}

            if @canvas 
                @canvas = null
            else 
                @canvas = Hal.r.canvas

            @g = new Renderer(@bounds, @canvas, @camera)
            @initHandlers() 

        resume: () ->
            @paused = false
            @triggerListener("SCENE_RESUMED", @)

            console.log("scene resumed")

        pause: () ->
            @paused = true
            @triggerListener("SCENE_PAUSED", @)

            console.log("scene paused")

        initHandlers: () ->
            #mozda ovako implicitno korisnik ne bi voleo da se desi
            #al meni se cini kao normalan behavior
            #crtaj po default canvasu
            @addListener "RESIZE", (scale) =>
                console.log(scale)

            @addListener "ENTER_FULLSCREEN", (scale) =>
                @scale = scale
                console.log(@.name + " in fullscreen")
                console.log("scale: " + scale[0] + "," + scale[1])
                Hal.triggerListener "ENTER_FULLSCREEN"

            @addListener "EXIT_FULLSCREEN", (scale) =>
                @scale = scale
                console.log(@.name + " exited fullscreen")
                console.log("scale: " + scale[0] + "," + scale[1])
                Hal.triggerListener "EXIT_FULLSCREEN"
                return

            @addListener "KEY_UP", (key) ->
                if key.keyCode == Hal.Keys.LEFT
                    Hal.triggerListener "SHOW_PREV_SCENE"
                else if key.keyCode == Hal.Keys.RIGHT
                    Hal.triggerListener "SHOW_NEXT_SCENE"

        moveCamera: (x, y) ->
            @camera[0] = x
            @camera[1] = y

        update: () ->
            return

        inBounds: (point) ->
            return Hal.m.isPointInRect(point, @bounds)

        drawBounds: () ->
            @g.strokeRect(@bounds)

        zoom: (scaleX, scaleY) ->
            return

        addEntity: (ent) ->
            if @exists(ent.id)
                return
            ent.parent_scene = @
            ent.g = @g
            ent.addCamera(@camera)
            ent.in_focus = @in_focus
            ent.init()
            # if @free_index != -1
            #     @ents[@free_index] = ent
            #     @free_index = -1
            # else
            #     
            @ent_cache[ent.id] = true
            @ents.push(ent)
            return ent

        removeEntity: (ent) ->
            ind = @ents.indexOf(ent)
            if ind != -1
                #@free_index = ind
                @ent_cache[ent.id] = false
                @ents[ind] = null
                @ents.splice(ind)

        exists: (id) ->
            return @ent_cache[id]

        move: (x, y) ->
            @bounds[0] = x
            @bounds[1] = y
            @x = x
            @y = y
            @g.offsetX = x
            @g.offsetY = y

        addComponent: HalalSupport.addComponent
        extend: HalalSupport.extend



    return Scene