$(document).ready(function() {
	$(".build-tab").click(function() {
		$(".friends").hide();
		$(".buildings").show();
	});
	$(".friends-tab").click(function() {
		$(".buildings").hide();
		$(".friends").show();
	});
	$(function() {
		$( ".chat" ).resizable();
	});
	$(".tab-build").click(function() {
		$(".build-box").toggle();
		$(".tab-build").toggleClass("tab-build-click");
		$(".research-box").hide();
		$(".tab-research").removeClass("tab-research-click");
		$(".training-box").hide();
		$(".tab-training").removeClass("tab-training-click");
		$(".tab-training").removeClass("tab-training2");
	});
	$(".tab-research").click(function() {
		$(".research-box").toggle();
		$(".tab-research").toggleClass("tab-research-click");
		$(".build-box").hide();
		$(".tab-build").removeClass("tab-build-click");
		$(".training-box").hide();
		$(".tab-training").removeClass("tab-training-click");
		$(".tab-training").toggleClass("tab-training2");
	});
	$(".tab-training").click(function() {
		$(".training-box").toggle();
		$(".tab-training").toggleClass("tab-training-click");
		$(".build-box").hide();
		$(".tab-build").removeClass("tab-build-click");
		$(".research-box").hide();
		$(".tab-research").removeClass("tab-research-click");
		$(".tab-training").removeClass("tab-training2");
	});
});

