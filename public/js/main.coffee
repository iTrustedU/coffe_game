"use strict"

require ["Halal", "Scene", "Clickable", "Entity"], 

(Halal, Scene, Clickable, Entity) ->
    
    #AssetManager = new AssetManager()
    #console.log(Hal)

    # class SimpleScene extends Scene
    #     constructor: (name, bounds, newcanvas, @color) ->
    #         super(name, bounds, newcanvas)

    #     init: () ->
    #         @addComponent(Clickable)
    #         @addListener "MOUSE_CLICK", (pos) ->
    #             console.log(pos)
    #             Hal.scm.putOnTop(@)


    #     update: (delta) ->
    #         #@g.clear()
    #         @g.ctx.fillStyle = @color
    #         if @in_focus[0]
    #            @g.ctx.fillStyle = "#ccc"
    #         @g.fillRect([0, 0, @w, @h])

    #         @ents.update(delta)

    # class SpriteEntity extends Entity
    #     constructor: (x, y, @sprite) ->
    #         super(x, y, [x, y, @sprite.w, @sprite.h])

    #     init: () ->
    #         @addComponent(Clickable)
    #         @addListener "MOUSE_CLICK", (pos) ->
    #             alert()

    #     update: (delta) ->
    #         @g.clearRect([@x, @y, @bounds[2], @bounds[3]])
    #         @g.drawSprite(@sprite, @x, @y)


    # Hal.addListener("ENGINE_STARTED", 
    #     (x) ->
    #         console.log("endzin startovan")
    #         require ["FullscreenButton"], () ->
    #         #require()
    #             zelena = new SimpleScene("Obicna1", [1, 1, 301, 301], false, "green")
    #             crvena = new SimpleScene("Obicna2", [51, 51, 301, 301], false, "red")
    #             plava = new SimpleScene("Obicna3", [100, 0, 301, 301], false, "blue")

    #             plava.addEntity(new SpriteEntity(10, 10, Hal.asm.getSprite("unordered_beze_tile_64x32")))
    #             crvena.addEntity(new SpriteEntity(10, 50, Hal.asm.getSprite("unordered_beze_tile_64x32")))
    #             zelena.addEntity(new SpriteEntity(10, 10, Hal.asm.getSprite("unordered_beze_tile_64x32")))   
    #             crvena.addEntity(new SpriteEntity(100, 10, Hal.asm.getSprite("unordered_beze_tile_64x32")))

    #             #crvena.move(0, 100)

    #             Hal.scm.addScene(zelena)
    #             Hal.scm.addScene(crvena)
    #             Hal.scm.addScene(plava)

    #             Halal.addListener("DRAG", (pos) =>
    #                 crvena.move(pos[0] - crvena.w/2, pos[1] - crvena.h/2)
    #             )
    #         #require(["../amjad/AmjadMap"], 
    #         #    (amjad) ->
    #         #        amjad.activate()
    #         #)
    # )


    #Hal.triggerListener("ON_LOAD")

    # AssetManager.addListener("SPRITESHEETS_LOADED", 
    #     (x) ->
    #         #console.log("spritesheets loaded")
    #         #console.log(x)
    # )

    # AssetManager.addListener("SPRITESHEET_LOADING", 
    #     (x) ->
    #         #console.log(x)
    #         #console.log(@)
    # )

    # AssetManager.addListener("SPRITE_LOADED", 
    #     (x) ->
    #         #console.log(x)
    #         #console.log(@)
    # )

    # AssetManager.addListener("SPRITESHEET_LOADED", 
    #     (x) ->
    #         #console.log(x)
    #         #console.log(@)
    # )

    # AssetManager.addListener("IMAGE_LOADED", 
    #     (x) ->
    #         #console.log("skinuo sliku: " + x.src)
    #         #console.log(x);
    #         #bla = BBoxResolver.find_edges(x.msg)
    # )
 
    # AssetManager.addListener("IMAGES_LOADED",
    #     (x) ->
    #         #console.log("sve slike skinute")
    #         #console.log(x)
    #         #Hal.pause()
    #         #Hal.resume()
    # )


    #ovde loadujem "pluginove"
    $(document).ready () ->
        require ["MapEditor"], () ->
            #load all shit from .list files
            Hal.asm.loadSpritesFromList().then () ->
                Hal.asm.loadSpritesheets("horses.json, djorses.json").then () ->
                    Hal.start()

    Hal.addListener "ENGINE_STARTED", () ->
        require ["FullscreenButton", "../amjad/Amjad", "../amjad/NovaScena", "../amjad/FOFScene"] , (fs, Amjad, Nova, FOFScene) ->
            nova = new Nova("nova scena", Hal.r.bounds, false)
            Hal.scm.addScene(nova)
            amjad = new Amjad("amjad", Hal.r.bounds, false)
            Hal.scm.addScene(amjad)
            fof = new FOFScene("fofscene", Hal.r.bounds, false)
            Hal.scm.addScene(fof)

            Hal.scm.putOnTop(amjad)
           # Hal.scm.putOnTop(nova)
    # AssetManager.loadImages([
    #     "beze_tile_12.png", 
    #     "beze_tile_1.png", 
    #     "beze_tile_1.png", 
    #     "beze_tile_13.png",
    #     "trees_01.png",
    #     "dune02.png",
    #     "well_PH.png",
    #     "sand01.png",
    #     "water01.png",
    #     "dune01.png",
    #     "cliff_01.png"
    # ])
    #AssetManager.loadSprite("beze_tile_1.png")

    #AssetManager.loadSpritesheets("horses.json, djorses.json")
