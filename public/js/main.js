(function() {
  "use strict";
  require(["Halal", "Scene", "Clickable", "Entity"], function(Halal, Scene, Clickable, Entity) {
    $(document).ready(function() {
      return require(["MapEditor"], function() {
        return Hal.asm.loadSpritesFromList().then(function() {
          return Hal.asm.loadSpritesheets("horses.json, djorses.json").then(function() {
            return Hal.start();
          });
        });
      });
    });
    return Hal.addListener("ENGINE_STARTED", function() {
      return require(["FullscreenButton", "../amjad/Amjad", "../amjad/NovaScena", "../amjad/FOFScene"], function(fs, Amjad, Nova, FOFScene) {
        var amjad, fof, nova;
        nova = new Nova("nova scena", Hal.r.bounds, false);
        Hal.scm.addScene(nova);
        amjad = new Amjad("amjad", Hal.r.bounds, false);
        Hal.scm.addScene(amjad);
        fof = new FOFScene("fofscene", Hal.r.bounds, false);
        Hal.scm.addScene(fof);
        return Hal.scm.putOnTop(amjad);
      });
    });
  });

}).call(this);
