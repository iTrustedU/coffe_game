"use strict"

###
    valjalo bi pomeranje kamere izdvojiti u scenu
    da izbegnem dupliranje koda na nekim mestima
###
define [
    "Scene", 
    "Clickable", 
    "Draggable", 
    "Vec2", 
    "Entity", 
    "../amjad/tiles/Tile", 
    "Keyboard", 
    "Scrollable", 
    "../amjad/MapLoader", 
    "PathFinder", 
    "Renderer"
    ],

(Scene, Clickable, Draggable, Vec2, Entity, Tile, Keyboard, Scrollable, MapLoader, PathFinder, Renderer) ->
    class AmjadMap extends Scene
        constructor: (name, bounds, newcanvas) ->
            super("amjad", bounds, newcanvas)

            @tiles          = []
            @margin_right   = 5
            @margin_bottom  = 5
            @numrows        = 20
            @numcols        = 20

            @tile_dim       = [128, 64]
            @prev_focused   = null

            @zoom_in_factor   = 0
            @zoom_out_factor  = 0

            @scale_step       = 0.05
            @current_zoom     = 1

            @tilew2prop     = 2 / @tile_dim[0]
            @tileh2prop     = 2 / @tile_dim[1]
            @tilew2         = @tile_dim[0] / 2
            @tileh2         = @tile_dim[1] / 2
            @offsetX        = 0
            @offsetY        = 0
            @grid           = []
            @shift_pressed  = false
            @can_place      = false

            @current_layer      = null
            @selected_sprite    = null

            @current_tile       = {}

            @current_tile_meta  = {
                name: "not selected"
            }

            @sand           = Hal.asm.getSprite("unordered_sand03")
            @mask           = Hal.asm.getSprite("unordered_tilemask_128x64")
            @over           = {
                "green": Hal.asm.getSprite("unordered_grid_unit_over_green_128x64")
                "red": Hal.asm.getSprite("unordered_grid_unit_over_red_128x64")
            }
            @current_tile_sprite = @over["green"]
            @current_tile_h = 0
            @current_tile_w = 0

            @hittest        = Hal.dom.createCanvas(@tile_dim[0], @tile_dim[1]).getContext("2d")
            @hittest.drawImage(@mask.img, 0, 0)
            @mask_data      = @hittest.getImageData(0, 0, @tile_dim[0], @tile_dim[1]).data

            @show_grid      = false

            @center_point   = [@bounds[2]/2, @bounds[3]/2]

            @from_row       = Math.max(0, Math.round(@camera[0] / @tile_dim[1]) - 1)
            @from_col       = Math.max(0, Math.round(@camera[1] / @tile_dim[0] * 2) - 1)
            @max_cols       = Math.min(@numcols-1, Math.floor(@bounds[2] / (@tile_dim[0] / 2) + 2))
            @max_rows       = Math.min(@numrows-1, Math.ceil(@bounds[3] / @tile_dim[1]) + 1)
            @cur_clicked    = null

            @over_coord     = [0, 0]
            @right_tile     = null

            @lerp           = false
            @lerp_to        = []

            @cur_area       = []
            @wait_frames    = 10

            for i,j in @mask_data
                @mask_data[j] = i < 120

            @camera_prev_pos = @camera.slice()
            @start_drag_point = [0, 0]

            for i in [0..@numrows-1]
                for j in [0..@numcols-1]
                    x = (j / 2) * @tile_dim[0] + @camera[0]
                    y = (i + ((j % 2) / 2)) * @tile_dim[1] + @camera[1]
                    @grid.push(@addEntity(new Tile(x, y, i, j, @tile_dim)))

            #@grid_length = @grid.length - 1

            @prev_selected = null
            @selected_arr = []

            @prev_area = []
            @focused = null

            @layers = {
                terrain: 0
                decor: 1
                buildings: 2
                poi: 3
                character: 1
            }

            @total = 0
            @camera_speed = 0.2
            @moveable = false

            @info = {
                row: "row: "
                col: "col: "
                tilename: "tile: "
                layer: "layer: "
                fps: "fps: "
                start_row: "starting row: "
                start_col: "staring col: "
                tile_x: "tile_x: ",
                tile_y: "tile_y: ",
                camera_x: "camera_x: ",
                camera_y: "camera_y: ",
                num_rendering: "no. rendereded entities: ",
                lerp_x: "lerp_x: ",
                lerp_y: "lerp_y: "
            }

            console.log("max columns: " + @max_cols)
            console.log("max rows: " + @max_rows)
            console.log("grid length: " + @grid_length)
            console.log("max that can be seen: " + ((@max_cols+1) * (@max_rows+1)))

            @character_selected = null

            @buffers = {
                del: {}
                path: {}
            }

            @delete_mode = false

            @pathfinder = new PathFinder(@)

            terrain_layer = Hal.dom.createCanvas()
            Hal.dom.addCanvas(terrain_layer,0,0,true,1)
            decor_layer = Hal.dom.createCanvas()
            Hal.dom.addCanvas(decor_layer,0,0,true,2)
            buildings_layer = Hal.dom.createCanvas()
            Hal.dom.addCanvas(buildings_layer,0,0,true,3)
            poi_layer = Hal.dom.createCanvas()
            Hal.dom.addCanvas(poi_layer,0,0,true,4)
            @layer_renderers = [
                new Renderer([0, 0, Hal.dom.area.width, Hal.dom.area.height], terrain_layer, @camera), 
                new Renderer([0, 0, Hal.dom.area.width, Hal.dom.area.height], decor_layer, @camera),
                new Renderer([0, 0, Hal.dom.area.width, Hal.dom.area.height], buildings_layer, @camera),
                new Renderer([0, 0, Hal.dom.area.width, Hal.dom.area.height], poi_layer, @camera)
            ]
            
            @terrain_layer_ctx = terrain_layer.getContext("2d")
            @decor_layer_ctx = decor_layer.getContext("2d")
            @buildings_layer_ctx = buildings_layer.getContext("2d")
            @poi_layer_ctx = poi_layer.getContext("2d")

    #ovo sam na najbolesniji nacin ikada tako skratio i ustimao
    AmjadMap::toOrtho = (pos) ->
        coldiv  = (pos[0] * @tilew2prop)
        rowdiv  = (pos[1] * @tileh2prop)
        off_x   = ~~(pos[0] - ~~(coldiv * 0.5) * @tile_dim[0])
        off_y   = ~~(pos[1] - ~~(rowdiv * 0.5) * @tile_dim[1])
        transp  = @mask_data[(off_x + @tile_dim[0] * off_y) * 4 + 3]
        return [
            coldiv - (transp ^ !(coldiv & 1)),
            (rowdiv - (transp ^ !(rowdiv & 1))) / 2
        ]

    AmjadMap::putAt = (pos, meta, overwrite) ->            
        t = @getTileAt(pos)
        if not t?
           return
        t.addLayer(@current_layer, meta, @current_tile_h, @current_tile_w, @layer_renderers[@current_layer])

    AmjadMap::save = () ->
        map = []
        for g in @grid
            if g?
                map.push(g.save())
        return map

    AmjadMap::load = (data) -> 
        @pause()
        for g in @grid
            for l in g.layers
                if l?
                    g.removeLayer(l.layer)

            @removeEntity(g)
            #@removeEntity(g)
            #g = null
            # setTimeout(
            #     () =>
            #         @removeEntity(g)
            #         setTimeout()
            # , 10)
        @grid = []

        size = data.name.match(/(\d*x\d*)/g)[0].split("x")
        @numrows = +size[0]
        @numcols = +size[1]
        console.log(@numrows)
        console.log(@numcols)
        @cur_clicked = null
        @selected = null
        #NProgress.configure({ trickleSpeed: 50 });
        NProgress.configure({ ease: 'linear', speed: 100, trickle: false });

        NProgress.start()
        NProgress.configure({ ease: 'linear', speed: 100, trickle: false });

        #NProgress.configure({ trickleSpeed: 50 });
        setTimeout(
            () => 
                @loadTile(0, data)
        , 10)

        #@grid_length = @grid.length - 1
        return

    AmjadMap::canBePlacedOn = (span, layer) ->
        for k in span
            if(k.layers[layer]?)
                if k.layers[layer].is_partial or k.layers[layer].is_root
                    return false
        return true

    AmjadMap::getTile = (row, col, dir=[0,0]) ->
        return @grid[(col+dir[1]) + (row+dir[0]) * @numcols]
    
    AmjadMap::getNeighbours = (tile) ->
        out = []
        for dir in Object.keys(tile.direction)
            n = @getTile(tile.row, tile.col, tile.direction[dir])
            if n?
                out.push(n)
        return out

    AmjadMap::getReachableNeighbours = (tile) ->
        out = []
        for dir in Object.keys(tile.direction)
            n = @getTile(tile.row, tile.col, tile.direction[dir])
            if n? and n.layers[0] and n.num_layers == 1 and not n.is_partial
                out.push(n)
        return out

        return tile.merged_span

    AmjadMap::tintNeighbours = (tile, t) ->
        for n in @getNeighbours(tile)
            n.tint(t)
        return

    AmjadMap::tintWithNeighbours = (tile, t) ->
        for n in @getNeighbours(tile)
            n.tint(t)
        @getTile(tile.row, tile.col).tint(t)
        return

    AmjadMap::getTileAt = (pos) ->
        coord = @toOrtho([pos[0] - @camera[0], pos[1] - @camera[1]])
        if (coord[0] < 0.0 || coord[1] < 0.0 || coord[1] >= @numrows || coord[0] >= @numcols)
            return null
        return @grid[Math.floor(coord[0]) + Math.floor(coord[1]) * @numcols]

    AmjadMap::findInDirectionOf = (tile, dirstr, len) ->
        if not tile?
            return []
        out = []
        out.push(tile)
        fromr = tile.row
        fromc = tile.col
        dir = tile.direction[dirstr]
        while len > 0
            t = @getTile(fromr, fromc, dir)
            if t?
                out.push(t)
                fromr = t.row
                fromc = t.col
                dir = t.direction[dirstr]
            else
                break
            len--
        return out

    ###
        Ovo definitivno moze da se optimizuje tako da 
        izbegnem pretragu nemarkiranih polja
    ###
    AmjadMap::getSpanArea = (tile, size) ->
        size    = size.toString(2).slice(1);
        sizelen = size.length - 1;
        span    = ~~(Math.log(size.length)/Math.LN2)
        nwests  = @findInDirectionOf(tile, "northwest", span - 1)
        out     = []
        for w in nwests
            neasts = @findInDirectionOf(w, "northeast", span - 1)
            out = out.concat(neasts)
        out = out.filter ($, i) ->
            return +size[sizelen-((i%span)*span)-~~(i/span)]
        return out

    AmjadMap::update = () ->
        @decor_layer_ctx.clearRect(0, 0, Hal.dom.area.width, Hal.dom.area.height)
        @terrain_layer_ctx.clearRect(0, 0, Hal.dom.area.width, Hal.dom.area.height)
        @buildings_layer_ctx.clearRect(0, 0, Hal.dom.area.width, Hal.dom.area.height)
        @poi_layer_ctx.clearRect(0, 0, Hal.dom.area.width, Hal.dom.area.height)
                
        @total = 0
        for i in [0..@max_rows]
            for j in [0..@max_cols]
                tile = @grid[(j+@from_col) + (i+@from_row)*@numcols]
                if not tile?
                    continue
                tile.update()
                @total++

        if @focused?
            @g.drawImage(@over["green"].img, @focused.pos[0], @focused.pos[1])
        
        @drawInfo()

    AmjadMap::drawInfo = () ->
        if @focused?
            @g.ctx.fillText(@info.row + @focused.row, 0, 10)
            @g.ctx.fillText(@info.col + @focused.col, 0, 20)
            @g.ctx.fillText(@info.tile_x + @focused.pos[0], 0, 80)
            @g.ctx.fillText(@info.tile_y + @focused.pos[1], 0, 90)

        @g.ctx.fillText(@info.camera_x + @camera[0], 0, 100)
        @g.ctx.fillText(@info.camera_y + @camera[1], 0, 110)
        @g.ctx.fillText(@info.num_rendering + @total, 0, 120)
        @g.ctx.fillText(@info.lerp_x + @lerp_to[0], 0, 130)
        @g.ctx.fillText(@info.lerp_y + @lerp_to[1], 0, 140)
        @g.ctx.fillText(@info.tilename + @current_tile_meta.name, 0, 30)
        @g.ctx.fillText(@info.layer + @current_layer, 0, 40)
        @g.ctx.fillText(@info.fps + Hal.fps, 0, 50)
        @g.ctx.fillText(@info.start_row + @from_row, 0, 60)
        @g.ctx.fillText(@info.start_col + @from_col, 0, 70)

    AmjadMap::loadTile = (i, data) ->
        if i >= data.map.length
            NProgress.set(1.0)
            NProgress.done(true)
            @resume()
            console.log("completed....")
            return
        else 
            g = data.map[i]
            t = new Tile(g.x, g.y, g.row, g.col, @tile_dim)
            @grid.push(@addEntity(t))
            if (g.layers != null)
                t.load(g.layers)
            setTimeout(
                () => 
                    if (i % @numcols) == 0
                        #console.log(i/data.map.length)
                        NProgress.set(i/data.map.length)
                    @loadTile(i+1, data)
            , 0)

    AmjadMap::drawSpan = (span, color) ->
        for s in span
            @g.drawImage(@over[color].img, s.pos[0], s.pos[1])

    AmjadMap::showTile = () ->
        skeep = 0
        if not @showtile?
            @showtile = 
                Hal.addListener "ENTER_FRAME", (delta) =>
                    if @shift_pressed and @focused    
                        if skeep % 2
                            @cur_area = @getSpanArea(@focused, @current_tile_meta.size)
                        if @canBePlacedOn(@cur_area, @current_layer)
                            @drawSpan(@cur_area, "green")
                            @can_place = true
                        else
                            @drawSpan(@cur_area, "red")
                            @can_place = false
                        @g.drawImage(
                            @current_tile_sprite.img, 
                            @over_coord[0] - @current_tile_w, 
                            @over_coord[1] - @current_tile_h
                        )                            
                        skeep = not skeep % 2
                    else
                        console.log("nice nice")
                        Hal.removeListener("ENTER_FRAME", @showtile)
                        @showtile = null 

    AmjadMap::lerpCameraTo = (pos) ->
        @lerp_to[0] = @center_point[0] - @tilew2 - pos[0]
        @lerp_to[1] = @center_point[1] - @tileh2 - pos[1]
        if not @lerp_anim
            @lerp_anim = Hal.addListener("ENTER_FRAME", (delta) =>
                if ~~Math.abs(@camera[0] - @lerp_to[0]) < 1 and ~~Math.abs(-@camera[1] + @lerp_to[1]) < 1
                    Hal.removeListener("ENTER_FRAME", @lerp_anim)
                    @lerp_anim = null
                else 
                    #@camera[0] = @lerp_to[0] 
                    Vec2.lerp(@camera, @camera.slice(), @lerp_to, delta / @camera_speed)
                    @from_col = Math.max(0, -Math.round(@camera[0] / @tile_dim[1]) - 2)
                    @from_row = Math.max(0, -Math.ceil(@camera[1] / @tile_dim[0] * 2) - 1)  
            )

    AmjadMap::lerpCharacterOnPath = (char, path) ->
        if not path?
            return
        out = []
        for p in path 
            tile = @getTile(p.row, p.col)
            out.unshift(tile.pos)
        char.followPath(out)

    AmjadMap::init = () ->
        @addComponent(Clickable)
        @addComponent(Draggable)
        @addComponent(Keyboard)
        @addComponent(Scrollable)

        @addListener "MOUSE_MOVE", (pos) ->
            @prev_focused = @focused
            @focused = @getTileAt([pos[0], pos[1]])
            if @focused?
                @over_coord = @focused.pos
            if @moveable and @selected? and @drag_started
                #@selected.pos[0] =  @selected.pos[0] + (@camera[0] + pos[0])#) + @selected.pos[0]
                # @selected.pos[1] = @pos[1] - @start_drag_point[1])
            else if @drag_started
                if @shift_pressed
                   @triggerListener "LEFT_CLICK", pos
                else if @delete_mode
                    @triggerListener "LEFT_CLICK", pos
                else 
                    @camera[0]  = (@camera_prev_pos[0] + (pos[0] - @start_drag_point[0]))
                    @camera[1]  = (@camera_prev_pos[1] + (pos[1] - @start_drag_point[1]))
                    @from_row   = Math.max(0, Math.round(-@camera[1] / @tile_dim[1] - 1))
                    @from_col   = Math.max(0, Math.ceil((-@camera[0] * @tilew2prop) - 3))


        @addListener "TILE_CLICKED", (tile) ->
            if not tile?
                return
            if not @cur_clicked
                @cur_clicked = tile
            else 
                if (tile.parent_tile.col == @cur_clicked.parent_tile.col) and (tile.parent_tile.row == @cur_clicked.parent_tile.row)
                    if @layers[tile.level] > @layers[@cur_clicked.level]
                        @cur_clicked = tile
                if (tile.parent_tile.row == @cur_clicked.parent_tile.row)
                    if (tile.h + tile.pos[1] > @cur_clicked.h + @cur_clicked.pos[1])
                        @cur_clicked = tile
                if (tile.parent_tile.col == @cur_clicked.parent_tile.col)
                    if (tile.h + tile.pos[1] > @cur_clicked.h + @cur_clicked.pos[1])
                        @cur_clicked = tile
                if (tile.parent_tile.col != @cur_clicked.parent_tile.col) and (tile.parent_tile.row != @cur_clicked.parent_tile.row)
                    if (tile.h + tile.pos[1] > @cur_clicked.h + @cur_clicked.pos[1])
                        @cur_clicked = tile

        @addListener "MOUSE_DBL_CLICK", () ->
            # if not @selected
            #     @selected = @cur_clicked
            # else if not @selected.equals(@cur_clicked)
            #     return @selected.triggerListener("DESELECTED")
            # @selected.triggerListener("SELECTED") if @selected?
            # @cur_clicked = null

        @addListener "MOUSE_CLICKED", () ->
            if not @cur_clicked? or @character_draw_path
                return
            if @cur_clicked.to_tint or (@selected? and not @cur_clicked.equals(@selected))
                if @selected?
                    @selected.triggerListener("DESELECTED")
                @selected = @cur_clicked
                @selected.triggerListener("SELECTED") if @selected?
                @cur_clicked = null
            else
                @selected = @cur_clicked
                @selected.triggerListener("SELECTED")

        @addListener "DRAG_START", (pos) ->
            @camera_prev_pos = @camera.slice()
            @start_drag_point = pos.slice()
            @drag_started = true
        
        @addListener "DRAG_END", (pos) ->
            @drag_started = false

        #trebalo bi da odgovara na bounds changed zapravo, al aj sad
        @addListener "ENTER_FULLSCREEN", () ->
            @center_point   = [@bounds[2]/2, @bounds[3]/2]
            @max_cols = Math.min(@numcols-1, Math.floor(@bounds[2] / (@tile_dim[0] / 2) + 3))
            @max_rows = Math.min(@numrows-1, Math.ceil(@bounds[3] / @tile_dim[1]) + 1)
            for r in @layer_renderers
                r.resize(Hal.dom.screen_w / @scale[0], Hal.dom.screen_h / @scale[1])
                console.log(@scale)

        @addListener "EXIT_FULLSCREEN", () ->
            @center_point   = [@bounds[2]/2, @bounds[3]/2]
            @max_cols = Math.min(@numcols-1, Math.floor(@bounds[2] / (@tile_dim[0] / 2) + 3))
            @max_rows = Math.min(@numrows-1, Math.ceil(@bounds[3] / @tile_dim[1]) + 1)
            for r in @layer_renderers
                r.resize(@bounds[2] / @scale[0], @bounds[3] / @scale[1])
                   
        @addListener "CHARACTER_SELECTED", (char) ->
            @character_selected = char

        @addListener "CHARACTER_DESELECTED", (char) ->
            @character_selected = null

        @addListener "RIGHT_CLICK", (pos) ->
            if @delete_mode and @focused
                if @focused.layers[@current_layer]
                    if @focused.layers[@current_layer].is_partial
                        layer = @focused.layers[@current_layer].parent_tile
                    else
                        layer = @focused.layers[@current_layer]
                    if @buffers.del[layer.id]?
                        delete @buffers.del[layer.id]
                        layer.tint(false)
            else if not @character_selected?
                @right_tile = @getTileAt(pos)
                if not @right_tile?
                    return
                @lerpCameraTo(@right_tile.pos)
            else if @character_draw_path and @character_selected? and @focused
                console.log("trying to delete...")
                if @buffers.path[@focused.id]?
                    console.log("deleting path...")
                    delete @buffers.path[@focused.id]
                    if @focused.layers[0]
                        @focused.layers[0].tint(false)

        @addListener "KEY_DOWN", (x) ->
            if (x.keyCode == Hal.Keys.SHIFT)
                @shift_pressed = true
                if @current_tile_meta.level?
                    if @current_tile_sprite.h == @tile_dim[1]
                        @current_tile_h = 0
                    else
                        @current_tile_h = (@current_tile_sprite.h - @tile_dim[1]) + @tile_dim[1]/2
                    @showTile()
            else if (x.keyCode == Hal.Keys.D)
                @delete_mode = true
            else if (x.keyCode == Hal.Keys.W)
                @moveable = true
            else if x.keyCode == Hal.Keys.C
                @character_draw_path = true

        @addListener "KEY_UP", (x) ->
            if (x.keyCode == Hal.Keys.SHIFT)
                @shift_pressed = false
            else if (x.keyCode == Hal.Keys.D)
                @delete_mode = false
            else if (x.keyCode == Hal.Keys.DELETE)
                for k,v of @buffers.del
                    v.parent_tile.removeLayer(v.layer)
                @buffers.del = {}
            else if (x.keyCode == Hal.Keys.ONE)
                @current_layer = 0
            else if (x.keyCode == Hal.Keys.TWO)
                @current_layer = 1
            else if (x.keyCode == Hal.Keys.THREE)
                @current_layer = 2
            else if (x.keyCode == Hal.Keys.FOUR)
                @current_layer = 3
            else if (x.keyCode == Hal.Keys.W)
                @moveable = false
            else if (x.keyCode == Hal.Keys.C)
                @character_draw_path = false

        @addListener "LEFT_CLICK", (pos) ->
            if @shift_pressed and @can_place and not @character_selected
                @putAt(pos, @current_tile_meta) 
            else if @delete_mode and @focused
                if @focused.layers[@current_layer]
                    if @focused.layers[@current_layer].is_partial
                        layer = @focused.layers[@current_layer].parent_tile
                    else
                        layer = @focused.layers[@current_layer]
                    @buffers.del[layer.id] = layer
                    layer.tint(true, "red")
            else if @character_selected? and @character_draw_path and @focused
                @buffers.path[@focused.id] = @focused.pos
                if @focused.layers[0]
                    @focused.layers[0].tint(true, "red")
            else if @character_selected? and @focused
                path = @pathfinder.find(@character_selected.parent_tile, @focused)
                @lerpCharacterOnPath(@character_selected, path)

        Hal.addListener "REQEST_MAP_SAVE", (name) =>
            map = @save()
            Hal.triggerListener "MAP_SAVED", {map: map, name: name + "_" + @numrows + "x" + @numcols}

        Hal.addListener "REQUEST_MAP_LOAD", (mapdata) =>
            @load(mapdata)

        Hal.addListener "TILE_SELECTED", (tile) =>
            @current_layer =
                @layers[tile.tile.level]
            @current_tile_sprite =
                Hal.asm.getSprite(tile.tile.sprite)
            if @current_tile_sprite.h == @tile_dim[1]
                @current_tile_h = 0
            else 
                @current_tile_h =
                    (@current_tile_sprite.h - @tile_dim[1]) + @tile_dim[1]/2
            @current_tile_w =
                @current_tile_sprite.w/2 - @tile_dim[0]/2
            @showTile()
            @current_tile_meta = 
                tile.tile

        @addListener "SCROLL_UP", (ev) =>
            if @shift_pressed
                @current_tile_h += 1
            else
                @zoom_in_factor += @scale_step
                if @zoom_in_factor > 0.25
                    @zoom_in_factor = 0.25
                    return
                console.log("zoom in")
                console.log(@zoom_in_factor)
                @current_zoom += (@zoom_in_factor - @zoom_out_factor)
                console.log(@current_zoom)
                Hal.r.ctx.scale(@current_zoom, @current_zoom)
                for r in @layer_renderers
                    r.ctx.scale(@current_zoom, @current_zoom)

        @addListener "SCROLL_DOWN", (ev) =>
            if @shift_pressed
                @current_tile_h -= 1
            else 

                @zoom_out_factor += @scale_step
                console.log("zoom out")
                console.log(@zoom_out_factor)

                if @zoom_out_factor > 0.25
                    @zoom_out_factor = 0.25
                    return

                @current_zoom -= (@zoom_out_factor + @zoom_in_factor)

                Hal.r.ctx.scale(@current_zoom, @current_zoom)
                for r in @layer_renderers
                    r.ctx.scale(@current_zoom, @current_zoom)

                #@scale_factor -= @scale_step

    return AmjadMap