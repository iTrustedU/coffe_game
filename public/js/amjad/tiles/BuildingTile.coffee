"use strict"

define ["../tiles/TileLayer"], 

(TileLayer) ->

    class BuildingTile extends TileLayer
        constructor: (meta, parent, h, w, layer, renderer) ->
            super(meta, parent, h, w, layer, renderer)
    return BuildingTile