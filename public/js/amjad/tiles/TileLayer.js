(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["Entity", "Clickable", "Draggable", "Renderer"], function(Entity, Clickable, Draggable, Renderer) {
    var TileLayer;
    TileLayer = (function(_super) {
      __extends(TileLayer, _super);

      function TileLayer(meta, parent, h, w, layer, renderer) {
        var i, _i, _ref;
        this.layer = layer;
        this.sprite = Hal.asm.getSprite(meta.sprite);
        TileLayer.__super__.constructor.call(this, parent.pos[0], parent.pos[1], [0, 0, this.sprite.w, this.sprite.h]);
        this.name = meta.name;
        this.level = meta.level;
        this.attr = meta.attr;
        this.draw_area = false;
        this.size = meta.size.toString(2).slice(1);
        this.span = ~~(Math.log(this.size.length) / Math.LN2);
        this.parent_scene = parent.parent_scene;
        this.span_area = null;
        this.parent_tile = parent;
        this.span_area = this.parent_scene.getSpanArea(this.parent_tile, meta.size);
        this.g = renderer;
        this.dim = parent.dim;
        this.in_focus = parent.in_focus;
        this.h = h || (this.sprite.h - this.dim[1]);
        this.w = w || (this.sprite.w - this.dim[0]);
        this.bnds = [this.pos[0] - this.w, this.pos[1] - this.h, this.bounds[2], this.bounds[3]];
        this.addCamera(parent.camera);
        this.init();
        this.tint_buffs = {
          green: Hal.imgutils.tintImage(this.sprite.img, "green", 0.3),
          red: Hal.imgutils.tintImage(this.sprite.img, "red", 0.3)
        };
        this.over_red = Hal.asm.getSprite("unordered_grid_unit_over_red_128x64");
        this.tint_buff = this.tint_buffs.green;
        this.to_tint = false;
        if (this.span_area.length > 1) {
          for (i = _i = 1, _ref = this.span_area.length - 1; 1 <= _ref ? _i <= _ref : _i >= _ref; i = 1 <= _ref ? ++_i : --_i) {
            this.span_area[i].layers[this.layer] = {
              is_partial: true,
              is_root: false,
              parent_tile: this
            };
            this.span_area[i].is_partial = true;
          }
        }
        this.is_partial = false;
        this.is_root = true;
      }

      TileLayer.prototype.init = function() {
        var _this = this;
        console.log("hmmmmmm");
        this.addComponent(Clickable);
        this.addListener("LEFT_CLICK", function(pos) {
          if (_this.parent_tile) {
            return _this.parent_scene.triggerListener("TILE_CLICKED", _this);
          }
        });
        this.addListener("SELECTED", function() {
          console.log(_this.getInfo());
          console.log(_this);
          _this.drawArea(true, "red");
          return _this.tint(true);
        });
        this.addListener("MSELECTED", function() {});
        return this.addListener("DESELECTED", function() {
          _this.tint(false);
          return _this.drawArea(false, "red");
        });
      };

      TileLayer.prototype.getInfo = function() {
        return "#" + this.parent_tile.id + "-" + this.parent_tile.row + ":" + this.parent_tile.col + "_" + this.layer;
      };

      TileLayer.prototype.update = function(delta) {
        var n, _i, _len, _ref;
        if (this.draw_area) {
          _ref = this.span_area;
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            n = _ref[_i];
            this.g.drawImage(this.over_red.img, n.pos[0], n.pos[1]);
          }
        }
        if (this.to_tint && !this.is_partial) {
          return this.g.drawImage(this.tint_buff, this.pos[0] - this.w, this.pos[1] - this.h, 1, 1);
        } else if (!this.is_partial) {
          return this.g.drawSprite(this.sprite, this.pos[0] - this.w, this.pos[1] - this.h, 1, 1);
        }
      };

      TileLayer.prototype.tint = function(t, color) {
        if (color == null) {
          color = "green";
        }
        this.tint_buff = this.tint_buffs[color];
        return this.to_tint = t;
      };

      TileLayer.prototype.inBounds = function(point) {
        if (Hal.m.isPointInRect([point[0] + this.w, point[1] + this.h], this.bounds)) {
          if (!Hal.imgutils.isTransparent(this.sprite.img, point[0] + this.w, point[1] + this.h)) {
            return true;
          }
        }
        return false;
      };

      TileLayer.prototype.intersects = function() {};

      TileLayer.prototype.equals = function(b) {
        if (b == null) {
          return false;
        }
        return (this.level === b.level) && this.parent_tile.equals(b.parent_tile);
      };

      TileLayer.prototype.drawArea = function(t) {
        return this.draw_area = t;
      };

      TileLayer.prototype.drawBounds = function() {
        return this.g.strokeRect(this.bnds);
      };

      TileLayer.prototype.serialize = function() {
        var layer;
        layer = {
          meta: {
            name: this.name,
            level: this.level,
            attr: this.attr,
            size: parseInt("1" + this.size, 2),
            sprite: this.sprite.name
          },
          h: this.h,
          w: this.w,
          layer: this.layer
        };
        return layer;
      };

      return TileLayer;

    })(Entity);
    return TileLayer;
  });

}).call(this);
