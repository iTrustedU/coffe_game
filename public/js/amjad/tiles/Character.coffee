"use strict"

define ["../tiles/TileLayer", "Vec2"], 

(TileLayer, Vec2) ->

    class Character extends TileLayer
        constructor: (meta, parent, h, w, layer, renderer) ->
            super(meta, parent, h, w, layer, renderer)
            @mass = 1
            @gravity = Vec2.fromValues(0, 0)

            @velocity = Vec2.fromValues(0.0001, 0.0001)
            @max_vel = 2
            @target = @pos
            Vec2.sub(@velocity, @target, @pos)

            @steering_f = Vec2.fromValues(0,0)

            @max_steering_f = 0.3
            @max_speed = 0.4
            @moving = false

        init: () ->
            super()
            @addListener "SELECTED", () =>
                @parent_scene.triggerListener("CHARACTER_SELECTED", @)
            
            @addListener "DESELECTED", () =>
                @parent_scene.triggerListener("CHARACTER_DESELECTED", @)

        followPath: (path) ->
            if not @follow_path
                @follow_path = Hal.addListener "ENTER_FRAME", (delta) =>
                    @moving = true
                    if (Math.abs(@pos[0] - @target[0]) < 1) and (Math.abs(@pos[1] - @target[1]) < 1)
                        @target = path.shift()
                        if not @target?
                            @moving = false
                            Hal.removeListener "ENTER_FRAME", @follow_path
                            @follow_path = null
                            @target = @pos
                            Vec2.sub(@velocity, @target, @pos)   
                            return
                        else 
                            @parent_tile.layers[@layer] = null
                            @parent_tile = @parent_scene.getTileAt([@target[0] + @camera[0] + @dim[0]/2, @target[1] + @camera[1] + @dim[1]/2])
                            if (not @parent_tile.layers[@layer]? or not @parent_tile.layers[@layer].is_partial) and @parent_tile.layers[0]?
                                @parent_tile.layers[@layer] = @

        update: () ->
            time = Hal.t.delta * 0.001
            desired_vel = []
            Vec2.sub(desired_vel, @target, @pos)
            Vec2.normalize(desired_vel, desired_vel)
            Vec2.scale(desired_vel, desired_vel, @max_vel)
            Vec2.sub(@steering_f, desired_vel, @velocity)
            Vec2.scale(@steering_f, @steering_f, 1/@mass)
            Vec2.scale(@velocity, @velocity, @max_vel)
            Vec2.add(@velocity, @velocity, @steering_f)
            Vec2.add(@pos, @pos, @velocity, time)
            Vec2.normalize(@velocity, @velocity)
            
            if @moving
                console.log("hmm")
                @parent_scene.lerpCameraTo(@pos)

            @g.ctx.strokeStyle = "green"
            @g.ctx.fillRect(@parent_scene.camera[0] + @target[0] - 2, @parent_scene.camera[1] + @target[1] - 2, 4, 4)
            @g.ctx.fill()
            super()
    return Character