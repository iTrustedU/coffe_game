"use strict"

define ["../tiles/TileLayer"], 

(TileLayer) ->

    class DecorTile extends TileLayer
        constructor: (meta, parent, h, w, layer, renderer) ->
            super(meta, parent, h, w, layer, renderer)
    return DecorTile