(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["../tiles/TileLayer"], function(TileLayer) {
    var DecorTile;
    DecorTile = (function(_super) {
      __extends(DecorTile, _super);

      function DecorTile(meta, parent, h, w, layer, renderer) {
        DecorTile.__super__.constructor.call(this, meta, parent, h, w, layer, renderer);
      }

      return DecorTile;

    })(TileLayer);
    return DecorTile;
  });

}).call(this);
