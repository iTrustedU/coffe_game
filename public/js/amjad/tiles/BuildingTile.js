(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["../tiles/TileLayer"], function(TileLayer) {
    var BuildingTile;
    BuildingTile = (function(_super) {
      __extends(BuildingTile, _super);

      function BuildingTile(meta, parent, h, w, layer, renderer) {
        BuildingTile.__super__.constructor.call(this, meta, parent, h, w, layer, renderer);
      }

      return BuildingTile;

    })(TileLayer);
    return BuildingTile;
  });

}).call(this);
