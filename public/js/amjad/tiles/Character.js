(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["../tiles/TileLayer", "Vec2"], function(TileLayer, Vec2) {
    var Character;
    Character = (function(_super) {
      __extends(Character, _super);

      function Character(meta, parent, h, w, layer, renderer) {
        Character.__super__.constructor.call(this, meta, parent, h, w, layer, renderer);
        this.mass = 1;
        this.gravity = Vec2.fromValues(0, 0);
        this.velocity = Vec2.fromValues(0.0001, 0.0001);
        this.max_vel = 2;
        this.target = this.pos;
        Vec2.sub(this.velocity, this.target, this.pos);
        this.steering_f = Vec2.fromValues(0, 0);
        this.max_steering_f = 0.3;
        this.max_speed = 0.4;
        this.moving = false;
      }

      Character.prototype.init = function() {
        var _this = this;
        Character.__super__.init.call(this);
        this.addListener("SELECTED", function() {
          return _this.parent_scene.triggerListener("CHARACTER_SELECTED", _this);
        });
        return this.addListener("DESELECTED", function() {
          return _this.parent_scene.triggerListener("CHARACTER_DESELECTED", _this);
        });
      };

      Character.prototype.followPath = function(path) {
        var _this = this;
        if (!this.follow_path) {
          return this.follow_path = Hal.addListener("ENTER_FRAME", function(delta) {
            _this.moving = true;
            if ((Math.abs(_this.pos[0] - _this.target[0]) < 1) && (Math.abs(_this.pos[1] - _this.target[1]) < 1)) {
              _this.target = path.shift();
              if (_this.target == null) {
                _this.moving = false;
                Hal.removeListener("ENTER_FRAME", _this.follow_path);
                _this.follow_path = null;
                _this.target = _this.pos;
                Vec2.sub(_this.velocity, _this.target, _this.pos);
              } else {
                _this.parent_tile.layers[_this.layer] = null;
                _this.parent_tile = _this.parent_scene.getTileAt([_this.target[0] + _this.camera[0] + _this.dim[0] / 2, _this.target[1] + _this.camera[1] + _this.dim[1] / 2]);
                if (((_this.parent_tile.layers[_this.layer] == null) || !_this.parent_tile.layers[_this.layer].is_partial) && (_this.parent_tile.layers[0] != null)) {
                  return _this.parent_tile.layers[_this.layer] = _this;
                }
              }
            }
          });
        }
      };

      Character.prototype.update = function() {
        var desired_vel, time;
        time = Hal.t.delta * 0.001;
        desired_vel = [];
        Vec2.sub(desired_vel, this.target, this.pos);
        Vec2.normalize(desired_vel, desired_vel);
        Vec2.scale(desired_vel, desired_vel, this.max_vel);
        Vec2.sub(this.steering_f, desired_vel, this.velocity);
        Vec2.scale(this.steering_f, this.steering_f, 1 / this.mass);
        Vec2.scale(this.velocity, this.velocity, this.max_vel);
        Vec2.add(this.velocity, this.velocity, this.steering_f);
        Vec2.add(this.pos, this.pos, this.velocity, time);
        Vec2.normalize(this.velocity, this.velocity);
        if (this.moving) {
          console.log("hmm");
          this.parent_scene.lerpCameraTo(this.pos);
        }
        this.g.ctx.strokeStyle = "green";
        this.g.ctx.fillRect(this.parent_scene.camera[0] + this.target[0] - 2, this.parent_scene.camera[1] + this.target[1] - 2, 4, 4);
        this.g.ctx.fill();
        return Character.__super__.update.call(this);
      };

      return Character;

    })(TileLayer);
    return Character;
  });

}).call(this);
