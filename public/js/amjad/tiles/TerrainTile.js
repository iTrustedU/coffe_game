(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["../tiles/TileLayer"], function(TileLayer) {
    var TerrainTile;
    TerrainTile = (function(_super) {
      __extends(TerrainTile, _super);

      function TerrainTile(meta, parent, h, w, layer, renderer) {
        TerrainTile.__super__.constructor.call(this, meta, parent, h, w, layer, renderer);
      }

      return TerrainTile;

    })(TileLayer);
    return TerrainTile;
  });

}).call(this);
