(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["Entity", "Clickable", "Draggable", "../MapLoader"], function(Entity, Clickable, Draggable, MapLoader) {
    var Tile;
    Tile = (function(_super) {
      __extends(Tile, _super);

      function Tile(x, y, row, col, dim) {
        var isOdd;
        this.dim = dim;
        isOdd = col % 2;
        this.direction = {
          north: [-1, 0],
          south: [1, 0],
          east: [0, 2],
          west: [0, -2],
          southeast: [isOdd, 1],
          northeast: [-1 + isOdd, 1],
          southwest: [isOdd, -1],
          northwest: [-1 + isOdd, -1]
        };
        this.is_partial = false;
        this.sprite = Hal.asm.getSprite("unordered_grid_unit");
        Tile.__super__.constructor.call(this, x, y, [0, 0, this.sprite.w, this.sprite.h]);
        this.row = row;
        this.col = col;
        this.layers = [null, null, null, null];
        this.holds_partial = false;
        this.cell = [[this.pos[0] + this.dim[0] / 2, this.pos[1]], [this.pos[0] + this.dim[0], this.pos[1] + this.dim[1] / 2], [this.pos[0] + this.dim[0] / 2, this.pos[1] + this.dim[1]], [this.pos[0], this.pos[1] + this.dim[1] / 2]];
        this.max_h = 0;
        this.max_w = 0;
        this.bnds = [this.pos[0], this.pos[1], this.bounds[2], this.bounds[3]];
        this.num_layers = 0;
        this.merged_span = {};
        this.is_partial = false;
      }

      Tile.prototype.update = function() {
        var layer, _i, _len, _ref, _results;
        if (this.num_layers === 0) {
          this.g.drawSprite(this.sprite, this.pos[0], this.pos[1]);
        }
        _ref = this.layers;
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          layer = _ref[_i];
          if ((layer != null) && !layer.is_partial) {
            _results.push(layer.update());
          } else {
            _results.push(void 0);
          }
        }
        return _results;
      };

      Tile.prototype.drawBounds = function() {
        this.g.ctx.strokeStyle = "red";
        return this.g.strokeRect(this.bnds);
      };

      Tile.prototype.addLayer = function(layer, meta, height, width, renderer, override) {
        var t;
        if (!override && this.layers[layer]) {
          return;
        }
        this.removeLayer(layer);
        t = MapLoader.loadTileLayer(meta, this, height, width, layer, renderer);
        if (t == null) {
          return;
        }
        t.layer = layer;
        this.layers[layer] = t;
        this.num_layers++;
        this.max_h = Math.max(t.h, this.max_h);
        this.max_w = Math.max(t.w, this.max_w);
        this.bounds[2] = Math.max(this.bounds[2], this.bounds[2] + this.max_w * 2);
        this.bounds[3] = Math.max(this.bounds[3], this.bounds[3] + this.max_h);
        this.bnds = [this.pos[0] - this.max_w, this.pos[1] - this.max_h, this.bounds[2], this.bounds[3]];
        return t;
      };

      Tile.prototype.getBoundedArea = function() {
        return this.merged_span;
      };

      Tile.prototype.removeLayer = function(layer) {
        var i, ispart, t, v, _i, _j, _len, _ref, _ref1;
        if (this.layers[layer]) {
          t = this.layers[layer];
          if (t.span_area != null) {
            for (i = _i = 1, _ref = t.span_area.length - 1; 1 <= _ref ? _i <= _ref : _i >= _ref; i = 1 <= _ref ? ++_i : --_i) {
              if (t.span_area[i] != null) {
                t.span_area[i].is_partial = false;
                t.span_area[i].layers[layer].parent_tile = null;
                t.span_area[i].layers[layer] = null;
              }
            }
          }
          this.layers[layer].removeListeners("LEFT_CLICK");
          this.layers[layer].removeListeners("SELECTED");
          this.layers[layer].removeListeners("DESELECTED");
          this.layers[layer] = null;
          this.num_layers--;
          ispart = false;
          _ref1 = this.layers;
          for (_j = 0, _len = _ref1.length; _j < _len; _j++) {
            v = _ref1[_j];
            if ((v != null) && v.is_partial) {
              ispart = true;
              break;
            }
          }
          return this.is_partial = ispart;
        }
      };

      Tile.prototype.isPartial = function() {
        var layer, _i, _len, _ref;
        _ref = this.layers;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          layer = _ref[_i];
          if (layer.is_partial) {
            return true;
          }
        }
      };

      Tile.prototype.tint = function(t, color) {
        var l, _i, _len, _ref;
        _ref = this.layers;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          l = _ref[_i];
          if (l != null) {
            l.tint(t, color);
          }
        }
      };

      Tile.prototype.drawBorder = function() {};

      Tile.prototype.sortByHeight = function() {
        return this.layers.sort(function(el1, el2) {
          return el1.h < el2.h;
        });
      };

      Tile.prototype.equals = function(other) {
        console.log(other);
        console.log(this);
        return other.row === this.row && other.col === this.col;
      };

      Tile.prototype.save = function() {
        var layer, tile, _i, _len, _ref;
        tile = {
          x: this.pos[0],
          y: this.pos[1],
          row: this.row,
          col: this.col,
          layers: []
        };
        if (this.num_layers === 0) {
          tile.layers = null;
        } else {
          _ref = this.layers;
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            layer = _ref[_i];
            if ((layer != null) && !layer.is_partial) {
              tile.layers.push(layer.serialize());
            }
          }
        }
        return tile;
      };

      Tile.prototype.load = function(layers) {
        var l, layer, _i, _len, _results;
        _results = [];
        for (_i = 0, _len = layers.length; _i < _len; _i++) {
          layer = layers[_i];
          if (layer === null) {
            continue;
          }
          _results.push(l = this.addLayer(layer.layer, layer.meta, layer.h, layer.w, this.parent_scene.layer_renderers[layer.layer], true));
        }
        return _results;
      };

      return Tile;

    })(Entity);
    return Tile;
  });

}).call(this);
