"use strict"

define ["Entity", "Clickable", "Draggable", "Renderer"],

(Entity, Clickable, Draggable, Renderer) ->

    # TileWidth  = 128
    # TileHeight = 64

    class TileLayer extends Entity
        constructor: (meta, parent, h, w, @layer, renderer) ->
            @sprite = Hal.asm.getSprite(meta.sprite)
            super(parent.pos[0], parent.pos[1], [0, 0, @sprite.w, @sprite.h])
            @name         = meta.name
            @level        = meta.level
            @attr         = meta.attr
            @draw_area    = false
            #console.log("meta size: " + meta.size)
            @size         = meta.size.toString(2).slice(1);
            @span         = ~~(Math.log(@size.length)/Math.LN2)
            @parent_scene = parent.parent_scene

            @span_area = null
            @parent_tile = parent

            @span_area    = @parent_scene.getSpanArea(@parent_tile, meta.size)

            @g = renderer
            @dim = parent.dim
            @in_focus = parent.in_focus

            @h = h || (@sprite.h - @dim[1])
            @w = w || (@sprite.w - @dim[0])

            @bnds = [
                @pos[0] - @w, @pos[1] - @h, @bounds[2], @bounds[3]
            ]

            @addCamera(parent.camera)
            @init()

            @tint_buffs = {
                green: Hal.imgutils.tintImage(@sprite.img, "green", 0.3),
                red: Hal.imgutils.tintImage(@sprite.img, "red", 0.3)
            }
            
            @over_red = Hal.asm.getSprite("unordered_grid_unit_over_red_128x64")

            @tint_buff = @tint_buffs.green

            @to_tint = false
            if @span_area.length > 1
                for i in [1..@span_area.length-1]
                    @span_area[i].layers[@layer] = {is_partial: true, is_root: false, parent_tile: @}
                    @span_area[i].is_partial = true
            @is_partial = false
            @is_root = true

        init: () ->
            console.log("hmmmmmm")
            @addComponent(Clickable)
            # @addComponent(Draggable)

            @addListener "LEFT_CLICK", (pos) =>
                if @parent_tile
                    @parent_scene.triggerListener("TILE_CLICKED", @)

            @addListener "SELECTED", () =>
                console.log(@getInfo())
                console.log(@)
                @drawArea(true, "red")
                @tint(true)

            @addListener "MSELECTED", () =>
                return

            @addListener "DESELECTED", () =>
                @tint(false);
                @drawArea(false,  "red")

            # @addListener "MOUSE_OVER", (pos) =>
            #     if @moveable
            #         console.log(pos)

        getInfo: () ->
            return "#"+@parent_tile.id+"-"+@parent_tile.row+":"+@parent_tile.col+"_"+@layer

        update: (delta) ->

            if @draw_area
                for n in @span_area
                    @g.drawImage(@over_red.img, n.pos[0], n.pos[1])
            if @to_tint and not @is_partial
                @g.drawImage(@tint_buff, @pos[0] - @w, @pos[1] - @h, 1, 1)
                #@drawBounds()
            else if not @is_partial
                @g.drawSprite(@sprite, @pos[0] - @w, @pos[1] - @h, 1, 1)

        tint: (t, color="green") ->
            @tint_buff = @tint_buffs[color]
            @to_tint = t

        inBounds: (point) ->
            if Hal.m.isPointInRect([point[0] + @w, point[1] + @h], @bounds)
                if not Hal.imgutils.isTransparent(@sprite.img, point[0] + @w, point[1] + @h)
                    return true
            return false

        intersects: () ->

        equals: (b) ->
            return false if not b?
            return (@level == b.level) and @parent_tile.equals(b.parent_tile)

        drawArea: (t) ->
            @draw_area = t

        drawBounds: () ->
            @g.strokeRect(@bnds)

        serialize: () ->
            layer = {
                meta: {
                        name: @name
                        level: @level
                        attr: @attr
                        size: parseInt("1" + @size, 2)
                        sprite: @sprite.name
                },
                h: @h, 
                w: @w, 
                layer: @layer
            }
            return layer

    return TileLayer 