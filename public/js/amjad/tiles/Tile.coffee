"use strict"

define ["Entity", "Clickable", "Draggable", "../MapLoader"], 

(Entity, Clickable, Draggable, MapLoader) ->


    #terrain_layer = Hal.dom.createCanvas()
    #Hal.dom.addCanvas(terrain_layer,0,0,1, true)


    class Tile extends Entity
        constructor: (x, y, row, col, @dim) ->           
            isOdd = col % 2
            @direction = 
                north: [-1, 0],
                south: [1, 0],
                east: [0, 2],
                west: [0, -2], 
                southeast: [isOdd, 1],
                northeast: [-1+isOdd,1],
                southwest: [isOdd, -1],
                northwest: [-1+isOdd, -1]
            @is_partial = false
            @sprite = Hal.asm.getSprite("unordered_grid_unit")
            super(x, y, [0, 0, @sprite.w, @sprite.h])
            @row = row
            @col = col
            @layers = [null, null, null, null]
            @holds_partial = false
            @cell = [
                [(@pos[0]) + @dim[0]/2, (@pos[1])],
                [(@pos[0]) + @dim[0], (@pos[1]) + @dim[1] / 2],
                [(@pos[0]) + @dim[0]/2, (@pos[1]) + @dim[1]],
                [(@pos[0]), (@pos[1]) + @dim[1] / 2]
            ]
            @max_h = 0
            @max_w = 0
            @bnds = [@pos[0], @pos[1], @bounds[2], @bounds[3]]
            @num_layers = 0

            @merged_span = {}
            @is_partial = false

        update: () ->
            @g.drawSprite(@sprite, @pos[0], @pos[1]) if @num_layers == 0

            for layer in @layers
                if layer? and not layer.is_partial
                    layer.update()
                    #layer.drawBounds()

        drawBounds: () ->
            @g.ctx.strokeStyle = "red"
            @g.strokeRect(@bnds)

        #removeLayer: 
        addLayer: (layer, meta, height, width, renderer, override) ->
            # console.log(layer)
            # console.log("layer: " + layer)
            if not override and @layers[layer]
                return

            @removeLayer(layer)
            t = MapLoader.loadTileLayer(meta, @, height, width, layer, renderer)
            if not t?
                return
            t.layer = layer

            @layers[layer] = t
            @num_layers++

            @max_h      = Math.max(t.h, @max_h)
            @max_w      = Math.max(t.w, @max_w)
            @bounds[2]  = Math.max(@bounds[2], @bounds[2] + @max_w*2)
            @bounds[3]  = Math.max(@bounds[3], @bounds[3] + @max_h)
            @bnds = [@pos[0] - @max_w, @pos[1] - @max_h, @bounds[2], @bounds[3]]

            #da li ovi bnds rade sada, eh? nvm :)''
            # if t.size == 3
            #     @merged_span = 
            # for v in t.span_areau
            #     console.log(v)
            #     @merged_span[v.id] = v

            return t

        getBoundedArea: () ->
            return @merged_span
        
        removeLayer: (layer) ->
            if @layers[layer]
                t = @layers[layer]
                if t.span_area?
                    for i in [1..t.span_area.length-1]
                        if t.span_area[i]?
                            t.span_area[i].is_partial = false
                            t.span_area[i].layers[layer].parent_tile = null 
                            t.span_area[i].layers[layer] = null
                        #t.span_area[0].is_partial = false  
                    #if t.span_area[0]
                    #    t.span_area[0].layers[layer] = null
                @layers[layer].removeListeners "LEFT_CLICK"
                @layers[layer].removeListeners "SELECTED"
                @layers[layer].removeListeners "DESELECTED"
                @layers[layer] = null
                @num_layers--
                ispart = false
                for v in @layers
                    if v? and v.is_partial
                        ispart = true
                        break
                @is_partial = ispart

        isPartial: () ->
            for layer in @layers
                if layer.is_partial
                    return true

        tint: (t, color) ->
            for l in @layers
                if l?
                    l.tint(t, color)
            return

        drawBorder: () ->
            return

        sortByHeight: () ->
            @layers.sort (el1, el2) ->
                return el1.h < el2.h

        equals: (other) ->
            console.log(other)
            console.log(@)
            return other.row == @row and other.col == @col

        save: () ->

            tile = {
                x: @pos[0], 
                y: @pos[1],
                row: @row, 
                col: @col,
                layers: []
            }

            if @num_layers == 0
                tile.layers = null
            else 
                for layer in @layers
                    if layer? and not layer.is_partial
                        tile.layers.push(layer.serialize())

            return tile

        load: (layers) ->
            for layer in layers
                if layer == null
                    continue
                l = @addLayer(layer.layer, layer.meta, layer.h, layer.w, @parent_scene.layer_renderers[layer.layer], true)
                
    return Tile
