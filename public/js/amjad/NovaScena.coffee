"use strict"

define ["Scene", "Entity", "Clickable", "Keyboard", "Vec2"], 

(Scene, Entity, Clickable, Keyboard, Vec2) ->

    class SimpleEntity extends Entity
        constructor: (x, y, @row, @col, @color, @sz, @map) ->
            super(x, y, [0, 0, @sz, @sz])
            @is_obstacle = false

        init: () ->
            @addComponent(Clickable)
            @addListener "LEFT_CLICK", (pos) ->
                @is_obstacle = not @is_obstacle

                if @is_obstacle
                    @color = "red"
                else
                    @color = "gray"

                # neighs = @map.getNeighbours(@)
                
                # for n in neighs
                #     n.color = "cyan"

            @addListener "RIGHT_CLICK", () ->
                @map.triggerListener("SELECTED", @)

        update: () ->
            @g.ctx.strokeRect(@pos[0], @pos[1], @sz, @sz)
            @g.ctx.save()
            @g.ctx.fillStyle = @color
            @g.ctx.fillRect(@pos[0] + 5, @pos[1] + 5, 40, 40)
            @g.ctx.restore()

    class PathFinder
        constructor: (@map) ->
            @diagonal_cost = 14
            @straight_cost = 10

            @open = []
            @closed = []

        isInClosed: (t) ->
            return @closed.some (x) -> return x.row == t.row and x.col == t.col

        isInOpen: (t) ->
            l = @open.filter (x) -> return x.row == t.row and x.col == t.col
            return l[0]

        tracePath: (dest) ->
            out = []
            while dest.parent?
                out.push(dest)
                dest = dest.parent

            return out

        find: (from, to) ->
            @open = []
            @closed = []

            if not from? or not to?
                console.log("no start or end node given")
                return

            @open.push({
                row: from.row
                col: from.col
                f: 0
                g: 0
                h: 0
                parent: null
            })

            while @open.length > 0
                cur = @open.shift()

                if cur.row == to.row and cur.col == to.col
                    console.log("nasao sam ga")
                    return @tracePath(cur)
                @closed.push(cur)

                neighs = @map.getNeighbours(@map.getTile(cur.row, cur.col))

                for t in neighs
                    if (t.row == cur.row or t.col == cur.col)
                        g = @straight_cost
                    else 
                        g = @diagonal_cost

                    if @isInClosed(t) or t.is_obstacle
                        continue

                    tt = @isInOpen(t)

                    if not tt
                        # xDist = Math.abs(to.row - t.row)
                        # yDist = Math.abs(to.col - t.col)
                        # if xDist > yDist
                        #     h = @diagonal_cost*yDist + @straight_cost*(xDist - yDist)
                        # else 
                        #     h = @diagonal_cost*xDist + @straight_cost*(yDist - xDist)
                        h = (Math.abs(t.row - to.row) + Math.abs(t.col - to.col)) * @straight_cost
                        # xDiff = Math.abs(to.pos[0] - t.pos[0])
                        # yDiff = Math.abs(to.pos[1] - t.pos[1])
                        # h = Math.sqrt(xDiff*xDiff + yDiff*yDiff)

                        @open.push({
                            h: h
                            row: t.row
                            col: t.col
                            g: cur.g + g
                            f: h + g
                            parent: cur
                        })

                    else if g + cur.g < tt.g
                            console.log("ikad?")
                            tt.g = g + cur.g
                            tt.f = tt.h + tt.g
                            tt.parent = cur

                @open.sort (a,b) ->
                    return a.f > b.f


    class NovaScena extends Scene
        constructor: (name, bounds, newcanvas) ->
            super(name, bounds, newcanvas)
            @map = []
            @camera = [0, 0]
            @numrows = 9
            @numcols = 16
            @sz = 50
            @start_and_end = 0
            for i in [0..@numrows-1]
                for j in [0..@numcols-1]
                    @map.push(@addEntity(new SimpleEntity(j * @sz, i * @sz, i, j, "gray", @sz, @)))
            @pathfinder = new PathFinder(@)
            @start_tile = undefined
            @end_tile = undefined
            @path = undefined

            @Character = {
                pos: Vec2.fromValues(0,0)
                color: "cyan"
            }

        followPath: (path) ->
            #@Character.pos = @start_tile.pos
            #while (x = path.pop())?
            x = path.pop()
            tile = @getTile(x.row, x.col)
            total = 0
            step = 5
            totalSecs = 0
            pos = [tile.pos[0], tile.pos[1]]
            if not @char_anim
                @char_anim = Hal.addListener "ENTER_FRAME", (delta) =>           
                        total += delta
                        totalSecs += delta
                        console.log(delta)              
                        Vec2.lerp(@start_tile.pos, @start_tile.pos, pos, delta * (totalSecs * total))
                        if (total > delta)
                            x = path.pop()
                            total = 0
                            if not x?
                                Hal.removeListener "ENTER_FRAME", @char_anim
                                total = 0
                                @char_anim = null
                                return
                            tile = @getTile(x.row, x.col)
                            pos = [tile.pos[0], tile.pos[1]]
        update: () ->
            @g.clear()
            for t in @map
                t.update()
            #@g.ctx.fillStyle = @Character.color
            #@g.ctx.fillRect(@Character.pos[0] + 5, @Character.pos[1] + 5, 40, 40)

        getTile: (row, col) -> 
            if row < 0 or row >= @numrows or col < 0 or col >= @numcols
                return
            return @map[col + row * @numcols]

        getNeighbours: (t) ->
            row = t.row
            col = t.col
            out = []
            for i in [-1..1]
                for j in [-1..1]
                    if i == j == 0
                        continue
                    tile = @getTile(row+i, col+j)
                    if tile?
                        out.push(tile)
            return out

        findPath: () ->
            t = window.performance.now()
            @path = @pathfinder.find(@start_tile, @end_tile)
            t1 = window.performance.now() - t
            console.log("time it took: " + t1)
            if not @path? 
                console.log("no path")
                return
            for p in @path
                @getTile(p.row, p.col).color = "green"
            @followPath(@path)

        reset: () ->
            for t in @map
                t.color = "gray"
                t.is_obstacle = false
                @start_and_end = 0
                @start_tile = null
                @end_tile = null

        init: () ->
            @addComponent(Keyboard)

            @addListener "KEY_UP", (x) ->
                if x.keyCode == Hal.Keys.G
                    @findPath()
                else if x.keyCode == Hal.Keys.W
                    @reset()
                else if x.keyCode == Hal.Keys.D
                    console.log("randomize....")
                    @randomize()

            @addListener "SELECTED", (tile) ->
                @start_and_end++
                if @start_and_end == 1
                    @start_tile = tile
                    @start_tile.color = "yellow"
                else if @start_and_end == 2
                    @end_tile = tile
                    @start_and_end = 0
                    @end_tile.color = "blue"

            @g.clear()

        randomize: () ->
            for t in @map
                if Math.random() < 0.4
                    t.triggerListener "LEFT_CLICK"

    return NovaScena