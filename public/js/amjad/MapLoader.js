(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["EventDispatcher", "../amjad/tiles/TerrainTile", "../amjad/tiles/BuildingTile", "../amjad/tiles/DecorTile", "../amjad/tiles/Character"], function(EventDispatcher, TerrainTile, BuildingTile, DecorTile, Character) {
    var MapLoader;
    MapLoader = (function(_super) {
      __extends(MapLoader, _super);

      function MapLoader() {
        MapLoader.__super__.constructor.call(this);
        return;
      }

      return MapLoader;

    })(EventDispatcher);
    MapLoader.prototype.loadTileLayer = function(meta, parent, h, w, layer, renderer) {
      if (meta.level === "terrain") {
        return new TerrainTile(meta, parent, h, w, 0, renderer);
      } else if (meta.level === "buildings") {
        return new BuildingTile(meta, parent, h, w, 2, renderer);
      } else if (meta.level === "decor") {
        return new DecorTile(meta, parent, h, w, 1, renderer);
      } else if (meta.level === "poi") {
        return new POITile(meta, parent, h, w, 3, renderer);
      } else if (meta.level === "character") {
        return new Character(meta, parent, h, w, 1, renderer);
      }
    };
    return new MapLoader();
  });

}).call(this);
