(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["Scene", "Entity", "Clickable", "Keyboard", "Vec2"], function(Scene, Entity, Clickable, Keyboard, Vec2) {
    var NovaScena, PathFinder, SimpleEntity;
    SimpleEntity = (function(_super) {
      __extends(SimpleEntity, _super);

      function SimpleEntity(x, y, row, col, color, sz, map) {
        this.row = row;
        this.col = col;
        this.color = color;
        this.sz = sz;
        this.map = map;
        SimpleEntity.__super__.constructor.call(this, x, y, [0, 0, this.sz, this.sz]);
        this.is_obstacle = false;
      }

      SimpleEntity.prototype.init = function() {
        this.addComponent(Clickable);
        this.addListener("LEFT_CLICK", function(pos) {
          this.is_obstacle = !this.is_obstacle;
          if (this.is_obstacle) {
            return this.color = "red";
          } else {
            return this.color = "gray";
          }
        });
        return this.addListener("RIGHT_CLICK", function() {
          return this.map.triggerListener("SELECTED", this);
        });
      };

      SimpleEntity.prototype.update = function() {
        this.g.ctx.strokeRect(this.pos[0], this.pos[1], this.sz, this.sz);
        this.g.ctx.save();
        this.g.ctx.fillStyle = this.color;
        this.g.ctx.fillRect(this.pos[0] + 5, this.pos[1] + 5, 40, 40);
        return this.g.ctx.restore();
      };

      return SimpleEntity;

    })(Entity);
    PathFinder = (function() {
      function PathFinder(map) {
        this.map = map;
        this.diagonal_cost = 14;
        this.straight_cost = 10;
        this.open = [];
        this.closed = [];
      }

      PathFinder.prototype.isInClosed = function(t) {
        return this.closed.some(function(x) {
          return x.row === t.row && x.col === t.col;
        });
      };

      PathFinder.prototype.isInOpen = function(t) {
        var l;
        l = this.open.filter(function(x) {
          return x.row === t.row && x.col === t.col;
        });
        return l[0];
      };

      PathFinder.prototype.tracePath = function(dest) {
        var out;
        out = [];
        while (dest.parent != null) {
          out.push(dest);
          dest = dest.parent;
        }
        return out;
      };

      PathFinder.prototype.find = function(from, to) {
        var cur, g, h, neighs, t, tt, _i, _len;
        this.open = [];
        this.closed = [];
        if ((from == null) || (to == null)) {
          console.log("no start or end node given");
          return;
        }
        this.open.push({
          row: from.row,
          col: from.col,
          f: 0,
          g: 0,
          h: 0,
          parent: null
        });
        while (this.open.length > 0) {
          cur = this.open.shift();
          if (cur.row === to.row && cur.col === to.col) {
            console.log("nasao sam ga");
            return this.tracePath(cur);
          }
          this.closed.push(cur);
          neighs = this.map.getNeighbours(this.map.getTile(cur.row, cur.col));
          for (_i = 0, _len = neighs.length; _i < _len; _i++) {
            t = neighs[_i];
            if (t.row === cur.row || t.col === cur.col) {
              g = this.straight_cost;
            } else {
              g = this.diagonal_cost;
            }
            if (this.isInClosed(t) || t.is_obstacle) {
              continue;
            }
            tt = this.isInOpen(t);
            if (!tt) {
              h = (Math.abs(t.row - to.row) + Math.abs(t.col - to.col)) * this.straight_cost;
              this.open.push({
                h: h,
                row: t.row,
                col: t.col,
                g: cur.g + g,
                f: h + g,
                parent: cur
              });
            } else if (g + cur.g < tt.g) {
              console.log("ikad?");
              tt.g = g + cur.g;
              tt.f = tt.h + tt.g;
              tt.parent = cur;
            }
          }
          this.open.sort(function(a, b) {
            return a.f > b.f;
          });
        }
      };

      return PathFinder;

    })();
    NovaScena = (function(_super) {
      __extends(NovaScena, _super);

      function NovaScena(name, bounds, newcanvas) {
        var i, j, _i, _j, _ref, _ref1;
        NovaScena.__super__.constructor.call(this, name, bounds, newcanvas);
        this.map = [];
        this.camera = [0, 0];
        this.numrows = 9;
        this.numcols = 16;
        this.sz = 50;
        this.start_and_end = 0;
        for (i = _i = 0, _ref = this.numrows - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
          for (j = _j = 0, _ref1 = this.numcols - 1; 0 <= _ref1 ? _j <= _ref1 : _j >= _ref1; j = 0 <= _ref1 ? ++_j : --_j) {
            this.map.push(this.addEntity(new SimpleEntity(j * this.sz, i * this.sz, i, j, "gray", this.sz, this)));
          }
        }
        this.pathfinder = new PathFinder(this);
        this.start_tile = void 0;
        this.end_tile = void 0;
        this.path = void 0;
        this.Character = {
          pos: Vec2.fromValues(0, 0),
          color: "cyan"
        };
      }

      NovaScena.prototype.followPath = function(path) {
        var pos, step, tile, total, totalSecs, x,
          _this = this;
        x = path.pop();
        tile = this.getTile(x.row, x.col);
        total = 0;
        step = 5;
        totalSecs = 0;
        pos = [tile.pos[0], tile.pos[1]];
        if (!this.char_anim) {
          return this.char_anim = Hal.addListener("ENTER_FRAME", function(delta) {
            total += delta;
            totalSecs += delta;
            console.log(delta);
            Vec2.lerp(_this.start_tile.pos, _this.start_tile.pos, pos, delta * (totalSecs * total));
            if (total > delta) {
              x = path.pop();
              total = 0;
              if (x == null) {
                Hal.removeListener("ENTER_FRAME", _this.char_anim);
                total = 0;
                _this.char_anim = null;
                return;
              }
              tile = _this.getTile(x.row, x.col);
              return pos = [tile.pos[0], tile.pos[1]];
            }
          });
        }
      };

      NovaScena.prototype.update = function() {
        var t, _i, _len, _ref, _results;
        this.g.clear();
        _ref = this.map;
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          t = _ref[_i];
          _results.push(t.update());
        }
        return _results;
      };

      NovaScena.prototype.getTile = function(row, col) {
        if (row < 0 || row >= this.numrows || col < 0 || col >= this.numcols) {
          return;
        }
        return this.map[col + row * this.numcols];
      };

      NovaScena.prototype.getNeighbours = function(t) {
        var col, i, j, out, row, tile, _i, _j;
        row = t.row;
        col = t.col;
        out = [];
        for (i = _i = -1; _i <= 1; i = ++_i) {
          for (j = _j = -1; _j <= 1; j = ++_j) {
            if ((i === j && j === 0)) {
              continue;
            }
            tile = this.getTile(row + i, col + j);
            if (tile != null) {
              out.push(tile);
            }
          }
        }
        return out;
      };

      NovaScena.prototype.findPath = function() {
        var p, t, t1, _i, _len, _ref;
        t = window.performance.now();
        this.path = this.pathfinder.find(this.start_tile, this.end_tile);
        t1 = window.performance.now() - t;
        console.log("time it took: " + t1);
        if (this.path == null) {
          console.log("no path");
          return;
        }
        _ref = this.path;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          p = _ref[_i];
          this.getTile(p.row, p.col).color = "green";
        }
        return this.followPath(this.path);
      };

      NovaScena.prototype.reset = function() {
        var t, _i, _len, _ref, _results;
        _ref = this.map;
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          t = _ref[_i];
          t.color = "gray";
          t.is_obstacle = false;
          this.start_and_end = 0;
          this.start_tile = null;
          _results.push(this.end_tile = null);
        }
        return _results;
      };

      NovaScena.prototype.init = function() {
        this.addComponent(Keyboard);
        this.addListener("KEY_UP", function(x) {
          if (x.keyCode === Hal.Keys.G) {
            return this.findPath();
          } else if (x.keyCode === Hal.Keys.W) {
            return this.reset();
          } else if (x.keyCode === Hal.Keys.D) {
            console.log("randomize....");
            return this.randomize();
          }
        });
        this.addListener("SELECTED", function(tile) {
          this.start_and_end++;
          if (this.start_and_end === 1) {
            this.start_tile = tile;
            return this.start_tile.color = "yellow";
          } else if (this.start_and_end === 2) {
            this.end_tile = tile;
            this.start_and_end = 0;
            return this.end_tile.color = "blue";
          }
        });
        return this.g.clear();
      };

      NovaScena.prototype.randomize = function() {
        var t, _i, _len, _ref, _results;
        _ref = this.map;
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          t = _ref[_i];
          if (Math.random() < 0.4) {
            _results.push(t.triggerListener("LEFT_CLICK"));
          } else {
            _results.push(void 0);
          }
        }
        return _results;
      };

      return NovaScena;

    })(Scene);
    return NovaScena;
  });

}).call(this);
