(function() {
  "use strict";
  /*
      valjalo bi pomeranje kamere izdvojiti u scenu
      da izbegnem dupliranje koda na nekim mestima
  */

  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["Scene", "Clickable", "Draggable", "Vec2", "Entity", "../amjad/tiles/Tile", "Keyboard", "Scrollable", "../amjad/MapLoader", "PathFinder", "Renderer"], function(Scene, Clickable, Draggable, Vec2, Entity, Tile, Keyboard, Scrollable, MapLoader, PathFinder, Renderer) {
    var AmjadMap;
    AmjadMap = (function(_super) {
      __extends(AmjadMap, _super);

      function AmjadMap(name, bounds, newcanvas) {
        var buildings_layer, decor_layer, i, j, poi_layer, terrain_layer, x, y, _i, _j, _k, _len, _ref, _ref1, _ref2;
        AmjadMap.__super__.constructor.call(this, "amjad", bounds, newcanvas);
        this.tiles = [];
        this.margin_right = 5;
        this.margin_bottom = 5;
        this.numrows = 20;
        this.numcols = 20;
        this.tile_dim = [128, 64];
        this.prev_focused = null;
        this.zoom_in_factor = 0;
        this.zoom_out_factor = 0;
        this.scale_step = 0.05;
        this.current_zoom = 1;
        this.tilew2prop = 2 / this.tile_dim[0];
        this.tileh2prop = 2 / this.tile_dim[1];
        this.tilew2 = this.tile_dim[0] / 2;
        this.tileh2 = this.tile_dim[1] / 2;
        this.offsetX = 0;
        this.offsetY = 0;
        this.grid = [];
        this.shift_pressed = false;
        this.can_place = false;
        this.current_layer = null;
        this.selected_sprite = null;
        this.current_tile = {};
        this.current_tile_meta = {
          name: "not selected"
        };
        this.sand = Hal.asm.getSprite("unordered_sand03");
        this.mask = Hal.asm.getSprite("unordered_tilemask_128x64");
        this.over = {
          "green": Hal.asm.getSprite("unordered_grid_unit_over_green_128x64"),
          "red": Hal.asm.getSprite("unordered_grid_unit_over_red_128x64")
        };
        this.current_tile_sprite = this.over["green"];
        this.current_tile_h = 0;
        this.current_tile_w = 0;
        this.hittest = Hal.dom.createCanvas(this.tile_dim[0], this.tile_dim[1]).getContext("2d");
        this.hittest.drawImage(this.mask.img, 0, 0);
        this.mask_data = this.hittest.getImageData(0, 0, this.tile_dim[0], this.tile_dim[1]).data;
        this.show_grid = false;
        this.center_point = [this.bounds[2] / 2, this.bounds[3] / 2];
        this.from_row = Math.max(0, Math.round(this.camera[0] / this.tile_dim[1]) - 1);
        this.from_col = Math.max(0, Math.round(this.camera[1] / this.tile_dim[0] * 2) - 1);
        this.max_cols = Math.min(this.numcols - 1, Math.floor(this.bounds[2] / (this.tile_dim[0] / 2) + 2));
        this.max_rows = Math.min(this.numrows - 1, Math.ceil(this.bounds[3] / this.tile_dim[1]) + 1);
        this.cur_clicked = null;
        this.over_coord = [0, 0];
        this.right_tile = null;
        this.lerp = false;
        this.lerp_to = [];
        this.cur_area = [];
        this.wait_frames = 10;
        _ref = this.mask_data;
        for (j = _i = 0, _len = _ref.length; _i < _len; j = ++_i) {
          i = _ref[j];
          this.mask_data[j] = i < 120;
        }
        this.camera_prev_pos = this.camera.slice();
        this.start_drag_point = [0, 0];
        for (i = _j = 0, _ref1 = this.numrows - 1; 0 <= _ref1 ? _j <= _ref1 : _j >= _ref1; i = 0 <= _ref1 ? ++_j : --_j) {
          for (j = _k = 0, _ref2 = this.numcols - 1; 0 <= _ref2 ? _k <= _ref2 : _k >= _ref2; j = 0 <= _ref2 ? ++_k : --_k) {
            x = (j / 2) * this.tile_dim[0] + this.camera[0];
            y = (i + ((j % 2) / 2)) * this.tile_dim[1] + this.camera[1];
            this.grid.push(this.addEntity(new Tile(x, y, i, j, this.tile_dim)));
          }
        }
        this.prev_selected = null;
        this.selected_arr = [];
        this.prev_area = [];
        this.focused = null;
        this.layers = {
          terrain: 0,
          decor: 1,
          buildings: 2,
          poi: 3,
          character: 1
        };
        this.total = 0;
        this.camera_speed = 0.2;
        this.moveable = false;
        this.info = {
          row: "row: ",
          col: "col: ",
          tilename: "tile: ",
          layer: "layer: ",
          fps: "fps: ",
          start_row: "starting row: ",
          start_col: "staring col: ",
          tile_x: "tile_x: ",
          tile_y: "tile_y: ",
          camera_x: "camera_x: ",
          camera_y: "camera_y: ",
          num_rendering: "no. rendereded entities: ",
          lerp_x: "lerp_x: ",
          lerp_y: "lerp_y: "
        };
        console.log("max columns: " + this.max_cols);
        console.log("max rows: " + this.max_rows);
        console.log("grid length: " + this.grid_length);
        console.log("max that can be seen: " + ((this.max_cols + 1) * (this.max_rows + 1)));
        this.character_selected = null;
        this.buffers = {
          del: {},
          path: {}
        };
        this.delete_mode = false;
        this.pathfinder = new PathFinder(this);
        terrain_layer = Hal.dom.createCanvas();
        Hal.dom.addCanvas(terrain_layer, 0, 0, true, 1);
        decor_layer = Hal.dom.createCanvas();
        Hal.dom.addCanvas(decor_layer, 0, 0, true, 2);
        buildings_layer = Hal.dom.createCanvas();
        Hal.dom.addCanvas(buildings_layer, 0, 0, true, 3);
        poi_layer = Hal.dom.createCanvas();
        Hal.dom.addCanvas(poi_layer, 0, 0, true, 4);
        this.layer_renderers = [new Renderer([0, 0, Hal.dom.area.width, Hal.dom.area.height], terrain_layer, this.camera), new Renderer([0, 0, Hal.dom.area.width, Hal.dom.area.height], decor_layer, this.camera), new Renderer([0, 0, Hal.dom.area.width, Hal.dom.area.height], buildings_layer, this.camera), new Renderer([0, 0, Hal.dom.area.width, Hal.dom.area.height], poi_layer, this.camera)];
        this.terrain_layer_ctx = terrain_layer.getContext("2d");
        this.decor_layer_ctx = decor_layer.getContext("2d");
        this.buildings_layer_ctx = buildings_layer.getContext("2d");
        this.poi_layer_ctx = poi_layer.getContext("2d");
      }

      return AmjadMap;

    })(Scene);
    AmjadMap.prototype.toOrtho = function(pos) {
      var coldiv, off_x, off_y, rowdiv, transp;
      coldiv = pos[0] * this.tilew2prop;
      rowdiv = pos[1] * this.tileh2prop;
      off_x = ~~(pos[0] - ~~(coldiv * 0.5) * this.tile_dim[0]);
      off_y = ~~(pos[1] - ~~(rowdiv * 0.5) * this.tile_dim[1]);
      transp = this.mask_data[(off_x + this.tile_dim[0] * off_y) * 4 + 3];
      return [coldiv - (transp ^ !(coldiv & 1)), (rowdiv - (transp ^ !(rowdiv & 1))) / 2];
    };
    AmjadMap.prototype.putAt = function(pos, meta, overwrite) {
      var t;
      t = this.getTileAt(pos);
      if (t == null) {
        return;
      }
      return t.addLayer(this.current_layer, meta, this.current_tile_h, this.current_tile_w, this.layer_renderers[this.current_layer]);
    };
    AmjadMap.prototype.save = function() {
      var g, map, _i, _len, _ref;
      map = [];
      _ref = this.grid;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        g = _ref[_i];
        if (g != null) {
          map.push(g.save());
        }
      }
      return map;
    };
    AmjadMap.prototype.load = function(data) {
      var g, l, size, _i, _j, _len, _len1, _ref, _ref1,
        _this = this;
      this.pause();
      _ref = this.grid;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        g = _ref[_i];
        _ref1 = g.layers;
        for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
          l = _ref1[_j];
          if (l != null) {
            g.removeLayer(l.layer);
          }
        }
        this.removeEntity(g);
      }
      this.grid = [];
      size = data.name.match(/(\d*x\d*)/g)[0].split("x");
      this.numrows = +size[0];
      this.numcols = +size[1];
      console.log(this.numrows);
      console.log(this.numcols);
      this.cur_clicked = null;
      this.selected = null;
      NProgress.configure({
        ease: 'linear',
        speed: 100,
        trickle: false
      });
      NProgress.start();
      NProgress.configure({
        ease: 'linear',
        speed: 100,
        trickle: false
      });
      setTimeout(function() {
        return _this.loadTile(0, data);
      }, 10);
    };
    AmjadMap.prototype.canBePlacedOn = function(span, layer) {
      var k, _i, _len;
      for (_i = 0, _len = span.length; _i < _len; _i++) {
        k = span[_i];
        if ((k.layers[layer] != null)) {
          if (k.layers[layer].is_partial || k.layers[layer].is_root) {
            return false;
          }
        }
      }
      return true;
    };
    AmjadMap.prototype.getTile = function(row, col, dir) {
      if (dir == null) {
        dir = [0, 0];
      }
      return this.grid[(col + dir[1]) + (row + dir[0]) * this.numcols];
    };
    AmjadMap.prototype.getNeighbours = function(tile) {
      var dir, n, out, _i, _len, _ref;
      out = [];
      _ref = Object.keys(tile.direction);
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        dir = _ref[_i];
        n = this.getTile(tile.row, tile.col, tile.direction[dir]);
        if (n != null) {
          out.push(n);
        }
      }
      return out;
    };
    AmjadMap.prototype.getReachableNeighbours = function(tile) {
      var dir, n, out, _i, _len, _ref;
      out = [];
      _ref = Object.keys(tile.direction);
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        dir = _ref[_i];
        n = this.getTile(tile.row, tile.col, tile.direction[dir]);
        if ((n != null) && n.layers[0] && n.num_layers === 1 && !n.is_partial) {
          out.push(n);
        }
      }
      return out;
      return tile.merged_span;
    };
    AmjadMap.prototype.tintNeighbours = function(tile, t) {
      var n, _i, _len, _ref;
      _ref = this.getNeighbours(tile);
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        n = _ref[_i];
        n.tint(t);
      }
    };
    AmjadMap.prototype.tintWithNeighbours = function(tile, t) {
      var n, _i, _len, _ref;
      _ref = this.getNeighbours(tile);
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        n = _ref[_i];
        n.tint(t);
      }
      this.getTile(tile.row, tile.col).tint(t);
    };
    AmjadMap.prototype.getTileAt = function(pos) {
      var coord;
      coord = this.toOrtho([pos[0] - this.camera[0], pos[1] - this.camera[1]]);
      if (coord[0] < 0.0 || coord[1] < 0.0 || coord[1] >= this.numrows || coord[0] >= this.numcols) {
        return null;
      }
      return this.grid[Math.floor(coord[0]) + Math.floor(coord[1]) * this.numcols];
    };
    AmjadMap.prototype.findInDirectionOf = function(tile, dirstr, len) {
      var dir, fromc, fromr, out, t;
      if (tile == null) {
        return [];
      }
      out = [];
      out.push(tile);
      fromr = tile.row;
      fromc = tile.col;
      dir = tile.direction[dirstr];
      while (len > 0) {
        t = this.getTile(fromr, fromc, dir);
        if (t != null) {
          out.push(t);
          fromr = t.row;
          fromc = t.col;
          dir = t.direction[dirstr];
        } else {
          break;
        }
        len--;
      }
      return out;
    };
    /*
        Ovo definitivno moze da se optimizuje tako da 
        izbegnem pretragu nemarkiranih polja
    */

    AmjadMap.prototype.getSpanArea = function(tile, size) {
      var neasts, nwests, out, sizelen, span, w, _i, _len;
      size = size.toString(2).slice(1);
      sizelen = size.length - 1;
      span = ~~(Math.log(size.length) / Math.LN2);
      nwests = this.findInDirectionOf(tile, "northwest", span - 1);
      out = [];
      for (_i = 0, _len = nwests.length; _i < _len; _i++) {
        w = nwests[_i];
        neasts = this.findInDirectionOf(w, "northeast", span - 1);
        out = out.concat(neasts);
      }
      out = out.filter(function($, i) {
        return +size[sizelen - ((i % span) * span) - ~~(i / span)];
      });
      return out;
    };
    AmjadMap.prototype.update = function() {
      var i, j, tile, _i, _j, _ref, _ref1;
      this.decor_layer_ctx.clearRect(0, 0, Hal.dom.area.width, Hal.dom.area.height);
      this.terrain_layer_ctx.clearRect(0, 0, Hal.dom.area.width, Hal.dom.area.height);
      this.buildings_layer_ctx.clearRect(0, 0, Hal.dom.area.width, Hal.dom.area.height);
      this.poi_layer_ctx.clearRect(0, 0, Hal.dom.area.width, Hal.dom.area.height);
      this.total = 0;
      for (i = _i = 0, _ref = this.max_rows; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
        for (j = _j = 0, _ref1 = this.max_cols; 0 <= _ref1 ? _j <= _ref1 : _j >= _ref1; j = 0 <= _ref1 ? ++_j : --_j) {
          tile = this.grid[(j + this.from_col) + (i + this.from_row) * this.numcols];
          if (tile == null) {
            continue;
          }
          tile.update();
          this.total++;
        }
      }
      if (this.focused != null) {
        this.g.drawImage(this.over["green"].img, this.focused.pos[0], this.focused.pos[1]);
      }
      return this.drawInfo();
    };
    AmjadMap.prototype.drawInfo = function() {
      if (this.focused != null) {
        this.g.ctx.fillText(this.info.row + this.focused.row, 0, 10);
        this.g.ctx.fillText(this.info.col + this.focused.col, 0, 20);
        this.g.ctx.fillText(this.info.tile_x + this.focused.pos[0], 0, 80);
        this.g.ctx.fillText(this.info.tile_y + this.focused.pos[1], 0, 90);
      }
      this.g.ctx.fillText(this.info.camera_x + this.camera[0], 0, 100);
      this.g.ctx.fillText(this.info.camera_y + this.camera[1], 0, 110);
      this.g.ctx.fillText(this.info.num_rendering + this.total, 0, 120);
      this.g.ctx.fillText(this.info.lerp_x + this.lerp_to[0], 0, 130);
      this.g.ctx.fillText(this.info.lerp_y + this.lerp_to[1], 0, 140);
      this.g.ctx.fillText(this.info.tilename + this.current_tile_meta.name, 0, 30);
      this.g.ctx.fillText(this.info.layer + this.current_layer, 0, 40);
      this.g.ctx.fillText(this.info.fps + Hal.fps, 0, 50);
      this.g.ctx.fillText(this.info.start_row + this.from_row, 0, 60);
      return this.g.ctx.fillText(this.info.start_col + this.from_col, 0, 70);
    };
    AmjadMap.prototype.loadTile = function(i, data) {
      var g, t,
        _this = this;
      if (i >= data.map.length) {
        NProgress.set(1.0);
        NProgress.done(true);
        this.resume();
        console.log("completed....");
      } else {
        g = data.map[i];
        t = new Tile(g.x, g.y, g.row, g.col, this.tile_dim);
        this.grid.push(this.addEntity(t));
        if (g.layers !== null) {
          t.load(g.layers);
        }
        return setTimeout(function() {
          if ((i % _this.numcols) === 0) {
            NProgress.set(i / data.map.length);
          }
          return _this.loadTile(i + 1, data);
        }, 0);
      }
    };
    AmjadMap.prototype.drawSpan = function(span, color) {
      var s, _i, _len, _results;
      _results = [];
      for (_i = 0, _len = span.length; _i < _len; _i++) {
        s = span[_i];
        _results.push(this.g.drawImage(this.over[color].img, s.pos[0], s.pos[1]));
      }
      return _results;
    };
    AmjadMap.prototype.showTile = function() {
      var skeep,
        _this = this;
      skeep = 0;
      if (this.showtile == null) {
        return this.showtile = Hal.addListener("ENTER_FRAME", function(delta) {
          if (_this.shift_pressed && _this.focused) {
            if (skeep % 2) {
              _this.cur_area = _this.getSpanArea(_this.focused, _this.current_tile_meta.size);
            }
            if (_this.canBePlacedOn(_this.cur_area, _this.current_layer)) {
              _this.drawSpan(_this.cur_area, "green");
              _this.can_place = true;
            } else {
              _this.drawSpan(_this.cur_area, "red");
              _this.can_place = false;
            }
            _this.g.drawImage(_this.current_tile_sprite.img, _this.over_coord[0] - _this.current_tile_w, _this.over_coord[1] - _this.current_tile_h);
            return skeep = !skeep % 2;
          } else {
            console.log("nice nice");
            Hal.removeListener("ENTER_FRAME", _this.showtile);
            return _this.showtile = null;
          }
        });
      }
    };
    AmjadMap.prototype.lerpCameraTo = function(pos) {
      var _this = this;
      this.lerp_to[0] = this.center_point[0] - this.tilew2 - pos[0];
      this.lerp_to[1] = this.center_point[1] - this.tileh2 - pos[1];
      if (!this.lerp_anim) {
        return this.lerp_anim = Hal.addListener("ENTER_FRAME", function(delta) {
          if (~~Math.abs(_this.camera[0] - _this.lerp_to[0]) < 1 && ~~Math.abs(-_this.camera[1] + _this.lerp_to[1]) < 1) {
            Hal.removeListener("ENTER_FRAME", _this.lerp_anim);
            return _this.lerp_anim = null;
          } else {
            Vec2.lerp(_this.camera, _this.camera.slice(), _this.lerp_to, delta / _this.camera_speed);
            _this.from_col = Math.max(0, -Math.round(_this.camera[0] / _this.tile_dim[1]) - 2);
            return _this.from_row = Math.max(0, -Math.ceil(_this.camera[1] / _this.tile_dim[0] * 2) - 1);
          }
        });
      }
    };
    AmjadMap.prototype.lerpCharacterOnPath = function(char, path) {
      var out, p, tile, _i, _len;
      if (path == null) {
        return;
      }
      out = [];
      for (_i = 0, _len = path.length; _i < _len; _i++) {
        p = path[_i];
        tile = this.getTile(p.row, p.col);
        out.unshift(tile.pos);
      }
      return char.followPath(out);
    };
    AmjadMap.prototype.init = function() {
      var _this = this;
      this.addComponent(Clickable);
      this.addComponent(Draggable);
      this.addComponent(Keyboard);
      this.addComponent(Scrollable);
      this.addListener("MOUSE_MOVE", function(pos) {
        this.prev_focused = this.focused;
        this.focused = this.getTileAt([pos[0], pos[1]]);
        if (this.focused != null) {
          this.over_coord = this.focused.pos;
        }
        if (this.moveable && (this.selected != null) && this.drag_started) {

        } else if (this.drag_started) {
          if (this.shift_pressed) {
            return this.triggerListener("LEFT_CLICK", pos);
          } else if (this.delete_mode) {
            return this.triggerListener("LEFT_CLICK", pos);
          } else {
            this.camera[0] = this.camera_prev_pos[0] + (pos[0] - this.start_drag_point[0]);
            this.camera[1] = this.camera_prev_pos[1] + (pos[1] - this.start_drag_point[1]);
            this.from_row = Math.max(0, Math.round(-this.camera[1] / this.tile_dim[1] - 1));
            return this.from_col = Math.max(0, Math.ceil((-this.camera[0] * this.tilew2prop) - 3));
          }
        }
      });
      this.addListener("TILE_CLICKED", function(tile) {
        if (tile == null) {
          return;
        }
        if (!this.cur_clicked) {
          return this.cur_clicked = tile;
        } else {
          if ((tile.parent_tile.col === this.cur_clicked.parent_tile.col) && (tile.parent_tile.row === this.cur_clicked.parent_tile.row)) {
            if (this.layers[tile.level] > this.layers[this.cur_clicked.level]) {
              this.cur_clicked = tile;
            }
          }
          if (tile.parent_tile.row === this.cur_clicked.parent_tile.row) {
            if (tile.h + tile.pos[1] > this.cur_clicked.h + this.cur_clicked.pos[1]) {
              this.cur_clicked = tile;
            }
          }
          if (tile.parent_tile.col === this.cur_clicked.parent_tile.col) {
            if (tile.h + tile.pos[1] > this.cur_clicked.h + this.cur_clicked.pos[1]) {
              this.cur_clicked = tile;
            }
          }
          if ((tile.parent_tile.col !== this.cur_clicked.parent_tile.col) && (tile.parent_tile.row !== this.cur_clicked.parent_tile.row)) {
            if (tile.h + tile.pos[1] > this.cur_clicked.h + this.cur_clicked.pos[1]) {
              return this.cur_clicked = tile;
            }
          }
        }
      });
      this.addListener("MOUSE_DBL_CLICK", function() {});
      this.addListener("MOUSE_CLICKED", function() {
        if ((this.cur_clicked == null) || this.character_draw_path) {
          return;
        }
        if (this.cur_clicked.to_tint || ((this.selected != null) && !this.cur_clicked.equals(this.selected))) {
          if (this.selected != null) {
            this.selected.triggerListener("DESELECTED");
          }
          this.selected = this.cur_clicked;
          if (this.selected != null) {
            this.selected.triggerListener("SELECTED");
          }
          return this.cur_clicked = null;
        } else {
          this.selected = this.cur_clicked;
          return this.selected.triggerListener("SELECTED");
        }
      });
      this.addListener("DRAG_START", function(pos) {
        this.camera_prev_pos = this.camera.slice();
        this.start_drag_point = pos.slice();
        return this.drag_started = true;
      });
      this.addListener("DRAG_END", function(pos) {
        return this.drag_started = false;
      });
      this.addListener("ENTER_FULLSCREEN", function() {
        var r, _i, _len, _ref, _results;
        this.center_point = [this.bounds[2] / 2, this.bounds[3] / 2];
        this.max_cols = Math.min(this.numcols - 1, Math.floor(this.bounds[2] / (this.tile_dim[0] / 2) + 3));
        this.max_rows = Math.min(this.numrows - 1, Math.ceil(this.bounds[3] / this.tile_dim[1]) + 1);
        _ref = this.layer_renderers;
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          r = _ref[_i];
          r.resize(Hal.dom.screen_w / this.scale[0], Hal.dom.screen_h / this.scale[1]);
          _results.push(console.log(this.scale));
        }
        return _results;
      });
      this.addListener("EXIT_FULLSCREEN", function() {
        var r, _i, _len, _ref, _results;
        this.center_point = [this.bounds[2] / 2, this.bounds[3] / 2];
        this.max_cols = Math.min(this.numcols - 1, Math.floor(this.bounds[2] / (this.tile_dim[0] / 2) + 3));
        this.max_rows = Math.min(this.numrows - 1, Math.ceil(this.bounds[3] / this.tile_dim[1]) + 1);
        _ref = this.layer_renderers;
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          r = _ref[_i];
          _results.push(r.resize(this.bounds[2] / this.scale[0], this.bounds[3] / this.scale[1]));
        }
        return _results;
      });
      this.addListener("CHARACTER_SELECTED", function(char) {
        return this.character_selected = char;
      });
      this.addListener("CHARACTER_DESELECTED", function(char) {
        return this.character_selected = null;
      });
      this.addListener("RIGHT_CLICK", function(pos) {
        var layer;
        if (this.delete_mode && this.focused) {
          if (this.focused.layers[this.current_layer]) {
            if (this.focused.layers[this.current_layer].is_partial) {
              layer = this.focused.layers[this.current_layer].parent_tile;
            } else {
              layer = this.focused.layers[this.current_layer];
            }
            if (this.buffers.del[layer.id] != null) {
              delete this.buffers.del[layer.id];
              return layer.tint(false);
            }
          }
        } else if (this.character_selected == null) {
          this.right_tile = this.getTileAt(pos);
          if (this.right_tile == null) {
            return;
          }
          return this.lerpCameraTo(this.right_tile.pos);
        } else if (this.character_draw_path && (this.character_selected != null) && this.focused) {
          console.log("trying to delete...");
          if (this.buffers.path[this.focused.id] != null) {
            console.log("deleting path...");
            delete this.buffers.path[this.focused.id];
            if (this.focused.layers[0]) {
              return this.focused.layers[0].tint(false);
            }
          }
        }
      });
      this.addListener("KEY_DOWN", function(x) {
        if (x.keyCode === Hal.Keys.SHIFT) {
          this.shift_pressed = true;
          if (this.current_tile_meta.level != null) {
            if (this.current_tile_sprite.h === this.tile_dim[1]) {
              this.current_tile_h = 0;
            } else {
              this.current_tile_h = (this.current_tile_sprite.h - this.tile_dim[1]) + this.tile_dim[1] / 2;
            }
            return this.showTile();
          }
        } else if (x.keyCode === Hal.Keys.D) {
          return this.delete_mode = true;
        } else if (x.keyCode === Hal.Keys.W) {
          return this.moveable = true;
        } else if (x.keyCode === Hal.Keys.C) {
          return this.character_draw_path = true;
        }
      });
      this.addListener("KEY_UP", function(x) {
        var k, v, _ref;
        if (x.keyCode === Hal.Keys.SHIFT) {
          return this.shift_pressed = false;
        } else if (x.keyCode === Hal.Keys.D) {
          return this.delete_mode = false;
        } else if (x.keyCode === Hal.Keys.DELETE) {
          _ref = this.buffers.del;
          for (k in _ref) {
            v = _ref[k];
            v.parent_tile.removeLayer(v.layer);
          }
          return this.buffers.del = {};
        } else if (x.keyCode === Hal.Keys.ONE) {
          return this.current_layer = 0;
        } else if (x.keyCode === Hal.Keys.TWO) {
          return this.current_layer = 1;
        } else if (x.keyCode === Hal.Keys.THREE) {
          return this.current_layer = 2;
        } else if (x.keyCode === Hal.Keys.FOUR) {
          return this.current_layer = 3;
        } else if (x.keyCode === Hal.Keys.W) {
          return this.moveable = false;
        } else if (x.keyCode === Hal.Keys.C) {
          return this.character_draw_path = false;
        }
      });
      this.addListener("LEFT_CLICK", function(pos) {
        var layer, path;
        if (this.shift_pressed && this.can_place && !this.character_selected) {
          return this.putAt(pos, this.current_tile_meta);
        } else if (this.delete_mode && this.focused) {
          if (this.focused.layers[this.current_layer]) {
            if (this.focused.layers[this.current_layer].is_partial) {
              layer = this.focused.layers[this.current_layer].parent_tile;
            } else {
              layer = this.focused.layers[this.current_layer];
            }
            this.buffers.del[layer.id] = layer;
            return layer.tint(true, "red");
          }
        } else if ((this.character_selected != null) && this.character_draw_path && this.focused) {
          this.buffers.path[this.focused.id] = this.focused.pos;
          if (this.focused.layers[0]) {
            return this.focused.layers[0].tint(true, "red");
          }
        } else if ((this.character_selected != null) && this.focused) {
          path = this.pathfinder.find(this.character_selected.parent_tile, this.focused);
          return this.lerpCharacterOnPath(this.character_selected, path);
        }
      });
      Hal.addListener("REQEST_MAP_SAVE", function(name) {
        var map;
        map = _this.save();
        return Hal.triggerListener("MAP_SAVED", {
          map: map,
          name: name + "_" + _this.numrows + "x" + _this.numcols
        });
      });
      Hal.addListener("REQUEST_MAP_LOAD", function(mapdata) {
        return _this.load(mapdata);
      });
      Hal.addListener("TILE_SELECTED", function(tile) {
        _this.current_layer = _this.layers[tile.tile.level];
        _this.current_tile_sprite = Hal.asm.getSprite(tile.tile.sprite);
        if (_this.current_tile_sprite.h === _this.tile_dim[1]) {
          _this.current_tile_h = 0;
        } else {
          _this.current_tile_h = (_this.current_tile_sprite.h - _this.tile_dim[1]) + _this.tile_dim[1] / 2;
        }
        _this.current_tile_w = _this.current_tile_sprite.w / 2 - _this.tile_dim[0] / 2;
        _this.showTile();
        return _this.current_tile_meta = tile.tile;
      });
      this.addListener("SCROLL_UP", function(ev) {
        var r, _i, _len, _ref, _results;
        if (_this.shift_pressed) {
          return _this.current_tile_h += 1;
        } else {
          _this.zoom_in_factor += _this.scale_step;
          if (_this.zoom_in_factor > 0.25) {
            _this.zoom_in_factor = 0.25;
            return;
          }
          console.log("zoom in");
          console.log(_this.zoom_in_factor);
          _this.current_zoom += _this.zoom_in_factor - _this.zoom_out_factor;
          console.log(_this.current_zoom);
          Hal.r.ctx.scale(_this.current_zoom, _this.current_zoom);
          _ref = _this.layer_renderers;
          _results = [];
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            r = _ref[_i];
            _results.push(r.ctx.scale(_this.current_zoom, _this.current_zoom));
          }
          return _results;
        }
      });
      return this.addListener("SCROLL_DOWN", function(ev) {
        var r, _i, _len, _ref, _results;
        if (_this.shift_pressed) {
          return _this.current_tile_h -= 1;
        } else {
          _this.zoom_out_factor += _this.scale_step;
          console.log("zoom out");
          console.log(_this.zoom_out_factor);
          if (_this.zoom_out_factor > 0.25) {
            _this.zoom_out_factor = 0.25;
            return;
          }
          _this.current_zoom -= _this.zoom_out_factor + _this.zoom_in_factor;
          Hal.r.ctx.scale(_this.current_zoom, _this.current_zoom);
          _ref = _this.layer_renderers;
          _results = [];
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            r = _ref[_i];
            _results.push(r.ctx.scale(_this.current_zoom, _this.current_zoom));
          }
          return _results;
        }
      });
    };
    return AmjadMap;
  });

}).call(this);
