"use strict"

define ["EventDispatcher", "../amjad/tiles/TerrainTile", "../amjad/tiles/BuildingTile", "../amjad/tiles/DecorTile", "../amjad/tiles/Character"], 

(EventDispatcher, TerrainTile, BuildingTile, DecorTile, Character) ->

    class MapLoader extends EventDispatcher
        constructor: () ->
            super()
            return

    MapLoader::loadTileLayer = (meta, parent, h, w, layer, renderer) ->
        #console.log(w)
        if meta.level == "terrain"
            return new TerrainTile(meta, parent, h, w, 0, renderer)        
        else if meta.level == "buildings"
            return new BuildingTile(meta, parent, h, w, 2, renderer)
        else if meta.level == "decor"
            return new DecorTile(meta, parent, h, w, 1, renderer)
        else if meta.level == "poi"
            return new POITile(meta, parent, h, w, 3, renderer)
        else if meta.level == "character"
            return new Character(meta, parent, h, w, 1, renderer)

    return new MapLoader()