(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["Scene", "Entity", "Clickable", "Keyboard", "Vec2"], function(Scene, Entity, Clickable, Keyboard, Vec2) {
    var FOFEntity, FOFScene;
    FOFEntity = (function(_super) {
      __extends(FOFEntity, _super);

      function FOFEntity(x, y, bounds) {
        this.base = 30;
        this.h = this.base * 2;
        FOFEntity.__super__.constructor.call(this, x, y, [-this.base, -this.base, this.h, this.h]);
        this.poly = Hal.m.createRegularon(6, this.base);
        this.step = Math.PI / 4;
        this.ang = 0;
        this.sprite = Hal.asm.getSprite("unordered_warhorse");
        this.mass = 7;
        this.gravity = Vec2.fromValues(0, 0);
        this.velocity = Vec2.fromValues(0.0001, 0.0001);
        this.max_vel = 2;
        this.target = this.pos;
        Vec2.sub(this.velocity, this.target, this.pos);
        this.steering_f = Vec2.fromValues(0, 0);
        this.max_steering_f = 0.2;
        this.max_speed = 0.5;
      }

      FOFEntity.prototype.init = function() {};

      FOFEntity.prototype.followPath = function(path) {
        var _this = this;
        if (!this.follow_path) {
          return this.follow_path = Hal.addListener("ENTER_FRAME", function(delta) {
            if ((Math.abs(_this.pos[0] - _this.target[0]) < 10) && (Math.abs(_this.pos[1] - _this.target[1]) < 10)) {
              _this.target = path.shift();
              if (_this.target == null) {
                Hal.removeListener("ENTER_FRAME", _this.follow_path);
                _this.follow_path = null;
                _this.target = _this.pos;
                Vec2.sub(_this.velocity, _this.target, _this.pos);
              }
            }
          });
        }
      };

      FOFEntity.prototype.update = function() {
        var desired_vel, p, time, _i, _len, _ref;
        time = Hal.t.delta * 0.001;
        desired_vel = [];
        Vec2.sub(desired_vel, this.target, this.pos);
        Vec2.normalize(desired_vel, desired_vel);
        Vec2.scale(desired_vel, desired_vel, this.max_vel);
        Vec2.sub(this.steering_f, desired_vel, this.velocity);
        Vec2.scale(this.velocity, this.velocity, this.max_vel);
        Vec2.scale(this.steering_f, this.steering_f, 0.3 / this.mass);
        Vec2.add(this.velocity, this.velocity, this.steering_f);
        Vec2.add(this.pos, this.pos, this.velocity, time);
        Vec2.normalize(this.velocity, this.velocity);
        this.g.ctx.strokeStyle = "red";
        this.g.ctx.strokeRect(this.pos[0], this.pos[1], 1, 1);
        this.g.ctx.stroke();
        this.g.ctx.strokeStyle = "green";
        this.g.ctx.strokeRect(this.target[0], this.target[1], 1, 1);
        this.g.ctx.stroke();
        this.g.ctx.moveTo(this.pos[0], this.pos[1]);
        this.g.ctx.lineTo(this.pos[0] + this.velocity[0] * 50, this.pos[1] + this.velocity[1] * 50);
        this.g.ctx.stroke();
        this.g.ctx.moveTo(this.pos[0], this.pos[1]);
        this.g.ctx.lineTo(this.pos[0] + this.steering_f[0] * 100, this.pos[1] + this.steering_f[1] * 100);
        this.g.ctx.stroke();
        this.g.ctx.transform(Math.cos(this.ang), -Math.sin(this.ang), Math.sin(this.ang), Math.cos(this.ang), this.pos[0], this.pos[1]);
        this.ang += this.step * Hal.t.delta * 0.001;
        this.g.ctx.beginPath();
        this.g.ctx.moveTo(this.poly[0][0], this.poly[0][1]);
        _ref = this.poly.slice(1);
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          p = _ref[_i];
          this.g.ctx.lineTo(p[0], p[1]);
        }
        this.g.ctx.closePath();
        return this.g.ctx.stroke();
      };

      return FOFEntity;

    })(Entity);
    FOFScene = (function(_super) {
      __extends(FOFScene, _super);

      FOFScene.prototype.drawInfo = function() {
        this.g.ctx.fillText("accel: " + Vec2.str(this.ent.gravity), 0, 10);
        this.g.ctx.fillText("velocity: " + Vec2.str(this.ent.velocity), 0, 20);
        this.g.ctx.fillText("position: " + Vec2.str(this.ent.pos), 0, 30);
        this.g.ctx.fillText("steering_f: " + Vec2.length(this.ent.steering_f), 0, 40);
        return this.g.ctx.fillText("speed: " + Vec2.length(this.ent.velocity), 0, 50);
      };

      function FOFScene(name, bounds, newcanvas) {
        FOFScene.__super__.constructor.call(this, name, bounds, newcanvas);
        this.camera[0] = 0;
        this.camera[1] = 0;
        this.ngons = [];
        this.ent = new FOFEntity(200, 100);
        this.addEntity(this.ent);
        this.path = [];
      }

      FOFScene.prototype.init = function() {
        this.addComponent(Clickable);
        this.addComponent(Keyboard);
        this.addListener("KEY_UP", function(key) {
          if (key.keyCode === Hal.Keys.G) {
            return this.ent.followPath(this.path);
          }
        });
        return this.addListener("LEFT_CLICK", function(pos) {
          return this.path.push([pos[0], pos[1]]);
        });
      };

      FOFScene.prototype.update = function() {
        var p, _i, _len, _ref;
        this.g.ctx.translate(this.camera[0], this.camera[1]);
        this.ent.update();
        this.g.ctx.setTransform(1, 0, 0, 1, 0, 0);
        _ref = this.path;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          p = _ref[_i];
          this.g.ctx.strokeRect(p[0] - 2, p[1] - 2, 4, 4);
          this.g.ctx.stroke();
        }
        return this.drawInfo();
      };

      return FOFScene;

    })(Scene);
    return FOFScene;
  });

}).call(this);
