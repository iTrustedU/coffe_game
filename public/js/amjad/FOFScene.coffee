"use strict"

define ["Scene", "Entity", "Clickable", "Keyboard", "Vec2"], 

(Scene, Entity, Clickable, Keyboard, Vec2) ->

    class FOFEntity extends Entity
        constructor: (x, y, bounds) ->
            @base = 30
            @h = @base*2
            super(x, y, [-@base, -@base, @h, @h])
            @poly = Hal.m.createRegularon(6, @base)
            @step = Math.PI/4 
            @ang = 0
            @sprite = Hal.asm.getSprite("unordered_warhorse")

            @mass = 7
            @gravity = Vec2.fromValues(0, 0)

            @velocity = Vec2.fromValues(0.0001, 0.0001)
            @max_vel = 2
            @target = @pos
            Vec2.sub(@velocity, @target, @pos)

            @steering_f = Vec2.fromValues(0,0)

            @max_steering_f = 0.2
            @max_speed = 0.5

        init: () ->
            #@g = @g.getLocalRenderer()

        followPath: (path) ->
            if not @follow_path
                @follow_path = Hal.addListener "ENTER_FRAME", (delta) =>
                    if (Math.abs(@pos[0] - @target[0]) < 10) and (Math.abs(@pos[1] - @target[1]) < 10)
                        @target = path.shift()
                        if not @target?
                            Hal.removeListener "ENTER_FRAME", @follow_path
                            @follow_path = null
                            @target = @pos
                            Vec2.sub(@velocity, @target, @pos)   
                            return  

        update: () ->    
            #@g.ctx.drawImage(@sprite.img, @pos[0]-@sprite.w/2, @pos[1]-@sprite.h/2)
            time = Hal.t.delta * 0.001
            desired_vel = []
            Vec2.sub(desired_vel, @target, @pos)
            Vec2.normalize(desired_vel, desired_vel)
            Vec2.scale(desired_vel, desired_vel, @max_vel)
            Vec2.sub(@steering_f, desired_vel, @velocity)
            Vec2.scale(@velocity, @velocity, @max_vel)
            Vec2.scale(@steering_f, @steering_f,  0.3/@mass)

            Vec2.add(@velocity, @velocity, @steering_f)
            Vec2.add(@pos, @pos, @velocity, time)
            Vec2.normalize(@velocity, @velocity)

            @g.ctx.strokeStyle = "red"
            @g.ctx.strokeRect(@pos[0], @pos[1], 1, 1)
            @g.ctx.stroke()

            @g.ctx.strokeStyle = "green"
            @g.ctx.strokeRect(@target[0], @target[1], 1, 1)
            @g.ctx.stroke()

            @g.ctx.moveTo(@pos[0], @pos[1])
            @g.ctx.lineTo(@pos[0] + @velocity[0]*50, @pos[1] + @velocity[1]*50)
            @g.ctx.stroke()

            @g.ctx.moveTo(@pos[0], @pos[1])
            @g.ctx.lineTo(@pos[0] + @steering_f[0]*100, @pos[1] + @steering_f[1]*100)
            @g.ctx.stroke()

            @g.ctx.transform(Math.cos(@ang), -Math.sin(@ang), Math.sin(@ang), Math.cos(@ang), @pos[0], @pos[1])
            @ang += (@step*Hal.t.delta*0.001)

            @g.ctx.beginPath()
            @g.ctx.moveTo(@poly[0][0], @poly[0][1])
            for p in @poly[1..]
                @g.ctx.lineTo(p[0] , p[1])
            @g.ctx.closePath()
            @g.ctx.stroke()

            #@g.ctx.translate(-@pos[0], -@pos[1])


    class FOFScene extends Scene

        drawInfo: () ->
            @g.ctx.fillText("accel: " + Vec2.str(@ent.gravity), 0, 10)
            @g.ctx.fillText("velocity: " + Vec2.str(@ent.velocity),0, 20)
            @g.ctx.fillText("position: " + Vec2.str(@ent.pos), 0, 30)
            @g.ctx.fillText("steering_f: " + Vec2.length(@ent.steering_f), 0, 40)
            @g.ctx.fillText("speed: " + Vec2.length(@ent.velocity), 0, 50)
        
        constructor: (name, bounds, newcanvas) ->
            super(name, bounds, newcanvas)
            @camera[0] = 0
            @camera[1] = 0
            @ngons = []

            @ent = new FOFEntity(200, 100)
            @addEntity(@ent)

            @path = []

        init: () ->
            @addComponent(Clickable)
            @addComponent(Keyboard)

            @addListener "KEY_UP", (key) ->
                if key.keyCode == Hal.Keys.G
                    @ent.followPath(@path)

            @addListener "LEFT_CLICK", (pos) ->
                @path.push([pos[0], pos[1]])

        update: () ->
            @g.ctx.translate(@camera[0], @camera[1])
            @ent.update()
            @g.ctx.setTransform(1, 0, 0, 1, 0, 0)

            for p in @path
                @g.ctx.strokeRect(p[0]-2, p[1]-2, 4, 4)
                @g.ctx.stroke()
            @drawInfo()
            #@g.ctx.translate(-@camera[0], -@camera[1])
            #@g.fillPolygon(@ent.poly)
    return FOFScene