fs      = require("fs")
io      = require("socket.io").listen(8080)
path    = require("path")

socket          = undefined
is_resource     = /^(.*\/(.*)\/)(.*[png|jpg|ogg|json])$/
is_png          = /^.*\.png$/
is_map          = /^.*\.json$/
maps            = []
cur_dir         = process.cwd()
baseDir         = "public"
publicDir       = cur_dir + path.sep + baseDir
resourceDir     = publicDir + path.sep + "resources"

io.sockets.on "connection", (sck) ->
    socket = sck
    socket.emit("hello", {
        type: "maps",
        data: loadMaps()
    })
    initSocket()

resources = 
    tileslist: resourceDir + path.sep + "TilesList.json"
    maps:   resourceDir + path.sep + "maps/maps.list"

loadMaps = () ->
    maps = fs.readFileSync(resources.maps) + ""
    return maps.split("\n")

initSocket = () ->
    socket.on "NEW_TILE", (data) ->
        tile = JSON.parse(data.tile)
        list = JSON.parse(fs.readFileSync(resources.tileslist))
        list[tile.name] = tile
        fs.writeFileSync(resources.tileslist, JSON.stringify(list))
        socket.emit "NEW_TILE_LOADED", tile
    
    socket.on "NEW_MAP", (data) ->
        fs.writeFileSync(resourceDir + path.sep + "maps" + path.sep + data.name + ".json", data.map);

    socket.on "LOAD_ALL_MAPS", () ->
        socket.emit "MAPS_LOADED", loadMaps()

monitor_resources = (x) ->
    matches = is_resource.exec(x)

    if matches.length < 4
        return
    files = fs.readdirSync(matches[1])

    if matches[2] == "maps"
        data = files.filter((x) -> return is_map.test(x))
    else if matches[2] == "sprites"
        data = files.filter((x) -> return is_png.test(x))
    
    fs.writeFileSync(matches[1]+matches[2]+'.list', data.join().replace(/,/g,"\n"))
    socket.emit("RESOURCES_CHANGED", {
        url: matches[1]
        type: matches[2]
        file: matches[3] 
        files: JSON.stringify(data)
    })

module.exports = (grunt) ->
    grunt.loadNpmTasks "grunt-contrib-uglify"
    grunt.loadNpmTasks "grunt-contrib-connect"
    grunt.loadNpmTasks "grunt-contrib-watch"
    grunt.loadNpmTasks "grunt-contrib-coffee"

    grunt.initConfig
        pkg: grunt.file.readJSON "package.json"
        
        coffee: 
            glob_all: 
                expand: true,
                cwd: baseDir + "/js/",
                src: ['**/*.coffee'],
                dest: baseDir + "/js/",
                ext: '.js'

        uglify:
            halal:          
                options:
                    banner: "/*! halal.js <%= grunt.template.today('yyyy-mm-dd') %> */\n"
                    compress: true
                files:
                    baseDir + "/js/halal/build/halal.min.js": 
                        [baseDir + "/js/halal/*.*"]
        connect: 
            server:
                options:
                    keepalive: false
                    port: 9000
                    base: baseDir

        watch:
            resources:
                files: [baseDir + "/resources/sprites/**/*.png", baseDir + "/resources/maps/**/*.json"]

            coffee: 
                files: [
                    baseDir + '/js/game/**/*.coffee', 
                    baseDir + "/js/main.coffee", 
                    baseDir + "/js/amjad/**/*.coffee",
                    baseDir + "/js/amjad/tiles/**/*.coffee",
                    baseDir + "/js/coptera/**/*.coffee"
                ]
                tasks: ['coffee:glob_all']
                options:
                    nospawn: true
                    livereload: true

            scripts:
                files: [baseDir + "/js/main.js", baseDir + "/js/game/**/*.js"]
                options:
                    livereload: true

        grunt.event.on "watch", (action, filepath) ->
            filepath = filepath.replace(grunt.config("coffee.glob_all.cwd"), '')
            if is_resource.test(filepath)
                monitor_resources(filepath)

            grunt.config("coffee.glob_all.src", [filepath])

    grunt.registerTask "serve", ["connect:server", "watch"]